WITH inputs AS (
    SELECT %i AS complexId, %i AS pitchId, '%s' AS DATE, %i AS day_of_week, %i AS START, %i AS xend, '%s' AS userEmail,
    0 AS paid
)
INSERT INTO reserves
  SELECT
    complexId,
    inputs.pitchId,
    inputs.stringDate,
    start,
    xend,
    userEmail,
    price,
    paid
  FROM inputs, pitchschedule, schedule
  WHERE
    inputs.pitchid = pitchschedule.pitchid AND pitchschedule.blockid = schedule.id AND schedule.day = day_of_week
    AND schedule.start >= inputs.start AND schedule.xend > inputs.start;
