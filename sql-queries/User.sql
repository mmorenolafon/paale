CREATE TABLE IF NOT EXISTS users(
	firstName VARCHAR(100) NOT NULL,
	lastName VARCHAR(100) NOT NULL,
	password VARCHAR(100) NOT NULL,
	email VARCHAR(50) NOT NULL PRIMARY KEY,
	phone VARCHAR(20) NOT NULL,
	username VARCHAR(20));
/*
CREATE TRIGGER reserveVerify
    BEFORE INSERT ON reserve
    BEGIN
    IF (NOT EXISTS(
      SELECT * FROM pitch where pitch.id = new.pitchId and pitch.complexId = new.complexId;
    )) THEN
    RAISE SQLSTATE '42P10';
    */