CREATE TABLE IF NOT EXISTS reserve(
	complexId INTEGER NOT NULL,
	pitchId INTEGER NOT NULL,
	day DATE NOT NULL,
	start INTEGER NOT NULL,
	xend INTEGER NOT NULL,
	userEmail VARCHAR(50) NOT NULL,
	price NUMERIC NOT NULL,
	paid NUMERIC DEFAULT 0.0,
	PRIMARY KEY (complexId, pitchId, day, start, xend)
);
/*
INSERT INTO reserve VALUES (1,1,'2016-05-27',16,17,'gmogni@itba.edu.ar',800.00,400.00),(1,2,'2016-05-27',13,14,'tito@itba.edu.ar',700.00,100.00),(1,1,'2016-05-27',13,14,'gmogni@itba.edu.ar',800.00,200.00),(1,2,'2016-05-28',20,21,'mbooos@itba.edu.ar',900.00,150.00),(1,2,'2016-05-30',14,17,'mili@itba.edu.ar',700.00,300.00),(1,2,'2016-05-29',14,17,'mili@itba.edu.ar',700.00,300.00),(1,2,'2016-05-29',20,21,'mbooos@itba.edu.ar',900.00,150.00);
*/
