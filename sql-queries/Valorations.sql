  CREATE TABLE IF NOT EXISTS valorations(
    email VARCHAR(50),
    complexId INTEGER NOT NULL REFERENCES complex(id),
    stringDate DATE NOT NULL,
    valoration INTEGER NOT NULL,
    PRIMARY KEY (email, complexId)
  );

    CREATE TABLE IF NOT EXISTS pending_valorations(
    email VARCHAR(50) REFERENCES users(email),
    complexId INTEGER NOT NULL REFERENCES complex(id),
    stringDate DATE NOT NULL,
    time INTEGER NOT NULL
  );
