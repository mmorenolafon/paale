package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.Pitch;
import ar.edu.itba.paw.model.Reserve;
import ar.edu.itba.paw.model.User;

import java.time.LocalDate;
import java.util.List;

public interface ReserveDao {

    /**
     * The method searches all the reserves from the "Reserve" table of the database where the user email
     * matches the parameter and the date is smaller than the current date.
     *
     * @param user
     * @return A list of the reserves matching the condition. Empty if no reserves where found
     */
    List<Reserve> getUserReserveHistory(User user);

    /**
     * The method searches all the reserves from the "Reserve" table of the database where the user email
     * matches the parameter and the date is greater than the current date.
     *
     * @param user The search key
     * @return A list of the reserves matching the condition. Empty if no reserves where found
     */
    List<Reserve> getUserReserves(User user);

    /**
     * The method inserts a new reserve into the database and returns "true". If that is not possible because of a violation in the
     * database integrity, the insertion is not done and the method return "false".
     *
     * @param pitch
     * @return True if the insertion was possible, false otherwise
     */
    boolean addReserve(Pitch pitch, int startTime, int endTime, LocalDate date, User user, String bookerUsername);

        /**
         * the method remove bookings from the data base.
         *
         * @param pitch
         * @param startTime
         * @param date
         * @return True if the booking was deleted
         */
    boolean removeBooking(Pitch pitch, int startTime, LocalDate date, User user);

    /**
     * @param complex
     * @param user
     * @param date
     * @param startTime
     * @param endTime
     * @return
     */
    List<Reserve> getBookingsByComplex(Complex complex, String user, LocalDate date, int startTime, int endTime);
}
