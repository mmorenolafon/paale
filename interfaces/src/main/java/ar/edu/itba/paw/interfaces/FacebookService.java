package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.User;

public interface FacebookService {

    User loginUser(String fToken);

}
