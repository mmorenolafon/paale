package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.Admin;
import ar.edu.itba.paw.model.User;

public interface AdminDao {

    /**
     * Looks for the complexes that are administrated for the user
     *
     * @param user The user instance with the email being the search key
     * @return An admin with its complexes if exists. Null if that user has no complexes depending on them.
     */
    Admin getAdmin(User user);

}
