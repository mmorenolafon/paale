package ar.edu.itba.paw.interfaces;


import ar.edu.itba.paw.model.ScheduleException;

import java.util.List;

public interface ScheduleExceptionService {

    /**
     * @param complexId
     * @param beginDate
     * @param endDate
     * @return
     */
    boolean addScheduleException(int complexId, String beginDate, String endDate);

    /**
     * @param complexId
     * @param beginDate
     * @param endDate
     * @return
     */
    void deleteScheduleException(int complexId, String beginDate, String endDate);

    /**
     * @param complexId
     * @return
     */
    List<ScheduleException> getAllScheduleExceptionsByComplex(int complexId);


    /**
     * Removes all the deprecated schedule exceptions, the one which end date has already passed
     */
    void removeOldScheduleExceptions();
}
