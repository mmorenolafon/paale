package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.States;

import java.util.List;

public interface StatesDao {

    /**
     * The method searches all the states inserted in the table "State" from the database.
     *
     * @return A list of states registered in the database.
     */
    List<States> getAllStates();

    /**
     * The method searches a single state in the database, with it's id matching the parameter.
     *
     * @param id The search key
     * @return The state with the requested id if found in the database, null otherwise
     */
    States getStatesById(int id);
}
