package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.User;

import java.util.List;


public interface UserDao {

    /**
     * The method inserts a new user in the database if there is no user with the same mail or password.
     *
     * @param firstName User's first name, not null
     * @param lastName  User's last name, not null
     * @param password  User's password, not null
     * @param email     User's email, not null, not existing in dabatase
     * @param phone     User's phone number, in any format
     * @param username  User's username. Can be null, not existing in database
     * @return The new user instance if it was created, null otherwise.
     */
    User create(final String firstName, final String lastName, final String password, final String email,
                final String phone, final String username);

    /**
     * The method searches any existing user in the databse with the same username received as parameter
     *
     * @param username The search key
     * @return The User instance if it exists, null otherwise
     */
    User getByUsername(String username);


    /**
     * The method searches any existing user in the databse with the same email received as parameter
     *
     * @param email The search key
     * @return The User instance if it exists, null otherwise
     */
    User getByEmail(String email);

    /**
     * @param email
     * @return
     */
    List<User> getByPartOfEmail(String email);
}