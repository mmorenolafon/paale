package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.PendingValoration;
import ar.edu.itba.paw.model.User;

import java.time.LocalDate;


public interface PendingValorationsDao {

    /**
     * @param user
     * @param complex
     * @param date
     * @param time
     */
    void addPendingValoration(User user, Complex complex, LocalDate date, int time);

    /**
     * @param user
     * @return the PendingValoration of the last complex where the user played and did not valorate
     */
    PendingValoration getPendingValoration(User user);

}
