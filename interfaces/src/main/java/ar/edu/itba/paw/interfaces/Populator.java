package ar.edu.itba.paw.interfaces;

public interface Populator {

    void populate();

    void addLepoPitches();
}
