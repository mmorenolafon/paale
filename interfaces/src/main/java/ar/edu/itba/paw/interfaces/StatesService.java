package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.States;

import java.util.List;

public interface StatesService {

    /**
     * The method searches all the states inserted in the table "State" from the database.
     *
     * @return A list of states registered in the database.
     */
    List<States> getAllStates();

}
