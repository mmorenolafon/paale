package ar.edu.itba.paw.interfaces;


public interface EmailSender {
    /**
     * @param to      Email address which should receive the email
     * @param subject Subject of the email to be sent
     * @param content Content of the email to be sent
     * @return Returns whether the email could be sent or nor
     */
    boolean sendMailTo(String to, String subject, String content);

    /**
     * @param complexId Complex's id
     * @param pitchId   Pitch's id
     * @param day       Day of the month to be notified
     * @param month     Month to be notified
     * @param year      Year to be notified
     * @param startTime Match's start time to be notified
     * @param endTime   Match's end time to be notified
     * @return Returns whether the emails could be sent o not
     */
    boolean sendBookingConfirmation(int complexId, int pitchId, int day, int month, int year, int startTime,
                                    int endTime);

    boolean sendBookingCancellation(int complexId, int pitchId, int day, int month, int year, int startTime,
                                    int endTime);

  void sendBookingInvitation(int complexId, int pitchId, int dayOfMonth, int monthValue, int year, int startTime, int i, String[] emailList, String message);


}
