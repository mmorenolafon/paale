package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.CardInformation;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.DTOs.PitchSetTimeTableDTO;
import ar.edu.itba.paw.model.DTOs.PitchTimeTableDTO;
import ar.edu.itba.paw.model.ScheduleException;

import java.time.LocalDate;
import java.util.List;


public interface ComplexService {

    /**
     * Find all pitches available between the specified times at the desired date, and returns for each complex,
     * its information and the best pondered pitch in it
     *
     * @param stateId   State's id
     * @param name      Complex's name
     * @param floor     Floor's id
     * @param size      Required pitch size
     * @param beginHour Minimum start time
     * @param endHour   Maximum start time
     * @param date      Expected date
     * @return Returns a list complexes and a default pitch for each one
     * @throws IllegalArgumentException
     */
    List<CardInformation> getComplex(Integer stateId, String name, Integer floor, Integer size, Integer beginHour,
                                     Integer endHour, String date) throws IllegalArgumentException;

    /**
     * @param id Complex's id
     * @return Complex with the specified id
     */
    Complex getComplexById(int id);

    /**
     * @param complexId
     * @param phone
     * @param email
     * @return True if the operation was completed successfully, false otherwise
     */
    boolean modifyComplexFields(int complexId, String phone, String email);


    /**
     * @param complexId
     * @return
     */
    List<ScheduleException> getScheduleExceptions(int complexId);

    /**
     * @param beginDate
     * @param endDate
     * @return
     */
    boolean addScheduleException(int complexId, LocalDate beginDate, LocalDate endDate);

    /**
     * @param beginDate
     * @param endDate
     */
    void deleteScheduleException(int complexId, LocalDate beginDate, LocalDate endDate);

  List<PitchTimeTableDTO> getSchedule(int complexId);

  void setNewPrices(Integer complexId, PitchSetTimeTableDTO timeTables);


}
