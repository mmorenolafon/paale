package ar.edu.itba.paw.interfaces;

public interface ScheduleExceptionDao {

    /**
     * Removes all the deprecated schedule exceptions, the one which end date has already passed
     */
    void removeOldScheduleExceptions();
}
