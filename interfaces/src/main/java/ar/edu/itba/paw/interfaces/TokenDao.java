package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.Token;


/**
 * Created by tritoon on 10/10/16.
 */
public interface TokenDao {

    Token getToken(String token);

    void setDao(Token token);

    Token getTokenByUserEmail(String userEmail);
}
