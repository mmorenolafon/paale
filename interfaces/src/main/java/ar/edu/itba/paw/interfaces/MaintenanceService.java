package ar.edu.itba.paw.interfaces;


public interface MaintenanceService {

    void cleanUpScheduleExceptions();
}
