package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.Admin;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.DTOs.ReserveDTO;
import ar.edu.itba.paw.model.Pitch;
import ar.edu.itba.paw.model.Reserve;

import java.util.List;
import java.util.zip.DataFormatException;


public interface BookingService {

  /**
   * Sends the corresponding emails to the user who made the booking and the complex administrator,
   * then call the method that tries to make the insertion into the database.
   *
   * @param complexId The complex's id
   * @param pitchId   The pitch's id
   * @param date      Book's date
   * @param startTime Book's start time
   * @return Returns whether the book was made or not
   */
  boolean book(int complexId, int pitchId, String date, int startTime) throws DataFormatException;

  /**
   * @param admin
   * @param bookerUsername
   * @param complexId
   * @param pitchId
   * @param date
   * @param startTime
   * @return
   * @throws DataFormatException
   */
  boolean book(Admin admin, String bookerUsername, int complexId, int pitchId, String date, int startTime) throws DataFormatException;

  /**
   * @param email User's email
   * @return User's reserves before today
   */
  List<ReserveDTO> getUserReserveHistory(String email);

  /**
   * @param email User's email
   * @return User's pending reserves
   */
  List<ReserveDTO> getUserReserves(String email);

  /**
   * Converts the reserves into a list with values that should be shown to the user
   *
   * @param reserves User's reserve list
   * @return List of showable reseves to be used at front-end
   */
  List<ReserveDTO> makeVisible(List<Reserve> reserves);

  /**
   * @param complexId Complex's id
   * @return Complex information
   */
  Complex getComplexById(int complexId);

  /**
   * @param complexId Complex's id
   * @param pitchId   Pitch's id
   * @return Pitch information
   */
  Pitch getPitchById(int complexId, int pitchId);

  /**
   * Returns the price that a pitch should have before making a reservation
   *
   * @param complexId Complex's id
   * @param pitchId   Pitch's id
   * @param date      Expected reservation date
   * @param startTime Expected reservation start time
   * @return Pitch price at a given time, in a specific day
   */
  Double getPitchPrice(int complexId, int pitchId, String date, int startTime);

  /**
   * Remove reserve in one complex.
   *
   * @param complexId
   * @param pitchId
   * @param date
   * @param startTime
   */
  boolean removeBooking(int complexId, int pitchId, String date, int startTime) throws DataFormatException;

  /**
   * @param complexId
   * @param Username
   * @param date
   * @param startTime
   * @param endTime
   * @return a list with the bookings of the complex determinated by the complexId
   * @throws DataFormatException
   */
  List<Reserve> getBookingsByComplex(int complexId, String Username, String date, int startTime, int endTime) throws DataFormatException;


  void invitePlayers(int complexId, int pitchId, String date, int startTime, String[] emailList, String message);
}
