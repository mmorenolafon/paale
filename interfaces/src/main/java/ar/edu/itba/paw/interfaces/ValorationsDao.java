package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.Valoration;


public interface ValorationsDao {

    /**
     * @param val
     */
    void addValoration(Valoration val);

    /**
     * @param quantity
     */
    void deleteByQuantity(int quantity);
}
