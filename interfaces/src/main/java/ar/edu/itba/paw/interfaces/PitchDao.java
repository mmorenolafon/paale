package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.AvailablePitch;
import ar.edu.itba.paw.model.Pitch;
import ar.edu.itba.paw.model.TimeTable;
import ar.edu.itba.paw.model.CardInformation;

import java.util.List;

public interface PitchDao {


  /**
   * @param complexId Complex's id
   * @param pitchId   Pitch's id
   * @return The pitch that matches both ids
   */
  Pitch getPitchById(int complexId, int pitchId);

  /**
   *
   * @param stateId
   * @param name
   * @param floor
   * @param size
   * @param beginHour Minimum start time required
   * @param endHour   Maximum start time required
   * @param date
   * @return List with the available pitches that matches the filters,
   * that has start time between the given values
   */
  List<CardInformation> getAvailablePitchList(Integer stateId, String name, Integer floor, Integer size, Integer beginHour,
                                              Integer endHour, String date);

  /**
   * @param complexId Complex's id
   * @param pitchId   Pitch's id
   * @param day       Desired day
   * @param startTime Desired start time
   * @return Returns the price of the pitch at the given time and date
   */

  Double getPitchPrice(int complexId, int pitchId, int day, int startTime);

  /**
   * @param floor     The id of the pitch floor
   * @param capacity  The size of the pitch
   * @param startTime The initial hour in which the pitch should be able to reserved, inclusive
   * @param endHour   The final hour in which the pitch should be able to be reserved, inclusive
   * @param date      The date where the pitch should be able to be reserved in format "yyyy-MM-dd"
   * @param complexId The id of the complex that owns the pitch
   * @param day       The day of the week, the accepted values are between 0 and 6
   * @return Returns all available pitched in the given complex
   */
  List<AvailablePitch> getAvailableByComplex(Integer startTime, Integer endHour, Integer complexId,
                                             String date, Integer day, Integer floor, Integer capacity);

  void addPitch(Pitch pitch);

  void addSchedule(TimeTable schedule);
}
