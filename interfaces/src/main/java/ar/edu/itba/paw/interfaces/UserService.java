package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.DTOs.ReserveDTO;
import ar.edu.itba.paw.model.User;

import java.util.List;
import java.util.zip.DataFormatException;

public interface UserService {
    /**
     * @param firstName User's first name
     * @param lastName  User's last name
     * @param password  User's password
     * @param email     User's email
     * @param phone     User's phone number
     * @param username  User's username
     * @return Created user
     */
    User register(String firstName, String lastName, String password, String email, String phone, String username);

    /**
     * @param username User's username
     * @return Stored user if exists
     */
    User getByUsername(String username);

    /**
     * @return Logged user if there is one
     */
    User getLoggedUser();

    /**
     * @return User's reserves before today
     */
    List<ReserveDTO> getUserReserveHistory(User user);

    /**
     * @return User's pending reserves
     */
    List<ReserveDTO> getUserReserves(User user);

    /**
     * @param email User's email
     * @return Stored user if exists
     */
    User getByEmail(String email);

    /**
     * @return Username of the logged user if there is one
     */
    String getLoggedUserName();

    /**
     * @param email
     * @return
     */
    List<User> getByPartOfEmail(String email);

    /**
     * @param complexId
     * @param pitchId
     * @param date
     * @param startTime
     * @return
     * @throws DataFormatException
     */
    boolean removeBooking(int complexId, int pitchId, String date, int startTime) throws DataFormatException;
}
