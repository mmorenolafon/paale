package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.ScheduleException;

import java.time.LocalDate;
import java.util.List;


public interface ComplexDao {

    /**
     * @return List with all the complexes
     */
    List<Complex> getComplex();

    /**
     * @param id Complex's id
     * @return Complex with the desired id
     */
    Complex getById(int id);

    /**
     * @param complexId
     * @param phone
     * @return True if the operation was completed successfully, false otherwise
     */
    boolean modifyComplexFields(int complexId, String phone, String email);

    /**
     * @param complexId
     * @return List of the exception of the desired complex
     */
    List<ScheduleException> getScheduleExceptions(int complexId);

    /**
     * @param beginDate
     * @param endDate
     * @return True if the operation was completed successfully, false otherwise
     */
    boolean addScheduleException(int complexId, LocalDate beginDate, LocalDate endDate);

    /**
     * @param beginDate
     * @param endDate
     */
    void deleteScheduleException(int complexId, LocalDate beginDate, LocalDate endDate);


    /**
     * @param complex
     * @return The instance of the complex added
     */
    Complex addComplex(Complex complex);
}
