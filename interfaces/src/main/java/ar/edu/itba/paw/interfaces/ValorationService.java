package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.PendingValoration;

import java.time.LocalDate;


public interface ValorationService {

    void addValoration(int complexid, int valoration, String date);

    void addPendingValoration(int complexId, LocalDate date, int time);

    PendingValoration getPendingValoration();

}
