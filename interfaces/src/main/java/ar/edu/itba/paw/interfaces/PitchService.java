package ar.edu.itba.paw.interfaces;

import ar.edu.itba.paw.model.AvailablePitch;
import ar.edu.itba.paw.model.CardInformation;
import ar.edu.itba.paw.model.Pitch;
import ar.edu.itba.paw.model.TimeTable;

import java.text.ParseException;
import java.util.List;

public interface PitchService {

  /**
   * Searches for all the pitches in the database where the complex id matches the parameter.
   *
   * @param complexId The search key
   * @return A list of all the pitches from the requested complex. Empty list if no pitch meets the condition.
   */
  List<Pitch> getPitch(String complexId);

  /**
   * The method searches for all the pitches that meet the filters required in the map
   *
   * @param floor     The id of the pitch floor
   * @param capacity  The size of the pitch
   * @param startTime The initial hour in which the pitch should be able to reserved, inclusive
   * @param endTime   The final hour in which the pitch should be able to be reserved, inclusive
   * @param date      The date where the pitch should be able to be reserved in format "yyyy-MM-dd"
   * @param complexId The id of the complex that owns the pitch
   * @param day       The day of the week, the accepted values are between 0 and 6
   * @return A list of all the pitches from the requested filters. Empty list if no pitch meets the condition.
   */
  List<AvailablePitch> getPitchAvailable(Integer startTime, Integer endTime, Integer complexId,
                                         String date, Integer day, Integer floor, Integer capacity);

  /**
   * The method searches a pitch in the database that meets the required filters and returns a AvailablePitch
   * instance, which contains the reference to the real pitch, as well as a timetable for the required period
   * of time
   *
   * @param floor     The id of the pitch floor
   * @param capacity  The size of the pitch
   * @param startTime The initial hour in which the pitch should be able to reserved, inclusive
   * @param endHour   The final hour in which the pitch should be able to be reserved, inclusive
   * @param date      The date where the pitch should be able to be reserved in format "yyyy-MM-dd"
   * @param complexId The id of the complex that owns the pitch
   * @return The instance of the AvailablePitch, which contains the reference to the real pitch, as well as a
   * timetable for the required period of time
   * @throws ParseException
   */
  List<AvailablePitch> getGroupsAvailable(Integer floor, Integer capacity, Integer startTime, Integer endHour,
                                          String date, Integer complexId) throws ParseException;

  /**
   * The method searches for a pitch in the database with the required id that belongs to the required complex
   *
   * @param complexId The id of the complex, owner of the required pitch
   * @param pitchId   The id of the pitch, search key.
   * @return The instance of the pitch if it is in the database, null otherwise.
   */
  Pitch getPitchById(int complexId, int pitchId);

  /**
   * The method searches for all the complexes that have an available pitch in the requested time and matches
   * the filters.
   *
   * @param stateId   filter that will be used for the search
   * @param name
   * @param floor     filter that will be used for the search
   * @param size      filter that will be used for the search
   * @param beginHour The initial hour in which the pitch should be able to reserved, inclusive
   * @param endHour   The final hour in which the pitch should be able to be reserved, inclusive
   * @param date      filter that will be used for the search
   * @return A list of all the WrapperComplex which contains at least one pitch that meets the request, null otherwise
   */
  List<CardInformation> getAvailable(Integer stateId, String name, Integer floor, Integer size, Integer beginHour,
                                     Integer endHour, String date);

  /**
   * The method searches for a pitch price in the database.
   *
   * @param complexId The id of the complex that is owner that is going to be searched
   * @param pitchId   The id of the pitch that is going to be searched
   * @param date      The date that is going to be searched to retrive the price
   * @param startTime The hour of the day (between 0 and 24) in which the price is being requested
   * @return The price of the required pitch in the required hour
   */
  Double getPitchPrice(int complexId, int pitchId, String date, int startTime);

  void persistPitch(Pitch pitch);

  void persistSchedule(TimeTable timeTable);
}
