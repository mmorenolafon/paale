<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='d' uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en" ng-app="registerApp">

<head>

    <script type="script/ng-template" id="error-messages">
        <span ng-message="required">This field is required.</span>
        <span ng-message="min">This number must be larger than {{min}}.</span>
        <span ng-message="max">This number must be smaller than {{max}}.</span>
        <span ng-message="number">A number is required.</span>
        <span ng-message="date">A date is required.</span>
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hay Equipo</title>

    <link rel="shortcut icon" href="<c:url value="/resources/img/rsz_logo.ico" />" type="image/x-icon"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.js"></script>

    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/js/register.js" />"></script>


    <!-- Bootstrap Core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">

    <!-- jQuery Version 1.11.1 -->
    <script src="<c:url value="/resources/js/jquery.js" />"></script>

    <!-- Bootstrap Core JavaScript -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="resources/js/bootstrap.min.js"></script>

    <!-- Custom CSS -->
    <link href="<c:url value="/resources/css/register.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/template.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/error.css" />" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- JS variables -->
    <input type="hidden" id="index_url" value="<c:url value="/index.html" />"/>


</head>

<body background="<c:url value="/resources/img/fondo1.jpg" />">

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/index.html" />">Hay Equipo</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#"></a>
                </li>
                <li>
                    <a href="#"></a>
                </li>
            </ul>

            <d:choose>
                <d:when test="${loggedUser==null}">
                    <div class="inline_div row">
                        <form class="form-inline " action="<c:url value="/index.html" />" method="post"
                              enctype="application/x-www-form-urlencoded">
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="username" name="j_username" type="text"
                                       placeholder="Usuario o email">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="password" name="j_password"
                                       type="password"
                                       placeholder="Contraseña">
                            </div>
                            <!--    <input name="j_rememberme" type="checkbox"> -->
                            <div class="col-md-4 col-xs-12 text-center">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <input class="btn btn-success-outline vertical-align login-btn btn-block"
                                               type="submit" value="Ingresar">
                                    </div>
                                    <div class="col-md-6 col-xs-12 padding-left-navbar2">
                                        <a href="<c:url value="/register.html" />"
                                           class="btn btn-success vertical-align register-btn btn-block padding-left-navbar">Registrarme</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </d:when>
                <d:otherwise>
                    <div>
                        <div class="greeting">
                            <a class="white-link" href="<c:url value="/profileDetails" />"> ${loggedUser} </a>
                        </div>
                        <div class="logout">
                            <a class="white-link" href="<c:url value="/logout" />"> Cerrar sesión </a>
                        </div>
                    </div>
                </d:otherwise>
            </d:choose>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row main-registration-container">
        <div class="col-lg-12 text-center">
            <div class="panel panel-success result-container my-container-blur2">
                <div class="panel-heading">
                    <div id="general-name">
                        Registrarme
                    </div>
                </div>

                <div ng-controller="controller" class="panel-body register-form-controller">
                    <form name="registerForm" class="panel-body register-form-controller">

                        <div class="form-group text-left">
                            <label>Nombre</label><span class="required-field">*</span>
                            <input class="form-control" type="text"
                                   name="firstname"
                                   required
                                   ng-model="controller.firstName"
                                   ng-maxlength="60"/>

                            <div ng-messages="registerForm.firstname.$error" ng-show="registerForm.firstname.$dirty"
                                 style="color:maroon" role="alert">
                                <div ng-messages-include="resources/html/ngMessages.html"></div>
                                <div ng-message="maxlength">El nombre no puede tener más de 60 caracteres.</div>
                            </div>
                        </div>

                        <div class="form-group text-left">
                            <label>Apellido</label><span class="required-field">*</span>
                            <input class="form-control" type="text"
                                   name="lastname"
                                   required
                                   ng-model="controller.lastname"
                                   ng-maxlength="60"/>

                            <div ng-messages="registerForm.lastname.$error" ng-show="registerForm.lastname.$dirty"
                                 style="color:maroon" role="alert">
                                <div ng-messages-include="resources/html/ngMessages.html"></div>
                                <div ng-message="maxlength">El apellido no puede tener más de 60 caracteres.</div>
                            </div>
                        </div>

                        <div class="form-group text-left">

                            <label>Dirección de email</label><span class="required-field">*</span>
                            <input class="form-control" type="email"
                                   name="email"
                                   required
                                   ng-model="controller.email"
                                   ng-maxlength="100"/>

                            <div ng-messages="registerForm.email.$error" ng-show="registerForm.email.$dirty"
                                 style="color:maroon" role="alert">
                                <div ng-messages-include="resources/html/ngMessages.html"></div>
                                <div ng-message="maxlength">La dirección de email no puede tener más de 100 caracteres.</div>
                            </div>
                        </div>

                        <div class="form-group text-left">

                            <label>Nombre de usuario</label>
                            <input class="form-control" type="text"
                                   name="username"
                                   ng-model="controller.username"
                                   ng-maxlength="20"/>

                            <div ng-messages="registerForm.username.$error" ng-show="registerForm.username.$dirty"
                                 style="color:maroon" role="alert">
                                <div ng-messages-include="resources/html/ngMessages.html"></div>
                                <div ng-message="maxlength">El nombre de usuario no puede tener más de 20 caracteres.</div>
                            </div>
                        </div>

                        <div class="form-group text-left">


                            <label>Teléfono</label><span class="required-field">*</span>
                            <input class="form-control" type="text"
                                   name="phone"
                                   required
                                   ng-model="controller.phone"
                                   ng-maxlength="20"/>

                            <div ng-messages="registerForm.phone.$error" ng-show="registerForm.phone.$dirty"
                                 style="color:maroon" role="alert">
                                <div ng-messages-include="resources/html/ngMessages.html"></div>
                                <div ng-message="maxlength">El número de teléfono no puede tener más de 20 caracteres.</div>

                            </div>
                        </div>

                        <div class="form-group text-left">

                            <label>Contraseña</label><span class="required-field">*</span>
                            <input class="form-control" type="password"
                                   name="password"
                                   required
                                   ng-model="controller.password"
                                   ng-minlength="7"
                                   ng-maxlength="50"/>

                            <div ng-messages="registerForm.password.$error" ng-show="registerForm.password.$dirty"
                                 style="color:maroon" role="alert">
                                <div ng-messages-include="resources/html/ngMessages.html"></div>
                                <span ng-message="maxlength">La contraseña no puede tener más de 50 caracteres.</span>
                                <span ng-message="minlength">La contraseña debe tener más de 7 caracteres.</span>
                            </div>
                        </div>

                        <div class="form-group text-left">

                            <label>Confirmar contraseña</label><span class="required-field">*</span>
                            <input class="form-control" type="password"
                                   name="confirmation"
                                   required
                                   ng-model="controller.confirmation"
                                   compare-to="controller.password"/>

                            <div ng-messages="registerForm.confirmation.$error"
                                 ng-show="registerForm.confirmation.$dirty"
                                 style="color:maroon" role="alert">
                                <div ng-messages-include="resources/html/ngMessages.html"></div>
                            </div>
                        </div>
                        <!--
                    <ng-messages ng-messages-include="error-messages" for="registerForm.firstname.$error"></ng-messages>

                        <div ng-messages="registerForm.firstname.$error" ng-messages-include="error-messages"></div>
                        <div ng-messages="registerForm.firstname.$error" ng-show="registerForm.firstname.$dirty" style="color:maroon" role="alert">
                            <div ng-message="required">Este campo es requerido</div>
                            <div ng-message="minlength">Este campo es muy corto</div>
                            <div ng-message="maxlength">Este campo es muy largo</div>
                            <div ng-repeat="errorMessage in errorMessages">
                                <!-- use ng-message-exp for a message whose key is given by an expression -->
                        <!--
                                        <div ng-message-exp="errorMessage.type">{{ errorMessage.text }}</div>
                                    </div>
                                </div>
                        -->

                        <div class="row">
                            <div class="col-lg-6 text-center">
                                <button id="back-btn" type="button" class="btn btn-default btn-block text-right">
                                    Volver
                                </button>
                            </div>
                            <div class="col-lg-6 text-center">
                                <input type="submit" value="Registrar" id="register-btn"
                                       class="btn btn-success btn-block text-left"/>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- /.row -->

</div>
<!-- /.container -->

</body>

</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>


</body>
</html>
