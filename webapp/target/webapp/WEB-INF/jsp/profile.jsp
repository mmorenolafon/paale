<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='d' uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <%@ page isELIgnored="false" %>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hay Equipo</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <link rel="shortcut icon" href="<c:url value="/resources/img/rsz_logo.ico" />" type="image/x-icon"/>


    <style>
        @font-face {
            font-family: 'Glyphicons Halflings';

            src: url("<c:url value="/resources/fonts/glyphicons-halflings-regular.eot" />");
            src: url("<c:url value="/resources/fonts/glyphicons-halflings-regular.eot?" />") format("embedded-opentype"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.woff2" />") format("woff2"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.woff" />") format("woff"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.ttf" />") format("truetype"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular" />") format("svg");
        }
    </style>


    <!-- Bootstrap Core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
    <script src="<c:url value="/resources/js/profile.js" />"></script>

    <!-- Custom CSS -->
    <link href="<c:url value="/resources/css/profile.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/template.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/error.css" />" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <input type="hidden" id="removebooking_url" value="<c:url value="/removebooking"/>"/>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <title>Hay Equipo</title>
</head>
<body background="<c:url value="/resources/img/fondo1.jpg" />">

      <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<c:url value="/index.html" />">Hay Equipo</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                    </ul>

                    <d:choose>
                        <d:when test="${loggedUser==null}">
                            <div class="inline_div row">
                                <form class="form-inline " action="<c:url value="/index.html" />" method="post"
                                      enctype="application/x-www-form-urlencoded">
                                    <div class="col-md-4 col-xs-12">
                                        <input class="form-control vertical-align" id="username" name="j_username" type="text"
                                               placeholder="Usuario o email">
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <input class="form-control vertical-align" id="password" name="j_password" type="password"
                                               placeholder="Contraseña">
                                    </div>
                                    <!--    <input name="j_rememberme" type="checkbox"> -->
                                    <div class="col-md-4 col-xs-12 text-center">
                                        <div class="row">
                                            <div class="col-md-6 col-xs-12">
                                                <input class="btn btn-success-outline vertical-align login-btn btn-block" type="submit" value="Ingresar">
                                            </div>
                                            <div class="col-md-6 col-xs-12 padding-left-navbar2">
                                                <a href="<c:url value="/register.html" />" class="btn btn-success vertical-align register-btn btn-block padding-left-navbar" >Registrarme</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </d:when>
                        <d:otherwise>
            <div>
              <div class="greeting">
                   <a class="white-link" href="<c:url value="/profileDetails" />"> ${loggedUser} </a>
              </div>
              <div class="logout">
                   <a class="white-link" href="<c:url value="/logout" />"> Cerrar sesión </a>
              </div>
            </div>
        </d:otherwise>
        </d:choose>

        </div>
        <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
        </nav>


<div id="mainContainer" class="panel panel-default my-container-blur">
    <p id="firstName">
        ${user.firstName} ${user.lastName}
    </p>
    <div class="row">
        <div class="col-sm-6">
            <div id="userDataContainer" class="panel panel-default">
                <p class="panelTitle">
                    Detalles
                </p>
                <div id="personalInfo">
                    <p>Nombre de usuario: ${user.username}</p>
                    <p>Dirección de email: ${user.email}</p>
                    <p>Teléfono: ${user.phone}</p>
                </div>
            </div>
            <div id="pending" class="panel panel-default">
                <p class="panelTitle">
                    Reservas
                </p>

                 <d:choose>
                 <d:when test="${empty reserves}">
                    No hay reservas pendientes.
                 </d:when>
                 <d:otherwise>


                    <c:forEach items="${reserves}" var="current">
                    <div class="reserve-registry row">
                        <div class="col-sm-4"><c:out value="${current.date}"/></div>
                        <div class="col-sm-2"><c:out value="${current.begin}:00hs"/></div>
                        <div class="col-sm-5"><c:out value="${current.complexName}"/></div>

                        <d:choose>
                        <d:when test="${current.day > reserveLimit}">
                            <div class="col-sm-1">
                                <button class="btn btn-success btn-xs delete-button" day="${current.date}" date="<c:out value="${current.day}"/>"
                                        time=<c:out value="${current.begin}"/> complex=<c:out value="${current.complexName}"/>
                                        pitchId=<c:out value="${current.pitchId}"/> complexId=<c:out value="${current.complexId}"/> >
                                <span class="glyphicon glyphicon-trash">
                                </span>
                                </button>
                            </div>
                        </d:when>
                        </d:choose>
                    </div>
                    </c:forEach>
                 </d:otherwise>
                 </d:choose>
            </div>
        </div>

        <div class="col-sm-6">
            <div id="history" class="panel panel-default">
                <p class="panelTitle">
                    Historial
                </p>

                 <d:choose>
                 <d:when test="${empty history}">
                    No hay reservas en el historial.
                 </d:when>
                 <d:otherwise>
                    <c:forEach items="${history}" var="current">
                    <div class="reserve-registry row">
                        <div class="col-sm-4"><c:out value="${current.date}"/></div>
                        <div class="col-sm-2"><c:out value="${current.begin}:00hs"/></div>
                        <div class="col-sm-4"><c:out value="${current.complexName}"/></div>
                    </div>
                    </c:forEach>
                 </d:otherwise>
                 </d:choose>

            </div>
        </div>
    </div>
</div>



      <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                      <h4 class="modal-title custom_align" id="Heading">Cancelar una reserva</h4>
                  </div>
                  <div class="modal-body" id="delete_entry">

                  </div>
                  <div class="modal-footer ">
                      <button type="button" class="btn btn-success" data-dismiss="modal">Volver</button>
                      <button type="button" id="delete_reserve" class="btn btn-default">Cancelar reserva</button>
                  </div>
              </div>
              <!-- /.modal-content -->
          </div>
      </div>

</body>
</html>
