<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='d' uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="<c:url value="/resources/img/rsz_logo.ico" />" type="image/x-icon"/>

    <title>Reservar cancha</title>

    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/js/register.js" />"></script>
    <script src="<c:url value="/resources/js/booking.js" />"></script>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <!-- Custom CSS -->
    <link href="<c:url value="/app/styles/modules/booking.css/css/booking.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/template.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/error.css" />" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <input type="hidden" id="requestLogin" value="<c:url value="/requestLogin" />"/>
    <input type="hidden" id="LOADING_URL" value="<c:url value="/resources/img/loading.gif"/>"/>


</head>

<body background="<c:url value="/resources/img/fondo1.jpg" />">
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/index.html" />">Hay Equipo</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

            </ul>

            <d:choose>
                <d:when test="${loggedUser==null}">
                    <div class="inline_div row">
                        <form class="form-inline " action="<c:url value="/index.html" />" method="post"
                              enctype="application/x-www-form-urlencoded">
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="username" name="j_username" type="text"
                                       placeholder="Usuario o email">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="password" name="j_password"
                                       type="password"
                                       placeholder="Contraseña">
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <input class="btn btn-success-outline vertical-align login-btn btn-block"
                                               type="submit" value="Ingresar">
                                    </div>
                                    <div class="col-md-6 col-xs-12 padding-left-navbar2">
                                        <a href="<c:url value="/register.html"/>"
                                           class="btn btn-success vertical-align register-btn btn-block padding-left-navbar">Registrarme</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </d:when>
                <d:otherwise>
                    <div>
                        <div class="greeting">
                            <a class="white-link" href="<c:url value="/profileDetails" />"> ${loggedUser} </a>
                        </div>
                        <div class="logout">
                            <a class="white-link" href="<c:url value="/logout" />"> Cerrar sesión </a>
                        </div>
                    </div>
                </d:otherwise>
            </d:choose>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="panel-booking-form text-center">
        <div class="panel panel-success result-container my-container-blur">
            <div class="panel-heading">
                <div id="general-name">
                    Detalles de la reserva
                </div>
            </div>
            <!-- Direccion, telefono, email, fecha, hora, suelo, precio
            -->
            <div class="panel-body" id="book-row">
                <div id="booking-form-container">
                    <c:url var="formPath" value="/confirmBooking"/>
                    <form:form modelAttribute="bookingConfirmationForm" method="post" action="${formPath}"
                               id="bookingForm">
                        <div class="row" id="data-container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group text-left row form-data">
                                        <form:label class="control-label" path="complexName">Complejo: </form:label>
                                        <d:out value="${bookingConfirmationForm.complexName}"></d:out>
                                    </div>
                                    <div class="form-group text-left row form-data">
                                        <form:label class="control-label" path="complexPhone">Teléfono: </form:label>
                                        <d:out value="${bookingConfirmationForm.complexPhone}"></d:out>
                                    </div>
                                    <div class="form-group text-left row form-data">
                                        <form:label class="control-label" path="complexEmail">Email: </form:label>
                                        <d:out value="${bookingConfirmationForm.complexEmail}"></d:out>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group text-left row form-data">
                                        <form:label class="control-label" path="date">Fecha: </form:label>
                                        <d:out value="${dateView}"></d:out>
                                    </div>
                                    <div class="form-group text-left row form-data">
                                        <form:label class="control-label" path="startTime">Hora: </form:label>
                                        <d:out value="${bookingConfirmationForm.startTime}:00"></d:out>
                                        -
                                        <d:out value="${bookingConfirmationForm.startTime+1}:00"></d:out>
                                    </div>
                                    <div class="form-group text-left row form-data">
                                        <form:label class="control-label" path="date">Suelo: </form:label>
                                        <d:out value="${bookingConfirmationForm.pitchFloor}"></d:out>
                                    </div>
                                    <div class="form-group text-left row form-data">
                                        <form:label class="control-label" path="pitchSize">Tamaño: </form:label>
                                        <d:out value="${bookingConfirmationForm.pitchSize}"></d:out>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                                <form:input type="hidden" path="date" id="formDate"/>
                                <form:input type="hidden" path="pitchId" id="formPitchId"/>
                                <form:input type="hidden" path="complexId" id="formComplexId"/>
                                <form:input type="hidden" path="startTime" id="formStartTime"/>
                            </div>
                            <div class="row">
                                <div class="form-group text-left form-data" id="price-data">
                                    <label class="control-label">Precio: </label>
                                    $<d:out value="${bookingConfirmationForm.price}"></d:out>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div id="confirm_form" class="row confirm_form">
                            <button type="button" value="Cancelar" id="back-btn" class="btn btn-default"
                                    data-container="body">Cancelar
                            </button>
                            <d:choose>
                                <d:when test="${loggedUser==null}">

                                    <!-- Trigger the modal with a button -->
                                    <button type="button" class="btn btn-success" data-toggle="modal"
                                            data-target="#preconfirm">Ingresar y confirmar reserva
                                    </button>

                                </d:when>
                                <d:otherwise>
                                    <button type="submit" value="Confirmar reserva" id="confirm_booking"
                                            class="btn btn-success " data-container="body" data-toggle="tooltip">
                                        Confirmar reserva
                                    </button>
                                </d:otherwise>
                            </d:choose>
                        </div>
                        <br>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<!-- Modal -->
<div id="preconfirm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ingresar y confirmar reserva</h4>
            </div>
            <form id="reconfirm_form" action="<c:url value="/index.html" />" method="post"
                  enctype="application/x-www-form-urlencoded">

                <div class="modal-body">
                    <div id="modal_confirm_form" class="row confirm_form">
                        <div class="col-md-5 col-xs-5">
                            <input class="form-control vertical-align" id="username" name="j_username" type="text"
                                   placeholder="Usuario o email">
                        </div>
                        <div class="col-md-5 col-xs-5">
                            <input class="form-control vertical-align" id="password" name="j_password" type="password"
                                   placeholder="Contraseña">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1 col-xs-1"></div>
                        <div class="col-md-9 col-xs-9">
                            <div id="reconfirm-message" class="form-warning"></div>
                        </div>
                        <div class="col-md-1 col-xs-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" value="Cancelar" id="back-btn" data-dismiss="modal" class="btn btn-default"
                            data-container="body">Cancelar
                    </button>

                    <button type="button" value="Confirmar reserva" id="reconfirm_booking" class="btn btn-success "
                            data-container="body" data-toggle="tooltip">Confirmar reserva
                    </button>
                    <button type="submit" id="hidden_resubmit"></button>
                </div>
            </form>

        </div>

    </div>
</div>

</html>
