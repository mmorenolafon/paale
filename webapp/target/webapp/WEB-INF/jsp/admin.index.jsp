<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix='d' uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hay Equipo</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script src="<c:url value="/resources/js/admin.index.js" />"></script>
    <script src="<c:url value="/resources/js/date-picker.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.js"/>"></script>

    <link rel="shortcut icon" href="<c:url value="/resources/img/rsz_logo.ico" />" type="image/x-icon"/>

    <style>
        @font-face {
            font-family: 'Glyphicons Halflings';

            src: url("<c:url value="/resources/fonts/glyphicons-halflings-regular.eot" />");
            src: url("<c:url value="/resources/fonts/glyphicons-halflings-regular.eot?#iefix" />") format("embedded-opentype"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.woff2" />") format("woff2"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.woff" />") format("woff"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.ttf" />") format("truetype"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular" />") format("svg");
        }
    </style>

    <!-- Bootstrap Core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/fonts/glyphicons-halflings-regular.eot" />" rel="stylesheet">

    <!-- Custom CSS -->

    <link href="<c:url value="/resources/css/template.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/admin.complexDetails.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/date-picker.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/admin.index.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/template.css" />" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <input type="hidden" id="getAllExceptions_url" value="<c:url value="/admin/getAllScheduleExceptions" />"/>
    <input type="hidden" id="addScheduleException_url" value="<c:url value="/admin/addScheduleException" />"/>
    <input type="hidden" id="getComplexDetails_url" value="<c:url value="/admin/getComplexDetails" />"/>
    <input type="hidden" id="deleteScheduleException_url" value="<c:url value="/admin/deleteScheduleException" />"/>


    <input type="hidden" id="bookingByComplex_url" value="<c:url value="/admin/bookingByComplex" />"/>
    <input type="hidden" id="removeBooking_url" value="<c:url value="/admin/bookingRemove" />"/>
    <input type="hidden" id="viewAll_url" value="<c:url value="/admin/complexPitches" />"/>
    <input type="hidden" id="reserve_url" value="<c:url value="/admin/booking" />"/>
    <input type="hidden" id="loadingIcon" value="<c:url value="/resources/img/loading.gif"/>"/>


</head>

<body background="<c:url value="/resources/img/fondo1.jpg" />">
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/admin/index.html" />">Hay Equipo</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

            </ul>

            <d:choose>
                <d:when test="${loggedUser==null}">
                    <div class="inline_div row">
                        <form class="form-inline " action="<c:url value="/admin/index.html" />" method="post"
                              enctype="application/x-www-form-urlencoded">
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="username" name="j_username" type="text"
                                       placeholder="Usuario o email">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="password" name="j_password"
                                       type="password"
                                       placeholder="Contraseña">
                            </div>
                            <!--    <input name="j_rememberme" type="checkbox"> -->
                            <div class="col-md-4 col-xs-12 text-center">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <input class="btn btn-success-outline vertical-align login-btn btn-block"
                                               type="submit" value="Ingresar">
                                    </div>
                                    <div class="col-md-6 col-xs-12 padding-left-navbar2">
                                        <a href="<c:url value="/register.html" />"
                                           class="btn btn-success vertical-align register-btn btn-block padding-left-navbar">Registrarme</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </d:when>
                <d:otherwise>
                    <div>
                        <div class="greeting">
                            <a class="white-link" href="<c:url value="/admin/profileDetails" />"> ${loggedUser} </a>
                        </div>
                        <div class="logout">
                            <a class="white-link" href="<c:url value="/logout" />"> Cerrar sesión </a>
                        </div>
                    </div>
                </d:otherwise>
            </d:choose>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-1">

                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <ul class="sidebar-nav">
                        <li class="sidebar-brand">
                            Panel de Control
                        </li>
                        <li id="complex_selector">
                            <div id="complex_label">Complejo</div>
                            <select class="form-control-complex form-control" id="form_complex">
                                <d:forEach items="${complexes}" var="current">
                                    <option id="${current.id}">${current.name}</option>
                                </d:forEach>
                            </select>
                        </li>
                        <li>
                            <a href="#" id="reserves_btn">Reservas</a>
                        </li>
                        <li>
                            <a href="#" id="status_btn">Detalles de Complejo</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-10 col-xs-12 col-xs-offset-1">
                <div class="panel panel-success result-container my-container-blur2" id="panel-content">
                    <div id="reserves" class="tabbable full-width-tabs">
                        <ul class="nav nav-tabs">
                            <li class="active" id="reserve-tab"><a data-toggle="tab" href="#Reservar">Reservas</a></li>
                            <li id="available-tab"><a data-toggle="tab" href="#Disponibles">Disponibles</a></li>
                        </ul>
                        <div class="row">
                            <div class="col-md-5 col-xs-12 text-center">
                                <div class="row text-center">
                                    <h4>Fecha</h4>
                                </div>
                                <div class="row text-center">
                                    <input type="hidden" id="2-date-picker-input"/>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6">
                                        <div class="row text-center">
                                            <h4>Desde</h4>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10 col-xs-10 col-xs-offset-1">
                                                <fieldset class="form-group width-full">
                                                    <select class="form-control form-control-begin"
                                                            id="form-control-begin">

                                                    </select>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <div class="row text-center">
                                            <h4>Hasta</h4>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10 col-xs-10 col-xs-offset-1">
                                                <fieldset class="form-group width-full">
                                                    <select class="form-control form-control-end" id="form-control-end">

                                                    </select>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12 padding-input">
                                        <div class="row text-center">
                                            <h4>Nombre de Usuario</h4>
                                        </div>
                                        <form id="formName">
                                            <input type="text" class="form-control margin-bottom" id="usr">
                                            <input type="button" style="display: none">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content my-overflow">
                            <div id="Disponibles" class="tab-pane fade">
                                <div class="row text-center">
                                    <div class="col-xs-2 padding-top">
                                        <h5>Hora</h5>
                                    </div>
                                    <div class="col-xs-2 padding-top">
                                        <h5>Cancha</h5>
                                    </div>
                                    <div class="col-xs-2 padding-top">
                                        <h5>Tamaño</h5>
                                    </div>
                                    <div class="col-xs-2 padding-top">
                                        <h5>Suelo</h5>
                                    </div>
                                    <div class="col-xs-2 padding-top">
                                        <h5>Precio</h5>
                                    </div>
                                </div>
                                <div class="availableToAppend">
                                    <div class="loading">
                                        <img src="<c:url value="/resources/img/loading.gif"/>"/> Cargando
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div id="Reservar" class="tab-pane bookings active">
                                <div class="row text-center table-header">
                                    <div class="col-xs-2 padding-top">
                                        <h5>Fecha</h5>
                                    </div>
                                    <div class="col-xs-2 padding-top">
                                        <h5>Hora</h5>
                                    </div>
                                    <div class="col-xs-2 padding-top">
                                        <h5>Cancha</h5>
                                    </div>
                                    <div class="col-xs-2 padding-top">
                                        <h5>Pagó</h5>
                                    </div>
                                    <div class="col-xs-2 padding-top">
                                        <h5>Usuario</h5>
                                    </div>
                                </div>
                                <div class="my-border-bottom">
                                </div>
                                <div class="bookingToAppend">
                                    <div class="loading">
                                        <img src="<c:url value="/resources/img/loading.gif"/>"/> Cargando
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Complex details-->
                    <div id="status" style="display: none;">
                        <div class="panel-body">
                            <div class="row">
                                <h1 class="content-title">Detalles del complejo</h1>
                                <br>
                                <div class="row" id="complex-details-form-row">
                                    <c:url var="formPath" value="/admin/modifyComplexField"/>
                                    <form:form modelAttribute="complexDetailsForm" method="post" action="${formPath}"
                                               id="complexDetailsFormId" role="form">
                                        <form:input path="complexId" name="complexId" type="hidden"
                                                    value="${complexDetailsForm.complexId}"/>
                                        <div class="filter-details">
                                            <div>
                                                <form:label path="address">Dirección: </form:label>
                                                <form:input path="address" name="address" type="hidden"
                                                            value="${complexDetailsForm.address}"/>
                                                <d:out value="${complexDetailsForm.address}"></d:out>
                                            </div>
                                            <div>
                                                <form:label path="phone">Teléfono: </form:label>
                                                <d:choose>
                                                    <d:when test="${phoneHasErrors==1}">
                                                        <form:input path="phone" name="phone" type="text"
                                                                    class="form-control input-enabled-background"
                                                                    id="phone"
                                                                    readonly="false"
                                                                    value="${complexDetailsForm.phone}"/>
                                                    </d:when>
                                                    <d:otherwise>
                                                        <form:input path="phone" name="phone" type="text"
                                                                    class="form-control modify-field-input "
                                                                    id="phone"
                                                                    readonly="false"
                                                                    value="${complexDetailsForm.phone}"/>
                                                    </d:otherwise>
                                                </d:choose>
                                                <button type="button" name="phone"
                                                        class="btn btn-default btn-circle btn-modify"
                                                        data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i>
                                                </button>
                                            </div>
                                            <form:errors path="phone" class="form-warning" element="div"/>
                                            <div>
                                                <form:label path="email">Email: </form:label>
                                                <d:choose>
                                                    <d:when test="${emailHasErrors==1}">
                                                        <form:input path="email" name="email" type="text"
                                                                    class="form-control input-enabled-background"
                                                                    id="email"
                                                                    readonly="true"
                                                                    value="${complexDetailsForm.email}"/>
                                                    </d:when>
                                                    <d:otherwise>
                                                        <form:input path="email" name="email" type="text"
                                                                    class="form-control modify-field-input "
                                                                    id="email"
                                                                    readonly="false"
                                                                    value="${complexDetailsForm.email}"/>
                                                    </d:otherwise>
                                                </d:choose>
                                                <button type="button" name="email"
                                                        class="btn btn-default btn-circle btn-modify"
                                                        data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i>
                                                </button>
                                            </div>
                                            <form:errors path="email" class="form-warning" element="div"/>
                                            <br>
                                        </div>
                                        <button type="submit" id="modify-complex-submit" class="btn btn-success"
                                                data-container="body">
                                        </button>
                                    </form:form>
                                </div>
                            </div>
                            <br>
                            <div class="my-border-bottom">
                            </div>
                            <br>
                            <div class="row">
                                <h1 class="content-title">Fechas de cierre</h1>
                                <br>
                                <!-- input fields -->
                                <div class="row text-center special-conditions-data">
                                    <div class="col-md-5 col-xs-12">
                                        <div class="row">
                                            <h4>Desde</h4>
                                        </div>
                                        <div class="row">
                                            <input type="hidden" id="4-date-picker-input"/>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                        <div class="row">
                                            <h4>Hasta</h4>
                                        </div>
                                        <div class="row">
                                            <input type="hidden" id="5-date-picker-input"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-12" id="special-condition-add-btn-row">
                                        <button id="special-condition-add-btn" type="button" class="btn btn-success">
                                            Agregar
                                        </button>
                                    </div>
                                </div>
                                <!-- table -->
                                <br>
                                <br>
                                <div class="row text-center special-conditions-data table-responsive">
                                    <table class="table table-hover" id="special-conditions-table">
                                        <thead>
                                        <tr>
                                            <th class="col-md-5 text-center">Desde</th>
                                            <th class="col-md-5 text-center">Hasta</th>
                                            <th class="col-md-2 text-center">Eliminar</th>
                                        </tr>
                                        </thead>
                                        <tbody class="table-body-schedule-exceptions">
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

</div>
<!-- /.container -->


<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Cancelar una reserva</h4>
            </div>
            <div class="modal-body" id="delete_entry">

            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-success" data-dismiss="modal">Volver</button>
                <button type="button" id="delete_reserve" class="btn btn-default">Cancelar reserva</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<div class="modal fade" id="delete-condition-modal" tabindex="-1" role="dialog" aria-labelledby="edit"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                        class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="delete-condition-heading">Eliminar condición especial</h4>
            </div>
            <div class="modal-body" id="delete-condition-entry">
                ¿Estás seguro que querés eliminar la condición especial?
            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
                <button type="button" id="delete-condition-confirm" class="btn btn-success">Eliminar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>


<!-- jQuery Version 1.11.1 -->
<script src="<c:url value="/resources/js/jquery.js"/>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

</body>


</html>