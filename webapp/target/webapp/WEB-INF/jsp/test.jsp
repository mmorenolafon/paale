<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!doctype html>
<html>
<head>
    <title>Tabs</title>
    <link rel="stylesheet" type="text/css" href='${pageContext.request.contextPath}/css/template.css" />'>
</head>
<body>
<h1 id="title">Tabs</h1>
<button id="theButton">add as new</button>
<input type="text" id="newName">
<div id="tabs"/>
</body>
</html>