<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix='c' uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='d' uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hay Equipo</title>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>


    <!-- jQuery Version 1.11.1 -->
    <script src="<c:url value="/resources/js/jquery.js" />"></script>


    <!-- Bootstrap Core JavaScript -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="resources/js/bootstrap.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/complexPitches.css" />" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<c:url value="/resources/css/template.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/error.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/complexPitches.js" />"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- JS variables -->
    <input type="hidden" id="reserve_url" value="<c:url value="/booking" />"/>
    <input type="hidden" id="index_url" value="<c:url value="/index.html" />"/>
</head>


<body background="<c:url value="/resources/img/fondo1.jpg" />">
<!-- Page Content -->
<div class="container">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<c:url value="/index.html" />">Hay Equipo</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#"></a>
                    </li>
                    <li>
                        <a href="#"></a>
                    </li>
                </ul>

                <d:choose>
                    <d:when test="${loggedUser==null}">
                        <div class="inline_div row">
                            <form class="form-inline " action="<c:url value="/index.html" />" method="post"
                                  enctype="application/x-www-form-urlencoded">
                                <div class="col-md-4 col-xs-12">
                                    <input class="form-control vertical-align" id="username" name="j_username"
                                           type="text"
                                           placeholder="Usuario o email">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <input class="form-control vertical-align" id="password" name="j_password"
                                           type="password"
                                           placeholder="Contraseña">
                                </div>
                                <!--    <input name="j_rememberme" type="checkbox"> -->
                                <div class="col-md-4 col-xs-12 text-center">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <input class="btn btn-success-outline vertical-align login-btn btn-block"
                                                   type="submit" value="Ingresar">
                                        </div>
                                        <div class="col-md-6 col-xs-12 padding-left-navbar2">
                                            <a href="<c:url value="/register.html" />"
                                               class="btn btn-success vertical-align register-btn btn-block padding-left-navbar">Registrarme</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </d:when>
                    <d:otherwise>
                        <div>
                            <div class="greeting">
                                <a class="white-link" href="<c:url value="/profileDetails" />"> ${loggedUser} </a>
                            </div>
                            <div class="logout">
                                <a class="white-link" href="<c:url value="/logout" />"> Cerrar sesión </a>
                            </div>
                        </div>
                    </d:otherwise>
                </d:choose>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="panel panel-success result-container my-container-blur2">
                    <div class="panel-heading">
                        <div id="general-name">
                            ${complexName}
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <div class=" filter-details">


                                    <div>
                                        <label>Complejo: </label>
                                        <span>${complexName}</span>
                                    </div>
                                    <div>
                                        <label>Dirección: </label>
                                        <span>${complexAddress}</span>
                                    </div>
                                    <div>
                                        <label>Teléfono: </label>
                                        <span>${complexPhone}</span>
                                    </div>
                                    <div>
                                        <label>Email: </label>
                                        <span>${complexEmail}</span>
                                    </div>
                                    <br>
                                    <div>
                                        <label>Fecha: </label>
                                        <span>${filterDay}</span>
                                    </div>
                                    <div>
                                        <label>Suelo: </label>
            <span><c:choose>
                <c:when test="${empty floor}">Todos</c:when><c:otherwise>${floor}</c:otherwise>
            </c:choose>
          </span></div>
                                    <div>
                                        <label>Tamaño: </label>
            <span><c:choose>
                <c:when test="${empty filterSize}">Todos</c:when><c:otherwise>${filterSize}</c:otherwise>
            </c:choose>
          </span>
                                    </div>

                                </div>

                                <button id="back-btn" type="button" class="btn btn-default btn-block text-right">
                                    Volver
                                </button>

                            </div>
                            <div class="col-lg-8 text-center">
                                <table class="table" id="pitchTable">
                                    <thead>
                                    <tr>
                                        <th class="<c:choose>
              <c:when test="${empty floor}">col-md-3</c:when><c:otherwise>col-md-4</c:otherwise>
            </c:choose>">Hora
                                        </th>
                                        <th class="<c:choose>
              <c:when test="${empty floor}">col-md-3</c:when><c:otherwise>col-md-4</c:otherwise>
            </c:choose>">Precio
                                        </th>
                                        <c:if test="${empty floor}">
                                            <th class="col-md-3">Suelo ${floor}</th>
                                        </c:if>
                                        <th class="<c:choose>
            <c:when test="${empty floor}">col-md-3</c:when><c:otherwise>col-md-4</c:otherwise>
          </c:choose>">Reservar
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${pitches}" var="current">
                                        <tr>
                                            <td class="<c:choose>
          <c:when test="${empty floor}">col-md-3</c:when><c:otherwise>col-md-4</c:otherwise>
        </c:choose>"><c:out value="${current.startTime}:00 hs"/></td>
                                            <td class="priceTag <c:choose>
          <c:when test="${empty floor}">col-md-3</c:when><c:otherwise>col-md-4</c:otherwise>
        </c:choose>"><c:out value="$${current.price}"/></td>
                                            <c:if test="${empty floor}">
                                                <td class="col-md-3"><c:out value="${current.floorName}"/></td>
                                            </c:if>
                                            <td class="<c:choose>
        <c:when test="${empty floor}">col-md-3</c:when><c:otherwise>col-md-4</c:otherwise>
      </c:choose>">


                                                <button name="${current.pitchId}" startTime="${current.startTime}"
                                                        class="btn-block btn btn-success btn-block text-left reserve-btn ">
                                                    Reservar
                                                </button>
                                            </td>
                                        </tr>
                                    </c:forEach>

                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container -->

</body>

</html>
