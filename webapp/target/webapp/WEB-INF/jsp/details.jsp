<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='d' uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hay Equipo</title>
    <link rel="shortcut icon" href="<c:url value="/resources/img/rsz_logo.ico" />" type="image/x-icon"/>


    <!-- jQuery Version 1.11.1 -->
    <script src="<c:url value="/resources/js/jquery.js" />"></script>

    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="http://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>


    <script src="<c:url value="/resources/js/complex.js" />"></script>
    <!-- Custom CSS -->
    <link href="<c:url value="/resources/css/template.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/complex.css" />" rel="stylesheet">

    <link href="<c:url value="/resources/css/error.css" />" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body background="<c:url value="/resources/img/fondo1.jpg" />">
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/index.html" />">Hay Equipo</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

            </ul>

            <d:choose>
                <d:when test="${loggedUser==null}">
                    <div class="inline_div row">
                        <form class="form-inline " action="<c:url value="/index.html" />" method="post"
                              enctype="application/x-www-form-urlencoded">
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="username" name="j_username" type="text"
                                       placeholder="Usuario o email">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="password" name="j_password"
                                       type="password"
                                       placeholder="Contraseña">
                            </div>
                            <!--    <input name="j_rememberme" type="checkbox"> -->
                            <div class="col-md-4 col-xs-12 text-center">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <input class="btn btn-success-outline vertical-align login-btn btn-block"
                                               type="submit" value="Ingresar">
                                    </div>
                                    <div class="col-md-6 col-xs-12 padding-left-navbar2">
                                        <a href="<c:url value="/register.html" />"
                                           class="btn btn-success vertical-align register-btn btn-block padding-left-navbar">Registrarme</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </d:when>
                <d:otherwise>
                    <div>
                        <div class="greeting">
                            <a class="white-link" href="<c:url value="/profileDetails" />"> ${loggedUser} </a>
                        </div>
                        <div class="logout">
                            <a class="white-link" href="<c:url value="/logout" />"> Cerrar sesión </a>
                        </div>
                    </div>
                </d:otherwise>
            </d:choose>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>


<!-- Page Content -->
<div class="container">
    <div class="panel panel-success result-container my-container-blur">
        <div class="panel-heading">
            <div id="complex-name">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">

                <div class="panel-body">
                    <div class="col-stats">
                        <div class="stat">
                            <strong>Telefono: </strong><span id="phone"></span>
                        </div>
                        <div class="stat">
                            <strong>Dirección: </strong><span id="address"></span>
                        </div>
                        <div class="stat">
                            <strong>Email: </strong><span id="email"></span>
                        </div>
                    </div>
                    <div class="col-pitches">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Número</th>
                                <th>Tamaño</th>
                                <th>Suelo</th>
                                <th>Precios</th>
                            </tr>
                            </thead>
                            <tbody id="table-content">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="book-row">


            <button type="button" id="start_booking" class="btn btn-success collapsed" data-toggle="collapse"
                    data-target="#book" aria-expanded="false">Realizar reserva
            </button>
            <form:form modelAttribute="bookingData" method="post" action="/booking" id="bookingForm">
                <div id="book" class="collapse">

                    <div id="book-form-container" class="row">
                        <div class="col-lg-1 text-center"></div>
                        <div class="col-lg-5 text-left">
                            <fieldset class="form-group">
                                <label for="form_size">Tamaño</label>
                                <select class="form-control" id="form_size">
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-lg-5 text-left">
                            <div style="overflow:hidden;">
                                <fieldset class="form-group">
                                    <label for="form_floor">Suelo</label>
                                    <select class="form-control" id="form_floor">
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-lg-1 text-center"></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 text-left"></div>

                        <div class="col-lg-10 text-left">
                            <label for="form_size">Dia y hora</label>
                            <div id="datetimepicker"></div>
                        </div>
                        <div class="col-lg-1 text-left"></div>

                    </div>
                    <button type="button" id="available_pitches" class="btn btn-success collapsed"
                            data-toggle="dropdown"
                            data-target="#availables" aria-expanded="false">Ver canchas disponibles
                    </button>


                    <div class="col-lg-1 text-center"></div>


                    <div id="availables" class="dropdown">
                        <div class="row">
                            <div class="col-lg-1 text-left"></div>

                            <div class="col-lg-10 text-left">
                                <div id="available_container">
                                </div>
                                <div id="final_form" class="invisible">


                                    <d:choose>
                                        <d:when test="${loggedUser==null}">
                                            <div><br>Para realizar una reserva debe registrarse. <br><br>
                                                <a href="/index.html" class="btn btn-success" role="button">Volver al
                                                    inicio</a>
                                            </div>
                                        </d:when>
                                        <d:otherwise>
                                            <button type="button" value="Submit" id="end_booking"
                                                    class="btn btn-success">Finalizar reserva
                                            </button>
                                        </d:otherwise>
                                    </d:choose>

                                </div>
                                <div class="col-lg-1 text-left"></div>
                            </div>


                        </div>

                    </div>

                </div>

                <form:input type="hidden" path="date" id="formDate"/>
                <form:input type="hidden" path="pitchId" id="formPitchId"/>
                <form:input type="hidden" path="complexId" id="formComplexId"/>
                <form:input type="hidden" path="startTime" id="formStartTime"/>

            </form:form>

        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container -->


<!-- Modal -->
<div class="modal fade" id="timetable_modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="timetable_header"></h4>
            </div>
            <div class="modal-body" id="timetable_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>

</div>

</body>

</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>