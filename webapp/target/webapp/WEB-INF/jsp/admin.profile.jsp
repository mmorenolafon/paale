<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='d' uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <%@ page isELIgnored="false" %>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hay Equipo</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <link rel="shortcut icon" href="<c:url value="/resources/img/rsz_logo.ico" />" type="image/x-icon"/>


    <!-- Bootstrap Core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
    <!-- Custom CSS -->
    <link href="<c:url value="/resources/css/profile.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/template.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/error.css" />" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <title>Hay Equipo</title>
</head>
<body background="<c:url value="/resources/img/fondo1.jpg" />">

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/admin/index.html" />">Hay Equipo</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

            </ul>

            <d:choose>
                <d:when test="${loggedUser==null}">
                    <div class="inline_div row">
                        <form class="form-inline " action="<c:url value="/admin/index.html" />" method="post"
                              enctype="application/x-www-form-urlencoded">
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="username" name="j_username" type="text"
                                       placeholder="Usuario o email">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="password" name="j_password"
                                       type="password"
                                       placeholder="Contraseña">
                            </div>
                            <!--    <input name="j_rememberme" type="checkbox"> -->
                            <div class="col-md-4 col-xs-12 text-center">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <input class="btn btn-success-outline vertical-align login-btn btn-block"
                                               type="submit" value="Ingresar">
                                    </div>
                                    <div class="col-md-6 col-xs-12 padding-left-navbar2">
                                        <a href="<c:url value="/register.html" />"
                                           class="btn btn-success vertical-align register-btn btn-block padding-left-navbar">Registrarme</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </d:when>
                <d:otherwise>
                    <div>
                        <div class="greeting">
                            <a class="white-link" href="<c:url value="/admin/profileDetails" />"> ${loggedUser} </a>
                        </div>
                        <div class="logout">
                            <a class="white-link" href="<c:url value="/logout" />"> Cerrar sesión </a>
                        </div>
                    </div>
                </d:otherwise>
            </d:choose>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>


<div id="mainContainer" class="panel panel-default my-container-blur">
    <p id="firstName">
        ${user.firstName} ${user.lastName}
    </p>
    <div class="row">
        <div class="col-sm-12">
            <div id="userDataContainer" class="panel panel-default">
                <p class="panelTitle">
                    Detalles
                </p>
                <div id="personalInfo">
                    <p>Nombre de usuario: ${user.username}</p>
                    <p>Dirección de email: ${user.email}</p>
                    <p>Teléfono: ${user.phone}</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
