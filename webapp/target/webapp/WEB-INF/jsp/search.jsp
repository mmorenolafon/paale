<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix='d' uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en" ng-app="searchApp">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hay Equipo</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <link rel="shortcut icon" href="<c:url value="/resources/img/rsz_logo.ico" />" type="image/x-icon"/>

    <style>
        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url("<c:url value="/resources/fonts/glyphicons-halflings-regular.eot" />");
            src: url("<c:url value="/resources/fonts/glyphicons-halflings-regular.eot?#iefix" />") format("embedded-opentype"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.woff2" />") format("woff2"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.woff" />") format("woff"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.ttf" />") format("truetype"),
            url("<c:url value="/resources/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular" />") format("svg");
        }
    </style>

    <script src="<c:url value="/resources/js/jquery.js"/>"></script>
    <script src="<c:url value="/resources/js/date-picker.js"/>"></script>
    <script src="<c:url value="/resources/js/angular-search.js"/>"></script>

    <!-- jQuery Version 1.11.1 -->
    <!-- <script src="<c:url value="/resources/js/searchComplex.js" />"></script> -->
    <!-- Bootstrap Core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/date-picker.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/fonts/glyphicons-halflings-regular.eot" />" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<c:url value="/resources/css/template.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/error.css" />" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <input type="hidden" id="states_url" value="<c:url value="/states" />"/>
    <input type="hidden" id="floor_url" value="<c:url value="/floor" />"/>
    <input type="hidden" id="complexId_url" value="<c:url value="/complexId" />"/>
    <input type="hidden" id="reserve_url" value="<c:url value="/booking" />"/>
    <input type="hidden" id="viewAll_url" value="<c:url value="/complexPitches" />"/>


</head>

<body background="<c:url value="/resources/img/fondo1.jpg" />">
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/index.html" />">Hay Equipo</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

            </ul>

            <d:choose>
                <d:when test="${loggedUser==null}">
                    <div class="inline_div row">
                        <form class="form-inline " action="<c:url value="/index.html" />" method="post"
                              enctype="application/x-www-form-urlencoded">
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="username" name="j_username" type="text"
                                       placeholder="Usuario o email">
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <input class="form-control vertical-align" id="password" name="j_password"
                                       type="password"
                                       placeholder="Contraseña">
                            </div>
                            <!--    <input name="j_rememberme" type="checkbox"> -->
                            <div class="col-md-4 col-xs-12 text-center">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <input class="btn btn-success-outline vertical-align login-btn btn-block"
                                               type="submit" value="Ingresar">
                                    </div>
                                    <div class="col-md-6 col-xs-12 padding-left-navbar2">
                                        <a href="<c:url value="/register.html" />"
                                           class="btn btn-success vertical-align register-btn btn-block padding-left-navbar">Registrarme</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </d:when>
                <d:otherwise>
                    <div>
                        <div class="greeting">
                            <a class="white-link" href="<c:url value="/profileDetails" />"> ${loggedUser} </a>
                        </div>
                        <div class="logout">
                            <a class="white-link" href="<c:url value="/logout" />"> Cerrar sesión </a>
                        </div>
                    </div>
                </d:otherwise>
            </d:choose>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <div class="row my" ng-controller="FilterController as filters">
                <div class="col-md-3 thumbnail my-container-blur">
                    <h5 class="text-center">Fecha</h5>
                    <div class="row text-center">
                        <input type="hidden" id="3-date-picker-input"/>
                    </div>
                    <div class="my-border-bottom">
                    </div>
                    <div class="row">
                        <h5 class="text-center ">Hora de inicio</h5>
                    </div>
                    <div class="row filters-margin">
                        <div class="col-md-6 col-xs-12">
                            <div class="row text-center">
                                <h6>Desde</h6>
                            </div>
                            <div class="row">
                                <div class="col-md-10 col-xs-10 col-xs-offset-1">
                                    <fieldset class="form-group">
                                        <select class="form-control form-control-begin" id="form-control-begin">
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="row text-center">
                                <h6>Hasta</h6>
                            </div>
                            <div class="row">
                                <div class="col-md-10 col-xs-10 col-xs-offset-1">
                                    <fieldset class="form-group">
                                        <select class="form-control form-control-end" id="form-control-end">

                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my-border-bottom">
                    </div>
                    <div class="row my-text-left-2">
                        <h5 class="filters-margin text-center">Tamaño</h5>
                    </div>
                    <div class="row my-text-left-2">
                        <div class="col-xs-8 col-xs-offset-2">
                            <fieldset class="form-group">
                                <%--<select class="form-control form-control-size" id="form-control-size">--%>
                                <%--</select>--%>
                                <select ng-model="filters.sizes" ng-options="size for size in filters.sizes"
                                        class="form-control-size form-control" id="form-control-size">
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    <div class="my-border-bottom">
                    </div>
                    <div class="row my-text-left-2">
                        <h5 class="filters-margin text-center">Barrio</h5>
                    </div>
                    <div class="row filters-margin my-text-left-2">
                        <div class="col-xs-8 col-xs-offset-2">
                            <fieldset class="form-group">
                                <select ng-model="filters.states" ng-options="state.id as state.name for state in filters.states"
                                        class="form-control-states form-control" id="form_states">
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    <div class="my-border-bottom">
                    </div>
                    <div class="row my-text-left-2">
                        <h5 class="filters-margin text-center">Suelo</h5>
                    </div>
                    <div class="row filters-margin my-text-left-2 ">

                        <div class="col-xs-8 col-xs-offset-2">
                            <fieldset class="form-group">
                                <select class="form-control-floors form-control" id="form_floor">
                                </select>
                            </fieldset>
                        </div>
                    </div>

                </div>
                <div class="col-md-9">
                    <div ng-controller="ResultsController as resCont" class="panel panel-success result-container my-container-blur">
                        <div class="panel-body results-body">
                            <div ng-repeat="result in resCont.results" class="panel panel-default result padding-left my-overflow">
                                <div class = "row">
                                    <div class = "col-md-8 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-5 col-xs-12">
                                                <div class = "row padding-left-card">
                                                    <div class="complex-location item item2 ">
                                                        <strong> {{result.name}} </strong>
                                                    </div>
                                                </div>
                                                <div class = "row padding-left-card">
                                                    <div class="complex-location item item2 ">
                                                        <p>{{result.address}}</p><p> {{result.state}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12 max-heigth1">
                                                <div class = "row">
                                                    <div class="complex-location item item2 padding-left-card">
                                                        <p><strong>  Hora: </strong>{{result.time}}:00</p>
                                                    </div>
                                                </div>
                                                <div class = "row">
                                                    <div class="complex-location item item2 padding-left-card">
                                                        <p><strong>  Suelo: </strong>{{result.floor}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3  col-xs-12 max-heigth2">
                                                <div class="complex-location item item3 text-center">
                                                    <strong>
                                                        {{result.price}}
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div  href="#" class="col-md-4 col-xs-12" complexId="' + wrapcomplex.complexId + wrapcomplex.availablePitch.pitchId + '" begin="' + wrapcomplex.availablePitch.startTime + '" floor="' + wrapcomplex.availablePitch.floor + '" >
                                        <div class="row booking result-booking white-text border-card1">
                                            <div class="col-xs-12 text-center vertical-align-booking">
                                                <span class="glyphicon glyphicon-ok element-button-icon" aria-hidden="true"></span>
                                                <p class="element-button-text">Reservar</p>
                                            </div>
                                        </div>
                                        <div href="#" class="row view-all result-view white-text border-card2">
                                            <div class="col-xs-12 text-center vertical-align-booking">
                                                <span class="glyphicon glyphicon-th-list element-button-icon" aria-hidden="true"></span>
                                                <p class="element-button-text">Ver todas</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="loading">
                                <img src="<c:url value="/resources/img/loading.gif"/>"/> Cargando
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.row -->

</div>
<!-- /.container -->

<!-- jQuery Version 1.11.1 -->
<script src="<c:url value="/resources/js/jquery.js"/>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

</body>

</html>
