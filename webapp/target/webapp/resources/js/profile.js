$(document).ready(function () {

    $(".delete-button").click(function () {

        $(".selected_button").removeClass("selected_button");

        $(this).addClass("selected_button");

        var body = '¿Estás seguro que querés cancelar la reserva en el complejo <span class="modal-data">' + $(this).attr("complex") +
            '</span> el día <span class="modal-data">' + $(this).attr("day") + '</span> a las <span class="modal-data">' +
            $(this).attr("time") + ':00hs</span>?';
        $("#delete_entry").html(body);

        $('#delete').modal('show');

        $("#delete_reserve").click(function () {
            var button = $(".selected_button").addClass("selected_button");

            getRequest(
                $("#removebooking_url").val() + "?complexId=" + button.attr("complexId") + "&pitchId=" + button.attr("pitchId") + "&date=" + button.attr("date")
                + "&startTime=" + button.attr("time"), // URL for the PHP file
                deleted,  // handle successful request
                reqError   // handle error
            );
        });
    });
});

function deleted(data) {
    var ans = JSON.parse(data);
    if (ans.result == "true") {
        $('#delete').modal('hide');
        $('.selected_button').parent().parent().slideUp(200).promise().done(function () {
            $('.selected_button').parent().parent().remove().promise().done(function () {
                if ($('#pending').children().length == 1) {
                    $("#pending").append("No hay reservas pendientes.");
                }
            });
        });
    } else {
        $("#delete_entry").html("No fue posible eliminar la reserva, lo sentimos.<br>Intentalo nuevamente más tarde")
    }
}

function getRequest(url, success, error) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}


function reqError(status) {

    var msg = '<div id="general-name">';
    msg += 'Error inesperado';
    msg += '</div>';
    msg += '<div class="error-body">';
    msg += '<div id="error-details">';
    msg += '¡Ups! No es posible comunicarse con el servidor.';
    msg += '</div>';
    msg += '</div>';
    $("#delete_entry").html(msg);
}