var request_filters = null;
var filt_state = undefined;
var filt_floor = undefined;
var filt_name = undefined;
var filt_endHour = undefined;
var filt_day = undefined;
var filt_beginHour = undefined;
var filt_size = undefined;

$(document).ready(function () {

    generateDatePicker(3, false, datePickerListener);

    var params = "?";
    var floor = $_GET("floor");
    var day = $_GET("day");
    var capacity = $_GET("capacity");
    var beginHour = $_GET("beginHour");
    var stateId = $_GET("stateId");
    var endHour = $_GET("endHour");

    if (day == undefined) {
        var prevParams = sessionStorage.getItem("searchParams");
        floor = $_GET("floor", prevParams);
        day = $_GET("day", prevParams);
        capacity = $_GET("capacity", prevParams);
        beginHour = $_GET("beginHour", prevParams);
        stateId = $_GET("stateId", prevParams);
        endHour = $_GET("endHour", prevParams);
    }


    if (floor != "todos" && floor != null) {
        filt_floor = floor;
        params += "floor=" + floor + "&";
    }
    filt_size = capacity;

    params += "capacity=" + capacity;

    if (stateId != "todos" && stateId != null) {
        filt_state = stateId;
        params += "&stateId=" + stateId;
    }

    filt_beginHour = beginHour;
    filt_endHour = endHour;
    filt_day = day;
    params += "&beginHour=" + beginHour;
    params += "&endHour=" + endHour;
    params += "&day=" + day;


    $("#complex_name_form").submit(function (event) {
        updateByFilters('name', $('#complex_name').val());
    });

    getRequest(
        $("#states_url").val(), // URL for the PHP file
        drawAvailable,  // handle successful request
        reqError,    // handle error
        'form_states'
    );

    getRequest(
        $("#floor_url").val(), // URL for the PHP file
        drawAvailable2,  // handle successful request
        reqError,    // handle error
        'form_floor'
    );

    getRequest(
        $("#complexId_url").val() + params, // URL for the PHP file
        parseComplex,  // handle successful request
        reqError,    // handle error
        'results-body'
    );

    $('.form-control-floors').on('change', function (e) {
        updateByFilters('floor', $('option:selected', this).attr("id").split('-')[1]);
    });

    $('.form-control-states').on('change', function (e) {
        updateByFilters('stateId', $('option:selected', this).attr("id").split('-')[1]);
    });

    $('.form-control-size').on('change', function (e) {
        updateByFilters('capacity', $('option:selected', this).attr("value"));
    });

    $('.form-control-begin').on('change', function (e) {
        limitEndHour($('option:selected', this).attr("value"));
    });

    $('.form-control-end').on('change', function (e) {
        var hour = parseInt($('option:selected', this).attr("value")) + 1;
        updateByFilters('endHour', hour.toString());
    });

    updateFiltersValue(capacity, beginHour, endHour, day);
});

function updateByFilters() {
    var ask = true;
    filt_endHour = $(".form-control-end").val();

    filt_day = $("#3-date-picker-input").attr("value");
    filt_beginHour = $("#form-control-begin").val();
    filt_size = $("#form-control-size").val();
    filt_floor = $("option:selected", "#form_floor").attr("id").split("-")[1];
    filt_state = $("option:selected", "#form_states").attr("id").split("-")[1];

    if (ask) {
        var filters = "";
        if (filt_state != undefined && filt_state != "todos") {
            filters += "stateId=" + filt_state;
        }
        if (filt_floor != undefined && filt_floor != "todos") {
            if (filters != "") {
                filters += "&";
            }
            filters += "floor=" + filt_floor;
        }
        if (filt_size != undefined) {
            if (filters != "") {
                filters += "&";
            }
            filters += "capacity=" + filt_size;
        }
        if (filt_name != undefined) {
            if (filters != "") {
                filters += "&";
            }
            filters += "name=" + filt_name;
        }
        if (filt_beginHour != undefined) {
            if (filters != "") {
                filters += "&";
            }
            filters += "beginHour=" + filt_beginHour;
        }
        if (filt_endHour != undefined) {
            if (filters != "") {
                filters += "&";
            }
            filters += "endHour=" + filt_endHour;
        }
        if (filt_day != undefined) {
            if (filters != "") {
                filters += "&";
            }
            filters += "day=" + filt_day;
        }
        if (request_filters != null) {
            request_filters.abort();
        }


        request_filters = getRequest(
            $("#complexId_url").val() + "?" + filters, // URL for the PHP file
            filterComplex,  // handle successful request
            reqError,    // handle error
            'results-body'
        );

    }
}


var fullComplexList;

function parseComplex(data, container) {
    sessionStorage.setItem("complex", data);

    fullComplexList = JSON.parse(data);
    drawComplex(fullComplexList, container);
}

function filterComplex(data, container) {

    var idVec = JSON.parse(data);
    drawComplex(idVec, container);
}


function drawComplex(complexes, container) {
    var arrayLength = complexes.length;
    var full = "";

    for (var i = 0; i < arrayLength; i++) {
        full += getCard(complexes[i]);
    }
    if (arrayLength % 2 == 1) {
        full += '<div class="panel panel-default result result-no-border"></div>';
    }

    if (complexes.length == 0) {
        notifyNoResults(1, container);
        return;
    }
    $("." + container).html(full);

    $('.booking').on("click", function () {
        var complexId = $(this).parent().attr("complexId");
        var pitchId = $(this).parent().attr("pitchId");
        var floor = $(this).parent().attr("floor");
        var begin = $(this).parent().attr("begin");

        var params = bookingRequest(complexId, pitchId, begin);

        window.location = $("#reserve_url").val() + "?" + params;
    });

    $('.view-all').on("click", function () {
        var complexId = $(this).parent().attr("complexId");
        var pitchId = $(this).parent().attr("pitchId");
        var floor = $(this).parent().attr("floor");
        var begin = $(this).parent().attr("begin");

        var params = viewAllRequest(complexId);

        sessionStorage.setItem("searchParams", getCurrentParams());
        window.location = $("#viewAll_url").val() + "?" + params;
    });

}


function reqError(status, container) {

    var msg = '<div id="general-name">';
    msg += 'Error inesperado';
    msg += '</div>';
    msg += '<div class="error-body">';
    msg += '<div id="error-details">';
    msg += '¡Ups! No es posible comunicarse con el servidor.';
    msg += '</div>';
    msg += '</div>';
    $("." + container).html(msg);
}

function notifyNoResults(status, container) {

    var msg = '<div id="general-name">';
    msg += 'No hay resultados';
    msg += '</div>';
    msg += '<div class="error-body">';
    msg += '<div id="error-details">';
    msg += 'No encontramos canchas con esas características.';
    msg += '</div>';
    msg += '</div>';
    $("." + container).html(msg);
}

function getCard(wrapcomplex) {

    var ans = '<div class="panel panel-default result padding-left my-overflow">';
    ans += '<div class = "row">';
    ans += '	<div class = "col-md-8 col-xs-12">';
    ans += '		<div class="row">';
    ans += '			<div class="col-md-5 col-xs-12">';
    ans += '				<div class = "row padding-left-card">';
    ans += '					<div class="complex-location item item2 ">';
    ans += '<strong>' + wrapcomplex.name + '</strong>';
    ans += '					</div>';
    ans += '				</div>';
    ans += '				<div class = "row padding-left-card">';
    ans += '					<div class="complex-location item item2 ">';
    ans += '<p>' + wrapcomplex.address + '</p><p>' + wrapcomplex.state + '</p>';
    ans += '					</div>';
    ans += '				</div>';
    ans += '			</div>';
    ans += '			<div class="col-md-4 col-xs-12 max-heigth1">';
    ans += '				<div class = "row">';
    ans += '					<div class="complex-location item item2 padding-left-card">';
    ans += '						<p><strong>  Hora: </strong>' + wrapcomplex.availablePitch.startTime + ':00</p>';
    ans += '					</div>';
    ans += '				</div>';
    ans += '				<div class = "row">';
    ans += '					<div class="complex-location item item2 padding-left-card">';
    ans += '						<p><strong>  Suelo: </strong>' + wrapcomplex.availablePitch.floorName + '</p>';
    ans += '					</div>';
    ans += '				</div>';
    ans += '			</div>';
    ans += '			<div class="col-md-3  col-xs-12 max-heigth2">';
    ans += '				<div class="complex-location item item3 text-center">';
    ans += '						<strong>';
    ans += '$ ' + wrapcomplex.availablePitch.price;
    ans += '						</strong>';
    ans += '				</div>';
    ans += '			</div>';
    ans += '		</div>';
    ans += '	</div>';
    ans += '	<div  href="#" class="col-md-4 col-xs-12" complexId="' + wrapcomplex.complexId + '" pitchId="' + wrapcomplex.availablePitch.pitchId + '" begin="' + wrapcomplex.availablePitch.startTime + '" floor="' + wrapcomplex.availablePitch.floor + '" >';
    ans += '		<div class="row booking result-booking white-text border-card1">';
    ans += '					<div class="col-xs-12 text-center vertical-align-booking">';
    ans += '						<span class="glyphicon glyphicon-ok element-button-icon" aria-hidden="true"></span>' +
        '<p class="element-button-text">Reservar</p>';
    ans += '					</div>';
    ans += '		</div>';
    ans += '		<div href="#" class="row view-all result-view white-text border-card2">';
    ans += '					<div class="col-xs-12 text-center vertical-align-booking">';
    ans += '						<span class="glyphicon glyphicon-th-list element-button-icon" aria-hidden="true"></span>' +
        '<p class="element-button-text">Ver todas</p>';
    ans += '					</div>';
    ans += '		</div>';
    ans += '	</div>';
    ans += '</div>';
    ans += '</div>';

    return ans;

}

// helper function for cross-browser request object
function getRequest(url, success, error, container) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText, container) : error(req.status, container);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}


function $_GET(param, source) {
    var vars = {};
    if (source == undefined) {
        source = window.location.href.replace(location.hash, '');
    }

    source.replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function (m, key, value) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if (param) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}


function drawAvailable2(data, id) {
    var floors = JSON.parse(data);
    var body = "";
    body += '<option id = allFloors >Todos</option>';
    for (var i = 0; i < floors.length; i++) {
        if (floors[i].id == filt_floor) {
            body += '<option id =floor-' + floors[i].id + ' selected>' + floors[i].name + '</option>';
        } else {
            body += '<option id =floor-' + floors[i].id + '>' + floors[i].name + '</option>';
        }
    }
    $("#" + id).html(body);
}

function drawAvailable(data, id) {
    var state = JSON.parse(data);
    var body = "";
    body += '<option id = allStates >Todos</option>';
    for (var i = 0; i < state.length; i++) {
        if (state[i].id == filt_state) {
            body += '<option id =state-' + state[i].id + ' selected>' + state[i].name + '</option>';
        } else {
            body += '<option id =state-' + state[i].id + '>' + state[i].name + '</option>';
        }

    }
    $("#" + id).html(body);
}


function limitEndHour(begin) {
    var endHour = $("#form-control-end").val();

    begin = parseInt(begin);

    var endContent = "";
    for (var i = begin + 1; i < 25; i++) {
        if (i == endHour) {
            endContent += "<option value=" + i + " selected>" + i + ":00</option>";
        } else {
            endContent += "<option value=" + i + ">" + i + ":00</option>";
        }
    }

    $("#form-control-end").html(endContent);
    updateByFilters();


}

function updateFiltersValue(capacity, beginHour, endHour, day) {

    var end;
    var beginContent = "";
    var endContent = "";
    updateDate(3, day);


    for (var i = 8; i < 24; i++) {
        if (i == beginHour) {
            beginContent += "<option value=" + i + " selected>" + i + ":00</option>";
        } else {
            beginContent += "<option value=" + i + ">" + i + ":00</option>";
        }
        if (i >= beginHour) {
            end = i + 1;

            if (end == endHour) {
                endContent += "<option value=" + end + " selected>" + end + ":00</option>";
            } else {
                endContent += "<option value=" + end + ">" + end + ":00</option>";
            }
        }


        $("#form-control-begin").html(beginContent);
        $("#form-control-end").html(endContent);


    }

    for (i = 5; i < 12; i += 2) {
        if (i == capacity) {
            $("#form-control-size").append("<option value=" + i + " selected>" + i + "</option>");
        } else {
            $("#form-control-size").append("<option value=" + i + ">" + i + "</option>");
        }
    }

}

function bookingRequest(complexId, pitchId, begin) {
    var date = $("#3-date-picker-input").attr("value");
    var params = "";
    params += "complexId=" + complexId;
    params += "&pitchId=" + pitchId;
    params += "&startTime=" + begin;
    params += "&date=" + date;
    return params;


}


function viewAllRequest(complexId) {
    var date = $("#3-date-picker-input").attr("value");
    var floor = $("option:selected", "#form_floor").attr("id").split("-")[1];
    var params = "";
    params += "complexId=" + complexId;
    params += "&beginHour=" + $('.form-control-begin').val();
    params += "&endHour=" + $('.form-control-end').val();
    params += "&date=" + date;
    if (floor != "todos" && floor != undefined) {
        params += "&floor=" + floor;
    }
    params += "&capacity=" + $('.form-control-size').val();

    return params;
}

function datePickerListener() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    var date = yyyy + "-" + mm + "-" + dd;
    if (date == $("#3-date-picker-input").attr("value")) {
        limitBeginHour(today.getHours() + 1);
    } else {
        limitBeginHour(8);
    }
}


function limitBeginHour(currentHour) {
    var beginHour = $("#form-control-begin").val();
    var beginContent = "";
    for (var i = currentHour; i < 24; i++) {
        if (i == beginHour) {

            beginContent += "<option value=" + i + " selected>" + i + ":00</option>";
        } else {
            beginContent += "<option value=" + i + ">" + i + ":00</option>";
        }
    }
    $("#form-control-begin").html(beginContent);
    limitEndHour(currentHour > beginHour ? currentHour : beginHour);
}

function getCurrentParams() {

    var filters = "";
    if (filt_state != undefined && filt_state != "todos") {
        filters += "stateId=" + filt_state;
    }
    if (filt_floor != undefined && filt_floor != "todos") {
        if (filters != "") {
            filters += "&";
        }
        filters += "floor=" + filt_floor;
    }
    if (filt_size != undefined) {
        if (filters != "") {
            filters += "&";
        }
        filters += "capacity=" + filt_size;
    }
    if (filt_name != undefined) {
        if (filters != "") {
            filters += "&";
        }
        filters += "name=" + filt_name;
    }
    if (filt_beginHour != undefined) {
        if (filters != "") {
            filters += "&";
        }
        filters += "beginHour=" + filt_beginHour;
    }
    if (filt_endHour != undefined) {
        if (filters != "") {
            filters += "&";
        }
        filters += "endHour=" + filt_endHour;
    }
    if (filt_day != undefined) {
        if (filters != "") {
            filters += "&";
        }
        filters += "day=" + filt_day;
    }
    if (request_filters != null) {
        request_filters.abort();
    }
    return "/saved?" + filters;
}