$(document).ready(function () {

    getRequest(
        $("#states_url").val(), // URL for the PHP file
        drawAvailable,  // handle successful request
        reqError,    // handle error
        'form_states'
    );

    getRequest(
        $("#floor_url").val(), // URL for the PHP file
        drawAvailable,  // handle successful request
        reqError,    // handle error
        'form_floor'
    );

    updateFiltersValue(8, 24);

    $('.form-control-begin').on('change', function (e) {
        limitEndHour($('option:selected', this).attr("value"));
    });

    $("#available_pitches").click(function () {
        var filters = getFilterRequest();
        window.location = $("#search_url").val() + "?" + filters;
    });

    $("#valoration-panel").animate({"top": "50px"}, 1100);
    $(".thumb-button").click(function () {
        $("#valoration-panel").animate({"top": "-30px"}, 1100);
        getRequest($(this).attr("url") + "&complexId=" + $("#valoration-complexid").val() + "&date=" + $("#valoration-date").val());
    });

});


function limitEndHour(begin) {
    var endHour = $(".form-control-end option:selected").val();
    var endContent = "";
    for (var i = begin; i < 24;) {
        i++;
        if (i == endHour && endHour > begin) {
            endContent += "<option value=" + i + " selected>" + i + ":00</option>";
        } else {
            endContent += "<option value=" + i + ">" + i + ":00</option>";

        }
    }

    $(".form-control-end").html(endContent);

}

function updateFiltersValue(beginHour, endHour) {

    var end;
    var beginContent = "";
    var endContent = "";

    for (var i = 8; i < 24; i++) {
        if (i == beginHour) {
            beginContent += "<option value=" + i + " selected>" + i + ":00</option>";
        } else {
            beginContent += "<option value=" + i + ">" + i + ":00</option>";
        }
        if (i >= beginHour) {
            end = i + 1;

            if (end == endHour) {
                endContent += "<option value=" + end + " selected>" + end + ":00</option>";
            } else {
                endContent += "<option value=" + end + ">" + end + ":00</option>";
            }
        }

        $(".form-control-begin").html(beginContent);
        $(".form-control-end").html(endContent);
    }
}


function getFilterRequest() {
    var ans = "";

    var floors = $(".form-control-floors option:selected").attr("id");
    ans += "floor=" + floors;

    var state = $(".form-control-states option:selected").attr("id");
    ans += "&stateId=" + state;

    var capacity = $(".form-control-size option:selected").attr("value");
    ans += "&capacity=" + capacity;

    var begin = $(".form-control-begin option:selected").attr("value");
    ans += "&beginHour=" + begin;

    var endHour = $(".form-control-end option:selected").attr("value");
    ans += "&endHour=" + endHour;

    var day = $("#1-date-picker-input").attr("value");
    ans += "&day=" + day;

    return ans;
}


function drawCombo(data, container) {
    sessionStorage.setItem(container, data);

    var items = JSON.parse(data);
    var arrayLength = items.length;
    var full = "<option value=''> Todos </options>";
    for (var i = 0; i < arrayLength; i++) {
        full += "<option value=" + items[i].id + ">" + items[i].name + "</option>"
    }

    $("#" + container).html(full);

    $('#state .dropdown-menu li > a').click(function (e) {
        $('#selected-state').text(this.innerHTML);
        updateByFilters('state', $(this).attr("id"));
    });

    $('#floor .dropdown-menu li > a').click(function (e) {
        $('#selected-floor').text(this.innerHTML);
        updateByFilters('floor', $(this).attr("id"));
    });
}


function reqError(status, container) {

    var msg = '<div id="general-name">';
    msg += 'Error inesperado';
    msg += '</div>';
    msg += '<div class="error-body">';
    msg += '<div id="error-details">';
    msg += '¡Ups! No es posible comunicarse con el servidor.';
    msg += '</div>';
    msg += '</div>';
    $("." + container).html(msg);
}

function getRequest(url, success, error, container) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText, container) : error(req.status, container);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}


function drawAvailable(data, id) {
    var floors = JSON.parse(data);
    var body = "";
    body += '<option id = todos >Todos</option>';
    for (var i = 0; i < floors.length; i++) {
        body += '<option id = ' + floors[i].id + '>' + floors[i].name + '</option>';
    }
    $("#" + id).html(body);
}