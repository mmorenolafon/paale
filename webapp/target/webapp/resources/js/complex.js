$(document).ready(function () {

    var id = getParameterByName('id');
    if (id == undefined) {
        window.location = "/error.html?cd=2";
    } else {
        sessionStorage.setItem('curComplexId', id);
    }

    populateFormTimes();

    var complexes = sessionStorage.getItem('complex');

    if (complexes != undefined) {
        selectComplex(complexes, id);
    } else {
        getRequest(
            '/complex/' + id, // URL for the PHP file
            parseComplex,  // handle successful request
            reqError,
            id
        );
    }

    $("#available_pitches").click(function () {
        requestFilterPitch(id);
    });

    $("#start_booking").click(function () {
        setTimeout(function () {
            $('html, body').animate({
                scrollTop: $("#book-row").offset().top
            }, 2000);

            $("#available_pitches").click(function () {
                setTimeout(function () {
                    $('html, body').animate({
                            scrollTop: $("#availables").offset().top
                        }
                        , 2000);
                }, 10);
            });


        }, 10);
    });


    $("#end_booking").click(function () {
        endBooking(id);
    });

});

function selectComplex(complexes, id) {
    var complex = JSON.parse(complexes);
    var selected = undefined;
    var arrayLength = complex.length;

    for (var i = 0; selected == undefined && i < arrayLength; i++) {
        if (complex[i].id == id) {
            selected = complex[i];
        }
    }

    if (selected == undefined) {
        window.location = "/error.html?cd=3";
    }

    drawComplex(selected, id);
};

function parseComplex(data, id) {
    var complex = JSON.parse(data);

    if (complex.length == 0) {
        window.location = "/error.html?cd=3";
    } else {
        drawComplex(complex, id);
    }
}

function requestFilterPitch(id) {

    var floor = $("#form_floor").find("option:selected").attr("value");
    var capacity = $("#form_size").find("option:selected").text();

    var date = $('#datetimepicker').data("DateTimePicker").date();
    if (date == undefined) {
        date = new Date();
    } else {
        date = date.toDate();
    }
    var day = (date.getDay() + 6) % 7;
    var beginHour = date.getHours();


    getRequest(
        '/pitchToReserve?floor=' + floor + '&capacity=' + capacity + '&complexId=' + id + '&beginHour=' + beginHour + '&endHour=' + ((beginHour + 1) % 24) + '&day=' + day, // URL for the PHP file
        drawAvailable,  // handle successful request
        reqError,
        id
    );

}

function drawComplex(selected, id) {
    $('#complex-name').html(selected.name);
    $('#phone').html(selected.phone);
    $('#address').html(selected.address + "," + selected.state);
    $('#email').html(selected.email);

    getRequest(
        '/pitch?complexId=' + id, // URL for the PHP file
        drawPitch,  // handle successful request
        reqError,
        id
    );

}

function drawAvailable(data, id) {
    var pitches = JSON.parse(data);

    if (pitches.length == 0) {
        $("#available_container").html("<div>No se encontraron canchas para los criterios deseados.</div>")

        $("#final_form").addClass("invisible");
    } else {

        $("#final_form").removeClass("invisible");

        var body = '<fieldset class="form-group">';
        body += '<label for="form_size">Selecciona una cancha de las disponibles:</label>';
        body += '<select class="form-control" id="pitches_selector">);';
        for (var i = 0; i < pitches.length; i++) {
            body += '<option pitchId=' + pitches[i].pitchId + ' price=' + pitches[i].price + '>Cancha ' + pitches[i].pitchId + ' - $' + pitches[i].price + '</option>';
        }
        body += '</select>';
        body += '</fieldset>';
        $("#available_container").html(body);
    }

}


function endBooking() {

    var date = $('#datetimepicker').data("DateTimePicker").date().toDate();
    var beginHour = date.getHours();
    var pitchSelected = $("#pitches_selector").find("option:selected");

    var pitchId = pitchSelected.attr("pitchId");
    var complexId = sessionStorage.getItem('curComplexId');

    var name = $("#form_first_name").val();
    var surname = $("#form_last_name").val();
    var email = $("#form_email").val();

    $("#formDate").attr("value", JSON.stringify(date));
    $("#formPitchId").attr("value", pitchId);
    $("#formComplexId").attr("value", complexId);
    $("#formStartTime").attr("value", beginHour);

    document.forms["bookingForm"].submit();
}


function drawPitchTimeTable(data, pitchId) {
    var colors = ['#77ddb4', '#77c8dd', '#778bdd', '#9f77dd', '#dd77dc', '#dd779f', '#dd8b77', '#ddc877', '#b4dd77'];

    var time_blocks = JSON.parse(data);

    var sign_table = [7];
    for (var i = 0; i < 24; i++) {
        sign_table[i] = [];
    }

    var prices = [];
    for (var i = 0; i < time_blocks.length; i++) {
        var block = time_blocks[i];
        var day = block.day;
        for (var j = block.begin; j < block.end; j++) {
            sign_table[day][j] = block.price;
            prices.push(block.price);
        }
    }
    var prices = jQuery.unique(prices);

    var priceColor = {};
    for (var i = 0; i < prices.length; i++) {
        priceColor[prices[i]] = colors[i];
    }

    var table = '<div class="container timetable-container">';
    table += '<div class="row"><div class="col-sm-1"/><div class="col-sm-2"/><div class="col-sm-1 no-padding-line">L</div><div class="col-sm-1 no-padding-line">M</div><div class="col-sm-1 no-padding-line">M</div><div class="col-sm-1 no-padding-line">J</div><div class="col-sm-1 no-padding-line">V</div><div class="col-sm-1 no-padding-line">S</div><div class="col-sm-1 no-padding-line">D</div><div class="col-sm-2"/></div>';

    for (var hour = 0; hour < 24; hour++) {
        table += '<div class="row">';
        table += '<div class="col-sm-1"></div>';
        table += '<div class="col-sm-2 no-padding-line">' + hour + ':00</div>';

        for (var day = 0; day < 7; day++) {

            table += '<div class="col-sm-1 no-padding-line">';

            if (sign_table[day][hour] != undefined) {
                table += '<a href="#" title="$' + sign_table[day][hour] + '" class="tooltip">';
                table += '<div class="time_block" style="background-color:' + priceColor[sign_table[day][hour]] + '">';
                table += '</a>';
            } else {
                table += '<div class="time_block">';
            }
            table += '</div>';
            table += '</div>';

        }

        table += '<div class="col-sm-2"></div>';
        table += '</div>';
    }

    table += '<div class="price_references"><div class="references">Referencias</div>';


    table += '<div class="time_block time_reference no-padding-line">Cerrada </div>';

    for (var price in priceColor) {
        if (priceColor.hasOwnProperty(price)) {
            table += '<div class="time_block time_reference no-padding-line" style="background-color:' + priceColor[price] + '"> $' + price + '</div>';
        }
    }
    table += '</div>';

    $("#timetable_header").html("Tabla de Precios - Cancha " + pitchId);
    $("#timetable_body").html(table);

}

function drawPitch(data, id) {
    var pitch = JSON.parse(data);
    var arrayLength = pitch.length;

    var size = [];
    var floor = {};
    var floorNames = [];

    var full = "";
    for (var i = 0; i < arrayLength; i++) {
        var p = pitch[i];
        full += getTableRow(p);
        size.push(p.size);

        floor[p.floor] = p.floorId;
        floorNames.push(p.floor);
    }

    completeForm(jQuery.unique(size), jQuery.unique(floorNames), floor);
    $("#table-content").html(full);


    $('.timetable-trigger').click(function () {
        var pitchId = $(this).attr("name");

        $("#timetable_header").html("Cargando precios de la cancha " + pitchId);

        $("#timetable_body").html('<div class="loading"><img src="./../../resources/img/loading.gif"></div>');

        $("#timetable_modal").modal();


        getRequest(
            '/pitch/timetable?pitchId=' + pitchId + '&complexId=' + id, // URL for the PHP file
            drawPitchTimeTable,  // handle successful request
            reqError,
            pitchId
        );


    });


}

function completeForm(size, floor, floorMap) {
    var size_options = '<option value="">Todos</option>';
    for (var i = 0; i < size.length; i++) {
        size_options += "<option>" + size[i] + "</option>";
    }
    $("#form_size").html(size_options);


    var floor_options = '<option value="">Todos</option>';
    for (var i = 0; i < floor.length; i++) {
        floor_options += "<option value=" + floorMap[floor[i]] + ">" + floor[i] + "</option>";
    }
    $("#form_floor").html(floor_options);
}

function populateFormTimes() {
    $('#datetimepicker').datetimepicker({
        inline: true,
        sideBySide: true,
        stepping: 60,
        minDate: new Date(),
    });
}


function getTableRow(pitch) {
    var ans = '<tr>';
    ans += '<td>' + pitch.id + '</td>';
    ans += '<td>' + pitch.size + '</td>';
    ans += '<td>' + pitch.floor + '</td>';
    ans += '<td><button type="button" class="btn btn-success btn-sm timetable-trigger" name="' + pitch.id + '"><span class="glyphicon glyphicon-search"></span></button></td>';
    ans += '</tr>';
    return ans;
}


function reqError(status, container) {

    var msg = '<div id="general-name">';
    msg += 'Error inesperado';
    msg += '</div>';
    msg += '<div class="error-body">';
    msg += '<div id="error-details">';
    msg += '¡Ups! No es posible comunicarse con el servidor.';
    msg += '</div>';
    msg += '</div>';
    $("." + container).html(msg);
}


// helper function for cross-browser request object
function getRequest(url, success, error, id) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText, id) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}