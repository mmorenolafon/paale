$(document).ready(function () {

    var hostURL = "pawserver.it.itba.edu.ar";

    $("#back-btn").click(function () {
        var oldURL = document.referrer;

        if (oldURL != "" && oldURL.split('/')[2] != undefined || oldURL.split('/')[2] == hostURL) {
            window.location.href = oldURL.split("?")[0];
        } else {
            window.location.href = $("#index_url").val();
        }
    });

    $(".reserve-btn").click(function () {
        window.location = $("#reserve_url").val() + "?" + window.location.search.substring(1) + "&pitchId=" + this.name + "&startTime=" + $(this).attr("startTime");
    });

});
