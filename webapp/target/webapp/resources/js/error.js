$(document).ready(function () {

    var cd = getParameterByName('cd');
    if (cd == undefined) {
        return;
    }

    /*
     1: Broken server
     2: No id on the /details.html page
     3: No results on /details.html page
     4: Unable to book
     */

    switch (cd) {
        case "1":
            $("#general-name").html("Error inesperado");
            $("#error-details").html('¡Ups! No es posible comunicarse con el servidor.');
            break;
        case "2":
            $("#general-name").html("Complejo no encontrado");
            $("#error-details").html('No se encontró el complejo deseado.<br><a href="' + $("#INDEXURL").val() + '" class="btn btn-success" ' +
                'role="button">Volver al inicio</a>');
            break;
        case "3":
            $("#general-name").html("Complejo no encontrado");
            $("#error-details").html('No es posible mostrar los detalles del complejo deseado.<br>Por favor, ' +
                'inténtalo nuevamente más tarde.<br><a href="' + $("#INDEXURL").val() + '" class="btn btn-success" role="button">Volver al inicio</a>');
            break;
            0
        case "4":
            $("#general-name").html("No se pudo realizar la reserva");
            $("#error-details").html('En este momento no ha sido posible realizar la reserva. ' +
                'Por favor inténtalo nuevamente más tarde.<br><a href="' + $("#INDEXURL").val() + '" class="btn btn-success" role="button">Volver al inicio</a>');
            break;

    }

});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}