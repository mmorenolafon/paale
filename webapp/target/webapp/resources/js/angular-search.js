searchApp = angular.module('searchApp', []);

searchApp.controller('ResultsController', function () {
    this.results = [
        {
            name: 'la vida no es justa',
            address: 'Lavalleja 238',
            state: 'Villa Crespo',
            time: 18,
            floor: 'grass',
            price: 800.0
        },
        {
            name: 'un poco menos injusta',
            address: 'Lavalleja 239',
            state: 'Villa Crespo',
            time: 18,
            floor: 'grass',
            price: 800.0
        }];
});

searchApp.controller('FilterController', function () {
    this.states = [
        {name: 'Puerto Madero', id: 3}, {name: 'Villa Crespo', id: 4}
    ];
    this.sizes = [5, 6, 7, 8, 9, 10, 11];
    // this.sizes = [{size:5}, {size:6}, {size:7}, {size:8}, {size:9}, {size:10}, {size:11}];
    
});

