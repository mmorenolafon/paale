var modal_content;
var form_content;

$(document).ready(function () {

    $('#reconfirm_form').submit(function (e) {
        e.preventDefault();
        var url = $(this).closest('form').attr('action'),
            data = $(this).closest('form').serialize();
        loadingState();

        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function (data) {
                $.ajax({
                    url: $("#requestLogin").val(),
                    type: 'post',
                    success: function (request) {
                        if (request.trim() == "") {
                            undoLoading();
                            $("#reconfirm-message").text("El nombre de usuario y/o contraseña son incorrectos");
                        } else {
                            $('#bookingForm').submit();
                        }
                    },
                    error: function () {
                        undoLoading();
                        $("#reconfirm-message").text("El nombre de usuario y/o contraseña son incorrectos");
                    }
                });
            },
            error: function () {
                $("#reconfirm-message").text("Error inesperado del servidor. Intenta nuevamente más tarde");
            }
        });
    });

    $("#reconfirm_booking").click(function () {
        $('#reconfirm_form').submit();
    });

    $("#confirm_booking").click(function () {
        loadingState();
        $('#bookingForm').submit();
    });


});

function loadingState() {
    var content = '<div class="loading">' +
        '<img src="' + $("#LOADING_URL").val() + '" /> Procesando' +
        '</div>';

    modal_content = $("#modal_confirm_form").html();
    form_content = $("#confirm_form").html();

    $('.confirm_form').html(content);
}

function undoLoading() {
    $("#modal_confirm_form").html(modal_content);
    $("#confirm_form").html(form_content);
}