var reload_admin_index_page = false;
var request_admin_filters_available = null;
var request_admin_filters_booking = null;

$(document).ready(function () {

    $("#reserves_btn").click(function () {
        $('#status').fadeOut(200).promise().done(function () {
            $('#reserves').fadeIn(200);
        });

    });

    $("#status_btn").click(function () {
        $('#status').removeClass("hide");
        $('#reserves').fadeOut(200).promise().done(function () {
            $('#status').fadeIn(200);
        });
    });

    initializeDatePickers();
    setComplexFormListener();
    setModifyListener();

    $("#special-condition-add-btn").click(function () {
        var url = $("#addScheduleException_url").val();
        url += "?" + getSpecialConditionFilterRequest();
        getRequest(
            url,
            addScheduleException,  // handle successful request
            reqError,    // handle error
            'table-body-schedule-exceptions',
            "GET"
        );
    });

    updateFiltersValue(8, 24);
    loadSpecialConditionsTable();

    $(".form-control-complex").on('change', function (e) {
        updateByFilters();
        updateByFiltersAvailables();
        loadComplexDetails();
        loadSpecialConditionsTable();
    });

    $('.form-control-begin').on('change', function (e) {
        limitEndHour($('option:selected', this).attr("value"));
    });

    $('.form-control-end').on('change', function (e) {
        updateByFilters();
        updateByFiltersAvailables();
    });

    $("#2-date-picker-input").on('change', function (e) {
        updateByFilters();
        updateByFiltersAvailables();
    });

    $("#formName").submit(function (e) {
        e.preventDefault();
        updateByFilters();
    });

    $("#available-tab").on('click', function (e) {
        if (reload_admin_index_page) {
            updateByFiltersAvailables();

        }
    });

    $("#reserve-tab").on('click', function (e) {
        if (reload_admin_index_page) {
            updateByFilters();
        }
    });


    getRequest(
        $("#bookingByComplex_url").val() + "?" + getFilterRequest(), // URL for the PHP file
        parseBooking,  // handle successful request
        reqError,    // handle error
        'bookingToAppend',
        "GET"
    );

    getRequest(
        $("#viewAll_url").val() + "?" + getFilterRequest(),
        parseAvailable,
        reqError,
        'availableToAppend',
        "GET"
    );
});


function initializeDatePickers() {
    var dateForDp = new Date();
    dateForDp.setDate(dateForDp.getDate() + 15);
    var dd = dateForDp.getDate();
    var mm = dateForDp.getMonth() + 1;
    var yy = dateForDp.getFullYear();

    generateDatePicker(4, true, datePickerListenerSchedule, yy, mm, dd);
    generateDatePicker(5, true, undefined, yy, mm, dd);
    generateDatePicker(2, false, datePickerListener);
}


function setComplexFormListener() {
    $('#complexDetailsFormId').submit(function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            type: "post",
            data: data,
            success: function (response) {
                $('#phone').attr('readonly', 'true');
                $('#phone').addClass('modify-field-input');
                $('#phone').removeClass('input-enabled-background');
                $('#email').attr('readonly', 'true');
                $('#email').addClass('modify-field-input');
                $('#email').removeClass('input-enabled-background');
            },
            error: function () {
            }
        });
    });
}

function setModifyListener() {
    $('.btn-modify').click(function () {
        var id = $(this).attr('name');
        $(this).prev().removeAttr('readonly');
        $("#" + id).removeClass('modify-field-input');
        $("#" + id).addClass('input-enabled-background');
    });

}

function limitEndHour(begin) {
    var endHour = $("#form-control-end").val();

    begin = parseInt(begin);

    var endContent = "";
    for (var i = begin + 1; i < 25; i++) {
        if (i == endHour) {
            endContent += "<option value=" + i + " selected>" + i + ":00</option>";
        } else {
            endContent += "<option value=" + i + ">" + i + ":00</option>";
        }
    }

    $("#form-control-end").html(endContent);
    updateByFiltersAvailables();
    updateByFilters();

}

function updateFiltersValue(beginHour, endHour) {

    var end;
    var beginContent = "";
    var endContent = "";

    for (var i = 8; i < 24; i++) {
        if (i == beginHour) {
            beginContent += "<option value=" + i + " selected>" + i + ":00</option>";
        } else {
            beginContent += "<option value=" + i + ">" + i + ":00</option>";
        }
        if (i >= beginHour) {
            end = i + 1;

            if (end == endHour) {
                endContent += "<option value=" + end + " selected>" + end + ":00</option>";
            } else {
                endContent += "<option value=" + end + ">" + end + ":00</option>";
            }
        }

    }
    $("#form-control-begin").html(beginContent);
    $("#form-control-end").html(endContent);

}

function loadComplexDetails() {
    var complexId = $(".form-control-complex option:selected").attr("id");
    var url = $("#getComplexDetails_url").val() + "?complexId=" + complexId;
    getRequest(
        url,
        loadComplexDetailsHandler,  // handle successful request
        reqError,    // handle error
        'none',
        "GET"
    );
}

function loadComplexDetailsHandler(data, container) {
    $('#complexDetailsFormId').remove();
    var form = $(data).find('#complexDetailsFormId');
    $('#complex-details-form-row').append(form);
    setComplexFormListener();
    setModifyListener();
}

function loadSpecialConditionsTable() {
    var complexId = $(".form-control-complex option:selected").attr("id");
    var url = $("#getAllExceptions_url").val() + "?complexId=" + complexId;
    getRequest(
        url,
        addAllScheduleExceptions,  // handle successful request
        reqError,    // handle error
        'table-body-schedule-exceptions',
        "GET"
    );
}

function addAllScheduleExceptions(data, container) {
    var result = (JSON.parse(data)).data;

    if (result.length == 0) {
        notifyNoResults(1, container);
    } else {
        for (var i = 0; i < result.length; i++) {
            addScheduleExceptionJson(result[i], container);
        }
    }
}

function getSpecialConditionFilterRequest() {
    var ans = "";

    var complexId = $(".form-control-complex option:selected").attr("id");
    ;
    ans += "&complexId=" + complexId;

    var beginDate = $("#4-date-picker-input").attr("value");
    ans += "&beginDate=" + beginDate;

    var endDate = $("#5-date-picker-input").attr("value");
    ans += "&endDate=" + endDate;

    return ans;
}


function addScheduleExceptionJson(data, container) {
    if (data != undefined) {
        if ($(".table-body-schedule-exceptions").children('tr').length == 0) {
            $(".table-body-schedule-exceptions").empty();
        }

        var newRow = "<tr><td class='beginDate'>" + data.beginDate + "</td>";
        newRow += "<td class='endDate'>" + data.endDate + "</td>";
        newRow += "<td class='col-md-2 delete-condition'><a href='#'><span class='glyphicon glyphicon-trash'></span></a></td></tr>";

        $("." + container).append(newRow);


        $('.delete-condition').on("click", function () {

            $(".special_condition_selected").removeClass("special_condition_selected");

            $(this).addClass("special_condition_selected");

            $('#delete-condition-modal').modal('show');

            $("#delete-condition-confirm").click(function () {
                var button = $(".special_condition_selected").addClass("special_condition_selected");
                var complexId = $(".form-control-complex option:selected").attr("id");
                var beginDate = button.closest('tr').children('td.beginDate').text();
                var endDate = button.closest('tr').children('td.endDate').text();

                var parameters = "complexId=" + complexId + "&beginDate=" + beginDate + "&endDate=" + endDate;
                getRequest(
                    $("#deleteScheduleException_url").val() + "?" + parameters, // URL for the PHP file
                    deleteScheduleException,  // handle successful request
                    reqError,    // handle error
                    button,
                    "GET"
                );
            })
        });

    }
}

function addScheduleException(data, container) {
    if (data != undefined) {
        var json = JSON.parse(data);
        if (!jQuery.isEmptyObject(json))
            addScheduleExceptionJson(json, container);
    }
}

function deleteScheduleException(data, container) {
    container.closest('tr').slideUp();
    container.closest('tr').remove();
    $('#delete-condition-modal').modal('hide');
    if ($('.table-body-schedule-exceptions').find('tr').length == 0) {
        notifyNoResults(1, 'table-body-schedule-exceptions');
    }
}

function getFilterRequest() {
    var ans = "";

    var complex = $(".form-control-complex option:selected").attr("id");
    ans += "complexId=" + complex;

    var begin = $("#form-control-begin").val();
    ans += "&startTime=" + begin;

    var endHour = $("#form-control-end").val();
    ans += "&endTime=" + endHour;

    var day = $("#2-date-picker-input").attr("value");
    ans += "&date=" + day;

    return ans;
}

function updateByFilters() {
    var filters = getFilterRequest();
    var username = $("#usr").val();
    if (username != "") {
        filters += "&username=" + username;
    }
    if (request_admin_filters_booking != null) {
        request_admin_filters_booking.abort();
    }
    loadingIcon("bookingToAppend");
    request_admin_filters_booking = getRequest(
        $("#bookingByComplex_url").val() + "?" + filters, // URL for the PHP file
        parseBooking,  // handle successful request
        reqError,    // handle error
        'bookingToAppend',
        "GET"
    );
}


function updateByFiltersAvailables() {
    var filters = getFilterRequest();
    if (request_admin_filters_available != null) {
        request_admin_filters_available.abort();
    }

    loadingIcon("availableToAppend");
    request_admin_filters_available = getRequest(
        $("#viewAll_url").val() + "?" + filters, // URL for the PHP file
        parseAvailable,  // handle successful request
        reqError,    // handle error
        'availableToAppend',
        "GET"
    );
}


function getRequest(url, success, error, container, method) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText, container) : error(req.status, container);
        }
    }
    req.open(method, url, true);
    req.send(null);
    return req;
}

function drawallbookings(bookings, container) {
    var arrayLength = bookings.length;
    var full = "";

    for (var i = 0; i < arrayLength; i++) {
        full += drawBooking(bookings[i], i);
    }
    if (arrayLength % 2 == 1) {
        full += '<div class="panel panel-default result result-no-border"></div>';
    }

    if (bookings.length == 0) {
        notifyNoResults(1, container);
        return;
    }
    $("." + container).html(full);

    $(".remove").on('click', function (e) {

        $(".selected_button").removeClass("selected_button");

        $(this).addClass("selected_button");

        var body = '¿Estás seguro que querés cancelar la reserva el día <span class="modal-data">' + $(this).attr("day") + '</span> ' +
            'a las <span class="modal-data">' + $(this).attr("begin") + ':00hs</span>?';

        $("#delete_entry").html(body);

        $('#delete').modal('show');

        $("#delete_reserve").click(function () {
            var button = $(".selected_button").addClass("selected_button");

            var complexId = button.attr("complexId");
            var pitchId = button.attr("pitchId");
            var date = button.attr("date");
            var begin = button.attr("begin");
            var params = "";
            params += "complexId=" + complexId;
            params += "&pitchId=" + pitchId;
            params += "&date=" + date;
            params += "&startTime=" + begin;
            getRequest(
                $("#removeBooking_url").val() + "?" + params, // URL for the PHP file
                deleteTab,  // handle successful request
                reqError,    // handle error
                $(button).parent().parent().attr("id"),
                "POST"
            );


        });

    });


}

function drawAllAvailables(availables, container) {
    var arrayLength = availables.length;
    var full = "";

    for (var i = 0; i < arrayLength; i++) {
        full += drawAvailable(availables[i], i);
    }
    if (arrayLength % 2 == 1) {
        full += '<div class="panel panel-default result result-no-border"></div>';
    }

    if (availables.length == 0) {
        notifyNoResults(1, container);
        return;
    }
    $("." + container).html(full);

    $(".booking-btn").on('click', function (e) {
        var complexId = $(this).attr("complexId");
        var pitchId = $(this).attr("pitchId");
        var date = $("#2-date-picker-input").attr("value");
        var begin = $(this).attr("begin");

        var params = "";
        params += "complexId=" + complexId;
        params += "&pitchId=" + pitchId;
        params += "&date=" + date;
        params += "&startTime=" + begin;
        getRequest(
            $("#reserve_url").val() + "?" + params, // URL for the PHP file
            deleteTab,  // handle successful request
            reqError,    // handle error
            $(this).parent().parent().attr("id"),
            "POST"
        );
    });
}


function parseBooking(data, container) {

    sessionStorage.setItem("bookings", data);

    fullComplexList = JSON.parse(data);

    drawallbookings(fullComplexList, container);
}

function parseAvailable(data, container) {

    sessionStorage.setItem("available", data);

    fullComplexList = JSON.parse(data);

    drawAllAvailables(fullComplexList, container);
}

function drawBooking(booking, index) {
    var ans = "";
    ans += '	<div class= "row border-top text-center" id="booking' + index + '">';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + booking.date + '</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + booking.start + ':00</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + booking.pitchId + '</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + booking.paid + '</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + booking.email + '</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-1 col-xs-offset-1 padding-top-btn">';
    ans += '			<button type="button" complexId="' + booking.complexId + '" pitchId="' + booking.pitchId + '" begin="' + booking.start + '" day="' + booking.date + '" date="' + booking.day + '"  class="btn btn-danger btn-block remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>';
    ans += '		</div>';
    ans += '	</div>';

    return ans;
}


function drawAvailable(pitchAvailable, index) {
    var ans = "";
    ans += '	<div class= "row border-top text-center" id="available' + index + '">';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + pitchAvailable.startTime + ':00</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + pitchAvailable.pitchId + '</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + pitchAvailable.size + '</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + pitchAvailable.floor + '</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-2 padding-top">';
    ans += '			<p>' + pitchAvailable.price + '</p>';
    ans += '		</div>';
    ans += '		<div class="col-xs-2 padding-top-btn">';
    ans += '			<button type="button" complexId="' + pitchAvailable.complexId + '" pitchId="' + pitchAvailable.pitchId + '" begin="' + pitchAvailable.startTime + '" class="btn btn-success btn-block booking-btn"><span class="glyphicon glyphicon-ok" aria-hidden="true"> Reservar </span></button>';
    ans += '		</div>';
    ans += '	</div>';

    return ans;
}

function deleteTab(result, id) {
    var json = JSON.parse(result);
    if (json.result == "true") {
        $("#" + id).slideUp();
    }
    $('#delete').modal('hide');

    reload_admin_index_page = true;
}

function reqError(status, container) {

    var msg = '<div id="general-name">';
    msg += 'Error inesperado';
    msg += '</div>';
    msg += '<div class="error-body">';
    msg += '<div id="error-details">';
    msg += '¡Ups! No es posible comunicarse con el servidor.';
    msg += '</div>';
    msg += '</div>';
    $("." + container).html(msg);
}


function notifyNoResults(status, container) {

    var msg = '<br><div id="no-result-msg"><div id="general-name">';
    msg += 'No hay resultados';
    msg += '</div>';
    msg += '<div class="error-body">';
    msg += '<div id="error-details">';
    msg += 'No encontramos canchas con esas características.';
    msg += '</div>';
    msg += '</div></div>';
    $("." + container).html(msg);
}

function datePickerListener() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    var date = yyyy + "-" + mm + "-" + dd;
    if (date == $("#2-date-picker-input").attr("value")) {
        limitBeginHour(today.getHours() + 1);
    } else {
        limitBeginHour(8);
        updateByFilters();
        updateByFiltersAvailables();
    }
}


function datePickerListenerSchedule() {
    var beginDate = $("#4-date-picker-input").attr("value");
    var endDate = $("#5-date-picker-input").attr("value");
    var endDateSplitted = beginDate.split("-");
    $("#5-date-picker-container").remove();
    generateDatePicker(5, true, undefined, parseInt(endDateSplitted[0]), parseInt(endDateSplitted[1]), parseInt(endDateSplitted[2]));
    if (Date.parse(endDate) > Date.parse(beginDate)) {
        updateDate(5, endDate);
    }

}

function limitBeginHour(currentHour) {
    var beginHour = $("#form-control-begin").val();
    var beginContent = "";
    for (var i = currentHour; i < 24; i++) {
        if (i == beginHour) {

            beginContent += "<option value=" + i + " selected>" + i + ":00</option>";
        } else {
            beginContent += "<option value=" + i + ">" + i + ":00</option>";
        }
    }
    $("#form-control-begin").html(beginContent);
    limitEndHour(currentHour > beginHour ? currentHour : beginHour);
}

function loadingIcon(container) {
    var html = '<div class="loading"><img src=' + $("#loadingIcon").attr("value") + ' /> Cargando </div>';
    $("." + container).html(html);
    $("." + container).html(html);
}