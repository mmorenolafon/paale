package ar.edu.itba.paw.webapp.controllers;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RegisterForm {

    @NotBlank
    @Pattern(regexp = "^[A-Z a-záéíóúñ]+$")
    @Size(max = 100)
    private String firstName;

    @NotBlank
    @Pattern(regexp = "^[A-Z a-záéíóúñ]+$")
    @Size(max = 100)
    private String lastName;

    @NotBlank
    @Size(min = 7, max = 100)
    private String password;

    @Email
    @NotBlank
    @Size(max = 100)
    private String email;

    @NotBlank
    @Pattern(regexp = "^[ 0-9]*+$")
    @Size(max = 20)
    private String phone;

    @Pattern(regexp = "^[A-Z_a-z0-9]*+$")
    @Size(min = 0, max = 20)
    private String username;

    public RegisterForm() {
    }

    public RegisterForm(String firstName, String lastName, String password, String email, String phone, String username) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
