package ar.edu.itba.paw.webapp.controllers;

import ar.edu.itba.paw.interfaces.*;
import ar.edu.itba.paw.model.*;
import ar.edu.itba.paw.model.DTOs.AdminDTO;
import ar.edu.itba.paw.model.DTOs.ComplexDTO;
import ar.edu.itba.paw.model.DTOs.ReserveDTO;
import ar.edu.itba.paw.model.DTOs.ScheduleExceptionDTO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.DataFormatException;

@Path("/admin")
public class AdminController {

  @Autowired
  private UserService us;

  @Autowired
  private AdminService as;

  @Autowired
  private ComplexService cs;

  @Autowired
  private PitchService ps;

  @Autowired
  private ScheduleExceptionService ses;

  @Autowired
  private BookingService bs;


  @GET
  @Path("/profile")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response getProfileDetails(@Context HttpHeaders headers, @QueryParam("username") String username) {
    User user = us.getLoggedUser();
    if (user == null) {
      return Response.status(401).build();
    }
    AdminDTO adminDTO = as.getAdmin(user).toDTO();
    return Response.ok(adminDTO).build();
  }

  @GET
  @Path("/complex/pitches")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response getAdminComplexAvailablePitches(@Context HttpHeaders headers,
                                                  @QueryParam("complexId") final int complexId,
                                                  @QueryParam("date") final String date,
                                                  @QueryParam("startTime") final int startTime,
                                                  @QueryParam("endTime") final int endTime,
                                                  @QueryParam("capacity") final int capacity,
                                                  @QueryParam("floor") final int floor) {


    List<AvailablePitch> pitches = null;
    try {
      pitches = ps.getGroupsAvailable(floor, capacity, startTime, endTime, date, complexId);
    } catch (ParseException e) {
      e.printStackTrace();
      return Response.status(400).build(); //Bad Request
    }

    GenericEntity<List<AvailablePitch>> entity = null;
    entity = new GenericEntity<List<AvailablePitch>>(pitches) {
    };
    return Response.ok(entity).build();
  }

  @GET
  @Path("/booking")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response getBookingsByComplex(@Context HttpHeaders headers,
                                       @QueryParam("username") String username,
                                       @QueryParam("complexId") final int complexId,
                                       @QueryParam("date") final String date,
                                       @QueryParam("startTime") final int startTime,
                                       @QueryParam("endTime") final int endTime) {

    Admin admin = as.getAdmin(us.getLoggedUser());
    Iterator<Complex> adminComplexesIt = admin.getComplexes().iterator();
    Complex current = null;
    while (adminComplexesIt.hasNext() && (current = adminComplexesIt.next()).getId() != complexId) {
    }
    if (current.getId() != complexId) {
      return Response.status(400).build();
    }

    List<Reserve> result = null;
    try {
      if ("".equals(username)) {
        username = admin.getUser().getUsername();
      }
      result = bs.getBookingsByComplex(complexId, username, date, startTime, endTime);
    } catch (DataFormatException e) {
      e.printStackTrace();
      return Response.status(400).build(); //Bad Request
    }


    if (headers.getRequestHeader("If-Modified-Since") != null) {


      DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
        "EEE MMM dd yyyy HH:mm:ss zZ (zz)");

      LocalDateTime lastModifDate;
      lastModifDate = LocalDateTime.parse(
        headers.getRequestHeader("If-Modified-Since").get(0), formatter);

      Complex complex = cs.getComplexById(complexId);
      if (complex == null) {
        return Response.status(400).build(); //Bad Request
      }

      if (complex.getLastModified() != null && complex.getLastModified().isBefore(lastModifDate)) {
        return Response.ok().build();
      }
    }


    if (result == null) {
      return Response.ok().build();
    }

    List<ReserveDTO> resultDTO = result.stream().map(Reserve::toDTO).collect(Collectors.toList());
    GenericEntity<List<ReserveDTO>> entity = new GenericEntity<List<ReserveDTO>>(resultDTO) {
    };
    return Response.ok(entity).build();
  }

  @POST
  @Path("/booking")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response addBooking(@Context HttpHeaders headers,
                             @QueryParam("bookerUsername") final String bookerUsername,
                             @QueryParam("complexId") final int complexId,
                             @QueryParam("pitchId") final int pitchId,
                             @QueryParam("date") final String date,
                             @QueryParam("startTime") final int startTime) {

    Admin admin = as.getAdmin(us.getLoggedUser());
    boolean result;

    try {

      result = bs.book(admin, bookerUsername, complexId, pitchId, date, startTime);

    } catch (DataFormatException e) {
      e.printStackTrace();
      return Response.status(400).build(); //Bad Request
    }
    if (result) {
      return Response.ok().build();
    }
    return Response.status(400).build();
  }


  @DELETE
  @Path("/booking")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response deleteBooking(@Context HttpHeaders headers,
                                @QueryParam("complexId") final int complexId,
                                @QueryParam("pitchId") final int pitchId,
                                @QueryParam("date") final String date,
                                @QueryParam("startTime") final int startTime) {

    boolean result;
    try {
      result = bs.removeBooking(complexId, pitchId, date, startTime);
    } catch (DataFormatException e) {
      e.printStackTrace();
      return Response.status(400).build();
    }
    if (result) {
      return Response.ok().build();
    }
    return Response.status(400).build();
  }


  @GET
  @Path("/complex")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response getComplexDetails(@Context HttpHeaders headers,
                                    @QueryParam("complexId") final int complexId) {

    ComplexDTO ans = cs.getComplexById(complexId).toDTO();
    return Response.ok(ans).build();
  }

  @POST
  @Path("/exception")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response addScheduleException(@Context HttpHeaders headers,
                                       @QueryParam("complexId") final int complexId,
                                       @QueryParam("beginDate") final String beginDate,
                                       @QueryParam("endDate") final String endDate) {

    if (!ses.addScheduleException(complexId, beginDate, endDate)) {
      return Response.status(400).build();
    }
    return Response.ok().build();
  }


  @GET
  @Path("/exception")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response getAllScheduleExceptions(@Context HttpHeaders headers,
                                           @QueryParam("complexId") final int complexId) {

    List<ScheduleException> result = ses.getAllScheduleExceptionsByComplex(complexId);
    List<ScheduleExceptionDTO> resultDTO = result.stream().map(ScheduleException::toDTO).collect(Collectors.toList());
    GenericEntity<List<ScheduleExceptionDTO>> entity = new GenericEntity<List<ScheduleExceptionDTO>>(resultDTO) {
    };

    return Response.ok(entity).build();
  }

  @DELETE
  @Path("/exception")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response deleteScheduleException(@Context HttpHeaders headers,
                                          @QueryParam("complexId") final int complexId,
                                          @QueryParam("beginDate") final String beginDate,
                                          @QueryParam("endDate") final String endDate) {

    if (!(complexId == -1 || beginDate.equals("") || endDate.equals(""))) {
      ses.deleteScheduleException(complexId, beginDate, endDate);
    } else {
      Response.status(400).build();
    }
    return Response.ok().build();
  }


  @POST
  @Path("/complex")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response modifyComplexDetails(@Context HttpHeaders headers,
                                       @QueryParam("complexId") final int complexId,
                                       @QueryParam("phone") final String phone,
                                       @QueryParam("email") final String email) {

    boolean result = cs.modifyComplexFields(complexId, phone, email);
    if (!result) {
      return Response.status(400).build();
    }
    return Response.ok().build();
  }

}
