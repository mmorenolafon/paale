package ar.edu.itba.paw.webapp.config;

import ar.edu.itba.paw.interfaces.AdminService;
import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.webapp.auth.SimpleAuthenticationSuccessHandlerRoled;
import ar.edu.itba.paw.webapp.auth.StatelessAuthenticationFilter;
import ar.edu.itba.paw.webapp.auth.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity

@ComponentScan("ar.edu.itba.paw.webapp.auth,  ar.edu.itba.paw.service")
public class WebAuthConfig extends WebSecurityConfigurerAdapter {

  public static final String SECRET = "thisIsASecret";


  @Autowired
  private AuthenticationProvider authenticationProvider;

  @Autowired
  private UserService us;

  @Autowired
  private AdminService as;


  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http.authenticationProvider(authenticationProvider)
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    //  .invalidSessionUrl("/403")

      .and().authorizeRequests()
      .antMatchers("/login").permitAll()
      .antMatchers("/login/facebook").permitAll()
      .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
      .antMatchers(HttpMethod.POST, "/user").permitAll()

      .antMatchers("/user/**").hasRole("USER")
      .antMatchers("/admin/**").hasRole("ADMIN")
      .antMatchers(HttpMethod.GET,"/complex/schedules").hasRole("ADMIN")
      .antMatchers(HttpMethod.POST,"/complex/schedules").hasRole("ADMIN")
      .anyRequest().permitAll()
      .and().addFilterBefore(new StatelessAuthenticationFilter(new TokenAuthenticationService(us, as)),
      UsernamePasswordAuthenticationFilter.class)

      //  .authorizeRequests()

      .logout()
      .logoutUrl("/logout")
      .logoutSuccessUrl("/")
      .and().exceptionHandling()
      .accessDeniedPage("/403")
      .and().csrf().disable();
  }


  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/resources/**", "/403");
  }

  @Bean
  public AuthenticationSuccessHandler successHandler() {

    SimpleUrlAuthenticationSuccessHandler handler = new SimpleAuthenticationSuccessHandlerRoled();
    return handler;
  }


}
