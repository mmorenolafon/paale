package ar.edu.itba.paw.webapp.controllers;

import ar.edu.itba.paw.interfaces.PitchService;
import ar.edu.itba.paw.model.AvailablePitch;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;

@Path("/pitch")
public class PitchController {

    @Autowired
    private PitchService serv;


    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response searchComplex(@QueryParam("complexId") final Integer complexId,
                                  @QueryParam("floor") final Integer floor,
                                  @QueryParam("capacity") final Integer capacity,
                                  @QueryParam("beginHour") final Integer beginHour,
                                  @QueryParam("endHour") final Integer endHour,
                                  @QueryParam("day") final String date) {


        //valid query:  http://localhost:8080/complex?stateId=2&name=GROSO&floor=1&capacity=5&beginHour=8&day=2016-10-14&endHour=10
        if (complexId == null || floor == null || capacity == null || beginHour == null || endHour == null || date == null) {
            return Response.status(400).build(); //bad request
        }


        List<AvailablePitch> pitches = null;



        try {
            pitches = serv.getGroupsAvailable(floor, capacity, beginHour, endHour, date, complexId);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (pitches == null) {
            return Response.status(400).build();
        }

        Collection<AvailablePitch> genPitches= pitches;
        GenericEntity<Collection<AvailablePitch>> entity  = new GenericEntity< Collection<AvailablePitch> >(genPitches) { };
        return Response.ok(entity).build();
    }

}
