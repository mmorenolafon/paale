package ar.edu.itba.paw.webapp.auth;

import ar.edu.itba.paw.interfaces.AdminService;
import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;


@Component
@EnableWebSecurity
@ComponentScan("ar.edu.itba.paw.webapp.auth")
public class PawAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(PawAuthenticationProvider.class);

    @Autowired
    private UserService us;

    @Autowired
    private AdminService ad;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        User user = us.getByUsername(username);
        if (user == null) {
            user = us.getByEmail(username);
        }

        password = password.replace("\n", "");
        System.out.println("(" + user.getPassword() + ") , (" + password + "), true:" + user.getPassword().equals(password));

        if (user.getPassword().equals(password)) {
            final Collection<SimpleGrantedAuthority> authorities = new HashSet<>();

            if (ad.getAdmin(user) != null) {
                authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                LOGGER.info("Admin authenticated");
            } else {
                authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
                LOGGER.info("User authenticated");
            }

            return new UsernamePasswordAuthenticationToken(username, password, authorities);
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
