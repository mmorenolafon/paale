package ar.edu.itba.paw.webapp.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SimpleAuthenticationSuccessHandlerRoled extends SimpleUrlAuthenticationSuccessHandler {


    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String role = auth.getAuthorities().toString();

        String targetUrl = "";
        if (role.contains("ADMIN")) {
            targetUrl = "/admin/index.html";
        } else if (role.contains("USER")) {
            if (request.getHeader("Referer") != null) {
                targetUrl = request.getHeader("Referer");
            } else {
                targetUrl = "/index.html";
            }
            setUseReferer(true);
        }
        return targetUrl;
    }

}
