package ar.edu.itba.paw.webapp.controllers;

import ar.edu.itba.paw.interfaces.StatesService;
import ar.edu.itba.paw.model.States;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/states")
public class StatesController {

    @Autowired
    private StatesService st;

    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response searchStates() {
        GenericEntity<List<States>> entity = new GenericEntity< List<States> >(st.getAllStates()) { };
        return Response.ok(entity).build();
    }
}
