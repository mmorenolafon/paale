package ar.edu.itba.paw.webapp.controllers;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Constraint(validatedBy = PhoneConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Phone {


    String message() default "El formato de teléfono es incorrecto";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
