package ar.edu.itba.paw.webapp.controllers;


import ar.edu.itba.paw.interfaces.ScheduleExceptionService;
import ar.edu.itba.paw.model.ScheduleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class ScheduleExceptionController {

    @Autowired
    private ScheduleExceptionService ses;

    private static final Logger LOGGER = LoggerFactory.getLogger(ComplexController.class);


    @POST
    @Path("/admin/exception")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public ModelAndView addScheduleException(@RequestParam(defaultValue = "-1") final int complexId,
                                      @RequestParam(defaultValue = "") final String beginDate,
                                      @RequestParam(defaultValue = "") final String endDate) {

        ModelAndView mav = new ModelAndView("jsonWrapper");

        if (!ses.addScheduleException(complexId, beginDate, endDate)) {
            LOGGER.debug("SCHEDULE EXCEPTION CONTROLLER: Could not insert new exception schedule");
            mav.addObject("json", "{}");
        } else {
            LOGGER.debug("SCHEDULE EXCEPTION CONTROLLER: Exception schedule inserted succesfully");
            StringBuilder beginDateFormatted = new StringBuilder();
            String[] bd = beginDate.split("-");
            beginDateFormatted.append(bd[2]).append("/").append(bd[1]).append("/").append(bd[0]);

            StringBuilder endDateFormatted = new StringBuilder();
            String[] ed = endDate.split("-");
            endDateFormatted.append(ed[2]).append("/").append(ed[1]).append("/").append(ed[0]);

            StringBuilder json = new StringBuilder();
            json.append("{" + toJsonField("complexId", String.valueOf(complexId)) + ",");
            json.append(toJsonField("beginDate", beginDateFormatted.toString()) + ",");
            json.append(toJsonField("endDate", endDateFormatted.toString()));
            json.append("}");
            mav.addObject("json", json);
        }

        return mav;
    }

    @DELETE
    @Path("/admin/exception")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public ModelAndView deleteScheduleException(@RequestParam(defaultValue = "-1") final int complexId,
                                                @RequestParam(defaultValue = "") final String beginDate,
                                                @RequestParam(defaultValue = "") final String endDate) {
        ModelAndView mav = new ModelAndView("jsonWrapper");
        if (!(complexId == -1 || beginDate.equals("") || endDate.equals(""))) {
            ses.deleteScheduleException(complexId, beginDate, endDate);
        } else {
            return new ModelAndView("error");
        }
        return mav;
    }

    private String toJsonField(String label, String value) {
        return "\"" + label + "\":\"" + value + "\"";
    }


    @GET
    @Path("/admin/exception")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public ModelAndView getAllScheduleExceptions(@RequestParam(defaultValue = "-1") final int complexId) {
        ModelAndView mav = new ModelAndView("jsonWrapper");
        StringBuilder json = new StringBuilder();
        if (complexId != -1) {
            List<ScheduleException> result = ses.getAllScheduleExceptionsByComplex(complexId);
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            json.append("{\"data\":[");
            for (int i = 0; i < result.size(); i++) {
                ScheduleException se = result.get(i);
                json.append("{\"beginDate\":\"" + se.getBeginDate().format(dtf) + "\",");
                json.append(" \"endDate\":\"" + se.getEndDate().format(dtf) + "\"}");
                if (i != result.size() - 1) { //if it is not the last element
                    json.append(",");
                }
            }
            json.append("]}");
            mav.addObject("json", json.toString());
        } else {
            return new ModelAndView("error");
        }
        return mav;
    }

}
