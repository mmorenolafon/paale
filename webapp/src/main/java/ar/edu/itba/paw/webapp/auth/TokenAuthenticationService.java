package ar.edu.itba.paw.webapp.auth;

import ar.edu.itba.paw.interfaces.AdminService;
import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.model.User;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by tritoon on 25/11/16.
 */
public class TokenAuthenticationService {

  protected static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

  private final TokenHandler tokenHandler;
  private final AdminService as;

  public TokenAuthenticationService(UserService userService, AdminService as) {
    tokenHandler = new TokenHandler(userService);
    this.as = as;
  }

  public void addAuthentication(HttpServletResponse response, User user) {
    response.addHeader(AUTH_HEADER_NAME, tokenHandler.createTokenForUser(user.getEmail()));
  }

  public Authentication getAuthentication(HttpServletRequest request) {
    final String token = request.getHeader(AUTH_HEADER_NAME);
    if (token != null) {
      final User user = tokenHandler.parseUserFromToken(token);
      if (user != null) {
        return new UserAuthentication(user, as);
      }
    }
    return null;
  }
}
