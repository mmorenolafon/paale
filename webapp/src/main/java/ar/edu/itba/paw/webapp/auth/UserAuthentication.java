package ar.edu.itba.paw.webapp.auth;

import ar.edu.itba.paw.interfaces.AdminService;
import ar.edu.itba.paw.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by tritoon on 25/11/16.
 */

public class UserAuthentication implements Authentication {
  private static final Logger LOGGER = LoggerFactory.getLogger(UserAuthentication.class);

  private User user;
  private boolean authenticated = true;
  private Set<SimpleGrantedAuthority> authorities;


  public UserAuthentication(User user, AdminService as) {
    this.user = user;

    authorities = new HashSet<>();

    if(as.getAdmin(user)!=null){
      LOGGER.info("Admin Authenticated");
      authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }else{
      LOGGER.info("User Authenticated");
      authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
    }
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return authorities;
  }

  @Override
  public Object getCredentials() {
    return user.getEmail();
  }

  @Override
  public Object getDetails() {
    return user.getUsername();
  }

  @Override
  public Object getPrincipal() {
    return user.getEmail();
  }

  @Override
  public boolean isAuthenticated() {
    return authenticated;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    this.authenticated = isAuthenticated;
  }

  @Override
  public String getName() {
    return user.getFirstName();
  }
}
