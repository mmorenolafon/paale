package ar.edu.itba.paw.webapp.controllers;

import ar.edu.itba.paw.interfaces.*;
import ar.edu.itba.paw.model.User;
import ar.edu.itba.paw.webapp.auth.TokenHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/")
@Component
public class MainController {

  private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
  private static final String AUTHORIZATION_BASIC = "BASIC";

  @Autowired
  UserService us;

  @Autowired
  AdminService as;

  @Autowired
  ValorationService vs;

  @Autowired
  FacebookService fbs;

  @Autowired
  Populator populator;

  @Context
  private UriInfo uriInfo;


  @GET
  @Path("/login")
  public Response getCredentials(@HeaderParam("Authorization") String authorization) {

    if (authorization != null &&
      authorization.split(" ")[0].toUpperCase().equals(AUTHORIZATION_BASIC)) {

      String base64encoded = authorization.split(" ")[1];
      String decoded = new String(Base64.decode(base64encoded.getBytes()));

      String[] credentials = decoded.split(":");

      User user = us.getByEmail(credentials[0]);
      if (user == null) {
        user = us.getByUsername(credentials[0]);
      }

      if (user == null || credentials.length < 2 || user.getPassword() == null || !user.getPassword().equals(credentials[1].trim())) {
        return Response.status(403).build();
      }

      TokenHandler th = new TokenHandler(us);
      String newToken = th.createTokenForUser(user.getEmail());

      String role = "{\"role\":\"user\"}";
      if (as.getAdmin(user) != null) {
        role = "{\"role\":\"admin\"}";
      }

      return Response.ok(role).header("X-Token", newToken).build();
    }
    return Response.status(401).build();
  }

  @GET
  @Path("/login/facebook")
  public Response getFacebookCredentials(@HeaderParam("X-Facebook-Token") String fToken) {

    if (fToken != null) {
      User user = fbs.loginUser(fToken);
      if (user != null) {
        TokenHandler th = new TokenHandler(us);
        String newToken = th.createTokenForUser(user.getEmail());

        String role = "{\"role\":\"user\"}";
        if (as.getAdmin(user) != null) {
          role = "{\"role\":\"admin\"}";
        }

        return Response.ok(role).header("X-Token", newToken).build();
      }
    }
    return Response.status(401).build();
  }

  @GET
  @Path("/403")
  public Response temp1(@HeaderParam("Authorization") String authorization) {


    return Response.ok(403).build();
  }

  @GET
  @Path("/404")
  public Response temp2(@HeaderParam("Authorization") String authorization) {

    return Response.ok(404).build();
  }

  @GET
  @Path("/401")
  public Response temp3(@HeaderParam("Authorization") String authorization) {

    return Response.ok(401).build();
  }

   /* @GET
    public Response defaultMapping() {
        URI targetURIForRedirection = URI.create("/index.html");
        return Response.seeOther(targetURIForRedirection).build();
    }
    */

}
