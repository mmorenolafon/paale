package ar.edu.itba.paw.webapp.controllers;

import org.springframework.stereotype.Component;

@Component
public class BookingConfirmationForm {
    private int complexId;
    private String complexName;
    private String complexAddress;
    private String complexPhone;
    private String complexEmail;
    private String date;
    private int startTime;
    private int pitchId;
    private int pitchSize;
    private String pitchFloor;
    private double price;


    public BookingConfirmationForm() {
    }

    public BookingConfirmationForm(int complexId, String complexName, String complexAddress, String complexPhone,
                                   String complexEmail, String date, int startTime, int pitchId, int pitchSize,
                                   String pitchFloor, double price) {
        this.complexId = complexId;
        this.complexName = complexName;
        this.complexAddress = complexAddress;
        this.complexPhone = complexPhone;
        this.complexEmail = complexEmail;
        this.date = date;
        this.startTime = startTime;
        this.pitchId = pitchId;
        this.pitchSize = pitchSize;
        this.pitchFloor = pitchFloor;
        this.price = price;
    }

    public int getComplexId() {
        return complexId;
    }

    public void setComplexId(int complexId) {
        this.complexId = complexId;
    }

    public String getComplexName() {
        return complexName;
    }

    public void setComplexName(String complexName) {
        this.complexName = complexName;
    }

    public String getComplexAddress() {
        return complexAddress;
    }

    public void setComplexAddress(String complexAddress) {
        this.complexAddress = complexAddress;
    }

    public String getComplexPhone() {
        return complexPhone;
    }

    public void setComplexPhone(String complexPhone) {
        this.complexPhone = complexPhone;
    }

    public String getComplexEmail() {
        return complexEmail;
    }

    public void setComplexEmail(String complexEmail) {
        this.complexEmail = complexEmail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getPitchId() {
        return pitchId;
    }

    public void setPitchId(int pitchId) {
        this.pitchId = pitchId;
    }

    public int getPitchSize() {
        return pitchSize;
    }

    public void setPitchSize(int pitchSize) {
        this.pitchSize = pitchSize;
    }

    public String getPitchFloor() {
        return pitchFloor;
    }

    public void setPitchFloor(String pitchFloor) {
        this.pitchFloor = pitchFloor;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
