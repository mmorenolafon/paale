package ar.edu.itba.paw.webapp.controllers;

import ar.edu.itba.paw.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ErrorController {

    @Autowired
    UserService us;

    @RequestMapping(value = {"/error.html"})
    public ModelAndView errorDetails() {
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("loggedUser", us.getLoggedUserName());
        return mav;
    }
}
