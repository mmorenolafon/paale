package ar.edu.itba.paw.webapp.controllers;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Repository
public class ComplexDetailsForm {

    @NotNull
    private Integer complexId;

    @NotBlank
    private String address;

    @Email
    @NotBlank
    @Size(max = 100)
    private String email;

    @NotBlank
    @Pattern(regexp = "^[ 0-9]*+$")
    @Size(max = 20)
    private String phone;


    public ComplexDetailsForm() {

    }

    public ComplexDetailsForm(int complexId, String address, String email, String phone) {
        this.complexId = complexId;
        this.address = address;
        this.email = email;
        this.phone = phone;
    }

    public int getComplexId() {
        return complexId;
    }

    public void setComplexId(int complexId) {
        this.complexId = complexId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
