package ar.edu.itba.paw.webapp.controllers;

import ar.edu.itba.paw.model.DTOs.FloorDTO;
import ar.edu.itba.paw.model.Floor;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("/floor")
@Controller
public class FloorsController {

    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response searchFloor() {
        Collection<FloorDTO> floors= Floor.getFloorsDTO();
        GenericEntity<Collection<FloorDTO>> entity  = new GenericEntity< Collection<FloorDTO> >(floors) { };
        return Response.ok(entity).build();
    }
}

