package ar.edu.itba.paw.webapp.auth;

import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.model.User;
import ar.edu.itba.paw.webapp.config.WebAuthConfig;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by tritoon on 25/11/16.
 */
public final class TokenHandler {

  @Autowired
  private UserService userService;

  public TokenHandler( UserService userService) {
    this.userService = userService;
  }

  public User parseUserFromToken(String token) {
    String username = null;
    try {
      username = Jwts.parser()
        .setSigningKey(WebAuthConfig.SECRET)
        .parseClaimsJws(token)
        .getBody()
        .getSubject();
    }catch(Exception e){
      // JJWT framework does not handle this exceptions
      return null;
    }
    return userService.getByUsername(username);
  }

  public String createTokenForUser(String userEmail) {
    return Jwts.builder()
      .setSubject(userEmail)
      .signWith(SignatureAlgorithm.HS512, WebAuthConfig.SECRET)
      .compact();
  }
}
