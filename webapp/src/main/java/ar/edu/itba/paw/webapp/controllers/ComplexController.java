package ar.edu.itba.paw.webapp.controllers;

import ar.edu.itba.paw.interfaces.AdminService;
import ar.edu.itba.paw.interfaces.ComplexService;
import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.model.CardInformation;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.DTOs.ComplexDTO;
import ar.edu.itba.paw.model.DTOs.PitchSetTimeTableDTO;
import ar.edu.itba.paw.model.DTOs.PitchTimeTableDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Path("/complex")
public class ComplexController {

  @Autowired
  private ComplexService comp;

  @Autowired
  UserService us;

  @Autowired
  AdminService as;

  private static final Logger LOGGER = LoggerFactory.getLogger(ComplexController.class);


  @GET
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response searchComplex(@QueryParam("name") final String name,
                                @QueryParam("stateId") final Integer stateId,
                                @QueryParam("floor") final Integer floor,
                                @QueryParam("capacity") final Integer capacity,
                                @QueryParam("beginHour") final Integer beginHour,
                                @QueryParam("endHour") final Integer endHour,
                                @QueryParam("day") final String day) {

    //valid query:  http://localhost:8080/complex?stateId=2&name=GROSO&floor=1&capacity=5&beginHour=8&day=2016-10-14&endHour=10
    if (floor == null || capacity == null || beginHour == null || endHour == null || day == null) {
      return Response.status(400).build(); //bad request
    }

    GenericEntity<List<CardInformation>> entity;
    List<CardInformation> cards = comp.getComplex(stateId, name, floor, capacity, beginHour, endHour, day);
    entity = new GenericEntity<List<CardInformation>>(cards) {
    };
    return Response.ok(entity).build();
  }

  @GET
  @Produces(value = {MediaType.APPLICATION_JSON})
  @Path("{id}")
  public Response searchComplexById(@PathParam("id") int id) {

    Complex complex = comp.getComplexById(id);

    ComplexDTO complexDTO = null;
    if (complex != null) {
      complexDTO = complex.toDTO();
    }

    return Response.ok(complexDTO).build();
  }

  @GET
  @Produces(value = {MediaType.APPLICATION_JSON})
  @Path("/schedules")
  public Response schedules(@QueryParam("complexId") int complexId) {
    List<PitchTimeTableDTO> pitches = comp.getSchedule(complexId);
    GenericEntity<List<PitchTimeTableDTO>> entity = new GenericEntity<List<PitchTimeTableDTO>>(pitches) {
    };
    return Response.ok(entity).build();
  }



  @POST
  @Produces(value = {MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_JSON})
  @Path("/schedules")
  public Response setSchedules(@QueryParam("ttablesJson") String timeTablesJson) {

    PitchSetTimeTableDTO timeTables = null;
    try {
      timeTables = new ObjectMapper().readValue(timeTablesJson, PitchSetTimeTableDTO.class);
    } catch (IOException e) {
      LOGGER.error("Unable un unparse PitchSetTimeTableDTO");
    }

    comp.setNewPrices(timeTables.getComplexId(), timeTables);
    return Response.ok().build();

  }

}
