package ar.edu.itba.paw.webapp.controllers;

import ar.edu.itba.paw.interfaces.BookingService;
import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.interfaces.ValorationService;
import ar.edu.itba.paw.model.DTOs.ErrorDTO;
import ar.edu.itba.paw.model.DTOs.ReserveDTO;
import ar.edu.itba.paw.model.DTOs.ReservesDTO;
import ar.edu.itba.paw.model.DTOs.UserDTO;
import ar.edu.itba.paw.model.PendingValoration;
import ar.edu.itba.paw.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.DataFormatException;


@Path("/")
@Component
public class UserController {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
  private static final int CANCEL_LIMIT = 1;

  @Autowired
  private UserService us;

  @Autowired
  private BookingService bs;

  @Autowired
  private ValorationService vs;


  @GET
  @Path("/user/profile")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response getProfileDetails(@Context HttpHeaders headers) {
    User user = us.getLoggedUser();
    if (user == null) {
      return Response.status(401).build();
    }
    UserDTO userDTO = user.toDTO();
    return Response.ok(userDTO).build();
  }

  @GET
  @Path("/user/booking")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response getUserBooking(@Context HttpHeaders headers) {

    User user = us.getLoggedUser();
    if (user == null) {
      return Response.status(401).build();
    }

    List<ReserveDTO> reserves = us.getUserReserves(user);
    List<ReserveDTO> reserveHistory = us.getUserReserveHistory(user);

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    dtf.withResolverStyle(ResolverStyle.STRICT);
    LocalDate cancelLimit = LocalDate.now().plus(CANCEL_LIMIT, ChronoUnit.DAYS);

    String limitString = cancelLimit.format(dtf);

    return Response.ok(new ReservesDTO(reserves, reserveHistory, limitString)).build();
  }


  @DELETE
  @Path("/user/booking")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response deleteBooking(@Context HttpHeaders headers,
                                @QueryParam("complexId") final int complexId,
                                @QueryParam("pitchId") final int pitchId,
                                @QueryParam("date") final String date,
                                @QueryParam("startTime") final int startTime) {

    User user = us.getLoggedUser();
    if (user == null) {
      return Response.status(401).build();
    }

    boolean result;
    try {
      result = bs.removeBooking(complexId, pitchId, date, startTime);
    } catch (DataFormatException e) {
      e.printStackTrace();
      return Response.status(400).build();
    }
    if (result) {
      return Response.ok().build();
    }
    return Response.status(400).build();
  }

  @POST
  @Path("/user")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response newUser(@FormParam("firstName") String firstName,
                          @FormParam("lastName") String lastName,
                          @FormParam("password") String password,
                          @FormParam("email") String email,
                          @FormParam("phone") String phone,
                          @FormParam("username") String username) {

    RegisterForm registerForm = new RegisterForm(firstName, lastName, password, email, phone, username);

    List<ErrorDTO> errors = registerValidation(registerForm);
    if (errors != null && errors.size() > 0) {
      GenericEntity<List<ErrorDTO>> genericList = new GenericEntity<List<ErrorDTO>>(errors) {
      };
      return Response.status(400).entity(genericList).build();
    }

    User registered = us.register(registerForm.getFirstName(), registerForm.getLastName(),
      registerForm.getPassword(), registerForm.getEmail(), registerForm.getPhone(),
      registerForm.getUsername());
    if (registered == null) {
      return Response.status(400).entity(new ErrorDTO("General", "Unable to create user")).build();
    }

    return Response.ok("").build();
  }


  private List<ErrorDTO> registerValidation(RegisterForm registerForm) {
    List<ErrorDTO> errors = new LinkedList<>();

    if(registerForm == null){
      return  errors;
    }


    if (!registerForm.getUsername().equals("")) {
      if (registerForm.getUsername().length() < 6) {
        errors.add(new ErrorDTO("username", "La longitud mínima del nombre de usuario es de 6 caracteres"));
      } else if (registerForm.getUsername().length() > 50) {
        errors.add(new ErrorDTO("username", "La longitud del nombre de usuario máxima es de 50 caracteres"));
      }

      if (us.getByUsername(registerForm.getUsername()) != null) {
        errors.add(new ErrorDTO("username", "El nombre de usuario ya existe"));
      }
    }

    if (us.getByEmail(registerForm.getEmail()) != null) {
      errors.add(new ErrorDTO("email", "El correo electrónico ya está registrado"));
    }

    return errors;
  }


  private boolean isAValidDate(String date) {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    df.setLenient(false);
    try {
      df.parse(date);
    } catch (ParseException e) {
      LOGGER.info("Invalid date format");
      return false;
    }
    return true;
  }



  @POST
  @Path("/user/booking")
  @Produces(value = {MediaType.APPLICATION_JSON})
  public Response addBooking(@Context HttpHeaders headers,
                             @QueryParam("complexId") final int complexId,
                             @QueryParam("pitchId") final int pitchId,
                             @QueryParam("date") final String date,
                             @QueryParam("startTime") final int startTime) {

    User user = us.getLoggedUser();
    if (user == null) {
      return Response.status(401).build();
    }

    boolean result = false;
    try {
      result = bs.book(complexId, pitchId, date, startTime);
    } catch (DataFormatException e) {
      e.printStackTrace();
      return Response.status(400).build(); //Bad Request
    }
    if (result) {
      return Response.ok().build();
    }
    return Response.status(400).build();
  }


  @GET
  @Path("/user/valoration")
  public Response getValoration() {
    PendingValoration val = vs.getPendingValoration();
    if(val != null){
      return Response.ok(val.toDTO()).build();
    }else{
      return Response.ok().build();
    }
  }

  @POST
  @Path("/user/valoration")
  public Response addValoration(
    @QueryParam("valoration") final int valoration,
    @QueryParam("complexId") final int complexId,
    @QueryParam("date") final String date) {

    if (complexId == -1 || valoration == -1 || date.length() == 0)
      return null;

    vs.addValoration(complexId, valoration, date);

    return Response.ok().build();
  }

  @POST
  @Path("/user/invitation")
  public Response invitePlayers(
    @QueryParam("complexId") final int complexId,
    @QueryParam("pitchId") final int pitchId,
    @QueryParam("date") final String date,
    @QueryParam("startTime") final int startTime,
    @QueryParam("emails") final String emails,
    @QueryParam("message") final String message) {


    String[] emailList = emails.split(";");

    bs.invitePlayers(complexId,pitchId,date,startTime,emailList,message);
    return Response.ok().build();
  }

}

