package ar.edu.itba.paw.webapp.controllers;

import ar.edu.itba.paw.interfaces.BookingService;
import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.interfaces.ValorationService;
import ar.edu.itba.paw.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Controller
public class BookingController {


    @Autowired
    private UserService us;

    @Autowired
    private ValorationService vs;

    @Autowired
    private BookingService bs;

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingController.class);

    @ModelAttribute
    public User loggedUser() {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof AnonymousAuthenticationToken) {
            return null;
        }
        return us.getByUsername((String) auth.getPrincipal());
    }


    @POST
    @Path("/valoration")
    public Response addValoration(
            @HeaderParam("valoration") final int valoration,
            @HeaderParam("complexId") final int complexId,
            @HeaderParam("date") final String date) {

        if (complexId == -1 || valoration == -1 || date.length() == 0)
            return null;

        vs.addValoration(complexId, valoration, date);

        return Response.ok().build();
    }


    private boolean isAValidDate(String date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setLenient(false);
        try {
            df.parse(date);
        } catch (ParseException e) {
            LOGGER.info("Invalid date format");
            return false;
        }
        return true;
    }

}
