'use strict';
define(['hayequipo'], function (hayequipo) {

  hayequipo.controller('UserCtrl', ['$scope', '$http', '$window', function ($scope, $http, $window) {
    var token = sessionStorage.token;


    $scope.updateprofile = function () {
      $http.get('http://' + $scope.ipconnection + '/user/profile', {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $scope.firstname = data.firstName;
        $scope.lastname = data.lastName;
        $scope.email = data.email;
        $scope.phone = data.phone;
        $scope.username = data.username;
      }).error(function (data, status, headers, config) {
      });
    };


    $scope.updatebookins = function () {
      $http.get('http://' + $scope.ipconnection + '/user/booking', {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $scope.bookings = data.reserves;
        $scope.history = data.reserveHistory;
        $scope.limitCancelDate = new Date(data.reserveLimit);
      }).error(function (data, status, headers, config) {
      });
    };

    $scope.cancel = function (currentDate) {
      var current = new Date(currentDate);
      if (current > $scope.limitCancelDate) {
        return true;
      } else {
        return false;
      }
    };

    $scope.deleteuserbooking = function (date, begin, complexId, pitchId) {
      $scope.dateremove = date;
      $scope.beginremove = begin;
      $scope.complexremove = complexId;
      $scope.pitchremove = pitchId;
      $('#delete').modal('toggle');
    };

    $scope.confirmdeleteuserbooking = function () {
      var date = $scope.dateremove;
      var begin = $scope.beginremove;
      var complexId = $scope.complexremove;
      var pitchId = $scope.pitchremove;
      var params =
        '?complexId=' + complexId +
        '&pitchId=' + pitchId +
        '&date=' + date +
        '&startTime=' + begin;
      $http.delete('http://' + $scope.ipconnection + '/user/booking' + params, {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $('#delete').modal('hide');
        $scope.updatebookins();
      }).error(function (data, status, headers, config) {
      });
    };

    $scope.updatebookins();


  }]);

});
