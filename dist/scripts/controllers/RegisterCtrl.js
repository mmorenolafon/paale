'use strict';
define(['hayequipo', 'controllers/IndexCtrl'], function (hayequipo) {

  hayequipo.controller('RegisterCtrl', ['$scope', '$http', '$window', function ($scope, $http, $window) {


    $scope.register = function () {

      $http({
        url: 'http://' + $scope.ipconnection + '/user',
        method: 'POST',
        data: $.param({
          firstName: $scope.firstName, lastName: $scope.lastName, password: $scope.password, email: $scope.email,
          phone: $scope.phone, username: $scope.username
        }),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {
        var username = $scope.username;
        var password = $scope.password;
        var token = btoa(username + ':' + password);
        $http.get('http://' + $scope.ipconnection + '/login', {
          headers: {'Authorization': 'Basic ' + token}
        }).success(function (data, status, headers, config) {
          var usertype = data;
          sessionStorage.token = headers('X-Token');
          sessionStorage.username = username;
          $scope.loggeduser = username;
          if (usertype.role === 'admin') {
            $window.location.href = '/#/admin/';
          } else {
            $window.location.href = '/#/';
          }
        }).error(function (data, status, headers, config) {
          $window.location.href = '/#/error';
        });
      }).error(function (data, status, headers, config) {
        $window.location.href = '/#/error';
      });
    };
  }]);
});
