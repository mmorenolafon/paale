'use strict';

define(['hayequipo'], function (hayequipo) {

  var IP = 'pawserver.it.itba.edu.ar/grupo3/api';

  hayequipo.controller('IndexCtrl', ['$scope', '$http', '$window', '$location', function ($scope, $http, $window, $location) {

    $scope.ipconnection = IP;

    this.token = sessionStorage.token;
    $window.$http = $http;
    $window.$scope = $scope;
    $window.ipconnection = IP;

    if (sessionStorage.user !== undefined) {
      $scope.loggeduser = JSON.parse(sessionStorage.user).firstName;
    }

    $scope.welcomeText = 'Welcome to your paw page';


    $scope.login = function () {
      var username = $scope.username;
      var password = $scope.password;
      var token = btoa(username + ':' + password);
      $http.get('http://' + $scope.ipconnection + '/login', {
        headers: {'Authorization': 'Basic ' + token}
      }).success(function (data, status, headers, config) {

        var usertype = data;

        sessionStorage.token = headers('X-Token');

        var requestRole = usertype.role;

        $http.get('http://' + $scope.ipconnection + '/' + requestRole + '/profile', {
          headers: {'X-Auth-Token': headers('X-Token')}
        }).success(function (data, status, headers, config) {
          sessionStorage.user = JSON.stringify(data);
          $scope.loggeduser = data.firstName;
          if (usertype.role === 'admin') {
            $window.location.href = '/#/admin/';
          } else if ($location.$$path === '/') {
            $scope.getValoration();
          }
        }).error(function (data, status, headers, config) {
        });


      }).error(function (data, status, headers, config) {
      });
    };
    $scope.logout = function () {
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('username');
      sessionStorage.removeItem('user');
      $scope.loggeduser = null;
      $scope.username = '';
      $scope.password = '';

      if ($location.$$path === '/user' || $location.$$path === '/admin') {
        $window.location.href = '/#/';
      }

    };

    $scope.getValoration = function () {
      var token = sessionStorage.token;
      $http.get('http://' + $scope.ipconnection + '/user/valoration', {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        if (jQuery.isEmptyObject(data)) {
          $scope.valoration = data;
          $scope.penddingValoration = true;
        }

      }).error(function (data, status, headers, config) {
        //   $window.location.href = '/#/error';
      });
    };

    $scope.submitlogin = function () {
      $scope.login();
    };
  }]);
});


function fbLoginCallback(window) {

  var IP = window.ipconnection;

  window.$http.get('http://' + IP + '/login/facebook', {
    headers: {
      'X-Facebook-Token': window.FB.getAccessToken()
    }
  }).success(function (data, status, headers) {
    var usertype = data;
    sessionStorage.token = headers('X-Token');
    var requestRole = usertype.role;

    window.$http.get('http://' + IP + '/' + requestRole + '/profile', {
      headers: {'X-Auth-Token': headers('X-Token')}
    }).success(function (data, status, headers, config) {
      sessionStorage.user = JSON.stringify(data);
      window.$scope.loggeduser = data.firstName;
      if (usertype.role === 'admin') {
        window.location.href = '/#/admin/';
      } else {
        //   $window.location.href = '/#/user/';
      }
    }).error(function (data, status, headers, config) {
    });

  }).error(function (data, status, headers, config) {

  });

}

