'use strict';

define([], function () {
  return {
    defaultRoutePath: '/',
    routes: {
      '/': {
        templateUrl: '/views/home.html',
        controller: 'HomeCtrl'
      },
      '/result': {
        templateUrl: '/views/result/ResultCtrl.html',
        controller: 'ResultCtrl'
      },
      '/booking': {
        templateUrl: '/views/booking/BookingCtrl.html',
        controller: 'BookingCtrl'
      },
      '/bookconfirm': {
        templateUrl: '/views/bookconfirm/BookConfirmCtrl.html',
        controller: 'BookConfirmCtrl'
      },
      '/admin': {
        templateUrl: '/views/admin/AdminCtrl.html',
        controller: 'AdminCtrl'
      },
      '/user': {
        templateUrl: '/views/user/UserCtrl.html',
        controller: 'UserCtrl'
      },
      '/showall': {
        templateUrl: '/views/showall/ShowAllCtrl.html',
        controller: 'ShowAllCtrl'
      },
      '/register': {
        templateUrl: '/views/register/RegisterCtrl.html',
        controller: 'RegisterCtrl'
      },
      '/error': {
        templateUrl: '/views/error/ErrorCtrl.html',
        controller: 'ErrorCtrl'
      }
      /* ===== yeoman hook ===== */
      /* Do not remove these commented lines! Needed for auto-generation */
    }
  };
});
