'use strict';
define(['hayequipo'], function (hayequipo) {

  hayequipo.service('datePicker', function () {
    return {
      /**
       *
       * @param scope
       * @param inputId
       * @param classic
       * @param callback
       * @param year
       * @param month
       * @param day
       * @param intervals expected format: {starts, ends}
       */
      generateDatePicker: function (scope, inputId, classic, callback, year, month, day, intervals) {

        var today = new Date();
        var DATE_PICKER_ROWS, DAYS_IN_MONTH_VIEW = 42;
        var onDayChangeCallback;
        var id = inputId;
        var currentYear, currentMonth;
        var MaxDays;
        var mm;
        var yyyy;
        var $scope = scope;
        if ($scope.selectedDate === undefined) {
          $scope.selectedDate = [];
        }
        if (intervals === undefined) {
          intervals = [];
        }

        onDayChangeCallback = callback;
        var dm = today.getDate();
        var dd;
        if (classic !== undefined && classic) {
          dd = day;
          id = inputId;
          generateClassicDatePicker(year, month, dd, dm, year, month, inputId);
        } else {
          dd = today.getDay();
          mm = today.getMonth() + 1; // january is 0
          yyyy = today.getFullYear();
          generateCustomDatePicker(dd, dm, mm, yyyy, inputId);
        }


        function generateClassicDatePicker(year, month, dd, dm, newYear, newMonth, id) {
          DATE_PICKER_ROWS = 6;
          currentYear = newYear;
          currentMonth = newMonth;
          var MaxDaysPrevMonth;
          if (currentMonth === 1) {
            MaxDaysPrevMonth = (new Date(currentYear, (12), 0).getDate());
          } else {
            MaxDaysPrevMonth = (new Date(currentYear, (currentMonth) - 1, 0).getDate());
          }
          MaxDays = (new Date(currentYear, currentMonth, 0).getDate());
          $('<div id="' + id + '-date-picker-container"/>').insertAfter('#' + id + '-date-picker-input');
          yyyy = year;
          mm = month;
          var i;
          $('#' + id + '-date-picker-container').append(
            '<div>' +
            ' <a id="' + id + '-left-arrow"><div class="month-arrow"><p><</p></div></a>' +
            ' <div class="month-name" id="' + id + '-month-name">' + getMonthName(currentMonth) + '</div>' +
            ' <a id="' + id + '-right-arrow"><div class="month-arrow"><p>></p></div></a>' +
            '</div>'
          );

          $('#' + id + '-right-arrow').click(function () {
            $('#' + id + '-date-picker-container').remove();
            if (currentMonth !== 12) {
              generateClassicDatePicker(yyyy, mm, dd, dm, currentYear, currentMonth + 1, id);
            } else {
              generateClassicDatePicker(yyyy, mm, dd, dm, currentYear + 1, 1, id);
            }
          });

          $('#' + id + '-left-arrow').click(function () {
            $('#' + id + '-date-picker-container').remove();
            if (currentMonth !== 1) {
              generateClassicDatePicker(yyyy, mm, dd, dm, currentYear, currentMonth - 1, id);
            } else {
              generateClassicDatePicker(yyyy, mm, dd, dm, currentYear + 1, 12, id);
            }

          });

          for (i = 0; i < DATE_PICKER_ROWS + 1; i++) {
            $('#' + id + '-date-picker-container').append(
              '<div class="day-row-container" id="' + id + '-day-row-container-' + i + '"></div>'
            );
          }

          insertDayHeaders();

          var firstDay = new Date(currentYear, (currentMonth - 1) % 12, 1).getDay();

          for (i = 0; i < DAYS_IN_MONTH_VIEW; i++) {
            var current = i + 1 - firstDay;
            if (current <= 0) {
              // console.log('menor a 0 ' + current);
              $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                '<div class="day-container day-container-void"> <p class="day-text">' + (MaxDaysPrevMonth + current) + '</p> </div>'
              );
            } else if (current > MaxDays) {
              // console.log('mayor a maxdays ' + current);
              $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                '<div class="day-container day-container-void"> <p class="day-text">' + current % MaxDays + '</p> </div>'
              );
            } else {

              var available = (function (constCurrent) {
                return $.grep(intervals, function (interval, index) {
                    return constCurrent >= interval.starts &&
                      constCurrent <= interval.ends;
                  }).length === 0;
              })(new Date(currentYear, currentMonth, current));

              if (available && (currentYear > year || (currentYear === year && currentMonth > month) ||
                (currentYear === year && currentMonth === month && current >= dd))) {
                //	console.log('habilitado ' + current);
                $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                  '<div id="' + id + '-d-' + current + '-' + currentMonth + '" class="day-container ' + id + '-day-container-enabled day-container-enabled" value="' + current +
                  '"><p class="day-text">' + current + '</p></div>'
                );
              } else {
                //	console.log('en el mes pero deshabilitado ' + current);
                $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                  '<div class="day-container day-container-void"> <p class="day-text">' + current + '</p> </div>'
                );
              }

            }

          }

          if (currentMonth === month && currentYear === year) {
            $('#' + id + '-d-' + dd + '-' + currentMonth).addClass(id + '-selected-day selected-day');
          } else {
            $('#' + id + '-d-1-' + currentMonth).addClass(id + '-selected-day selected-day');
          }

          $('#' + id + '-date-picker-input').attr('value', currentYear + '-' + currentMonth + '-' + $('.' + id + '-selected-day').attr('value'));

          updateClassicSelected();

          $('.' + id + '-day-container-enabled').click(updateClassicSelectedOnClick);
        }

        function generateCustomDatePicker(dd, dm, mm, yyyy, id) {
          DATE_PICKER_ROWS = 3;
          var MaxDaysPrevMonth;
          if (mm > 0) {
            MaxDaysPrevMonth = (new Date(yyyy, mm - 1, 0).getDate());
          } else {
            MaxDaysPrevMonth = (new Date(yyyy, 12, 0).getDate());
          }
          var i;
          var daysShown = 21;
          var daysEnabled = 15;

          var MaxDays = (new Date(yyyy, mm, 0).getDate());

          $('<div id="' + id + '-date-picker-container"/>').insertAfter('#' + id + '-date-picker-input');

          for (i = 0; i < DATE_PICKER_ROWS + 1; i++) {
            $('#' + id + '-date-picker-container').append(
              '<div class="day-row-container" id="' + id + '-day-row-container-' + i + '"></div>'
            );
          }

          insertDayHeaders();

          var current;

          for (i = 0; i < daysShown; i++) {
            current = ((dm - dd) + i);
            if (current < dm || current > dm + daysEnabled) {
              if (current <= 0) {
                $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                  '<div class="day-container day-container-void"> <p class="day-text">' + (MaxDaysPrevMonth + current) + '</p> </div>'
                );
              } else if (current <= MaxDays) {
                $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                  '<div class="day-container day-container-void"> <p class="day-text">' + (current) + '</p> </div>'
                );
              } else {
                $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                  '<div class="day-container day-container-void"> <p class="day-text">' + (current - MaxDays) + '</p> </div>'
                );
              }
            } else {
              var t;
              if (current === dm) {
                $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                  '<div id="' + id + '-d-' + current + '-' + mm + '" class="day-container ' + id + '-day-container-enabled day-container-enabled" value="' + (current) +
                  '"><p class="day-text day-text-today">hoy</p></div>'
                );
              } else {
                if (dd === 5 || dd === 6) {
                  if (current === dm + 1) {
                    if (current > MaxDays) {
                      $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                        '<div id="' + id + '-d-' + (current - MaxDays) + '-' + ((mm) % 12 + 1) + '" class="day-container ' + id + '-day-container-enabled day-container-enabled selected-day ' + id + '-selected-day" value="' + (current - MaxDays) +
                        '"><p class="day-text">' + (current - MaxDays) + '</p></div>'
                      );
                    } else {
                      $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                        '<div id="' + id + '-d-' + current + '-' + mm + '" class="day-container ' + id + '-day-container-enabled day-container-enabled selected-day ' + id + '-selected-day" value="' + (current) +
                        '"><p class="day-text">' + (current) + '</p></div>'
                      );
                    }
                  } else {
                    if (current > MaxDays) {
                      $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                        '<div  id="' + id + '-d-' + (current - MaxDays) + '-' + ((mm) % 12 + 1) + '" class="day-container ' + id + '-day-container-enabled day-container-enabled" value="' + (current - MaxDays) +
                        '"><p class="day-text">' + (current - MaxDays) + '</p></div>'
                      );
                    } else {
                      $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                        '<div id="' + id + '-d-' + current + '-' + mm + '" class="day-container ' + id + '-day-container-enabled day-container-enabled" value="' + (current) +
                        '"><p class="day-text">' + (current) + '</p></div>'
                      );
                    }
                  }
                } else {
                  if (current === dm + 5 - dd) {
                    if (current > MaxDays) {
                      $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                        '<div  id="' + id + '-d-' + (current - MaxDays) + '-' + ((mm) % 12 + 1) + '" class="day-container ' + id + '-day-container-enabled day-container-enabled selected-day ' + id + '-selected-day" value="' + (current - MaxDays) +
                        '"><p class="day-text">' + (current - MaxDays) + '</p></div>'
                      );
                    } else {
                      $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                        '<div id="' + id + '-d-' + current + '-' + mm + '" class="day-container ' + id + '-day-container-enabled day-container-enabled ' + id + '-selected-day selected-day" value="' + (current) +
                        '"><p class="day-text">' + (current) + '</p></div>'
                      );
                    }
                  } else {
                    if (current > MaxDays) {
                      $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                        '<div  id="' + id + '-d-' + (current - MaxDays) + '-' + ((mm) % 12 + 1) + '" class="day-container ' + id + '-day-container-enabled day-container-enabled" value="' + (current - MaxDays) +
                        '"><p class="day-text">' + (current - MaxDays) + '</p></div>'
                      );
                    } else {
                      $('#' + id + '-day-row-container-' + parseInt(i / 7 + 1, 10)).append(
                        '<div id="' + id + '-d-' + current + '-' + mm + '" class="day-container ' + id + '-day-container-enabled day-container-enabled" value="' + (current) +
                        '"><p class="day-text">' + (current) + '</p></div>'
                      );
                    }
                  }
                }
              }
            }
          }

          var selectedmm = mm;

          if (parseInt($('.' + id + '-selected-day').attr('value'), 10) < dm) {
            selectedmm++;
          }

          $('#' + id + '-date-picker-input').attr('value', yyyy + '-' + selectedmm + '-' + $('.' + id + '-selected-day').attr('value'));
          updateSelected();

          $('.' + id + '-day-container-enabled').click(updateSelectedOnClick);
        }

        function updateSelectedOnClick() {
          $('.' + id + '-selected-day').removeClass(id + '-selected-day selected-day');
          $(this).addClass(id + '-selected-day selected-day');
          updateSelected();
          if (onDayChangeCallback !== undefined) {
            onDayChangeCallback();
          }
        }

        function updateSelected() {
          var selectedmm = mm;
          if (parseInt($('.' + id + '-selected-day').attr('value'), 10) < dm) {
            selectedmm++;
          }
          var monthZero = '';
          if (selectedmm < 10) {
            monthZero = '0';
          }
          var dayZero = '';
          if ($('.' + id + '-selected-day').attr('value') < 10) {
            dayZero = '0';
          }

          var dateToUpdate = yyyy + '-' + monthZero + selectedmm + '-'
            + dayZero + $('.' + id + '-selected-day').attr('value');

          $('#' + id + '-date-picker-input').attr('value', dateToUpdate);

          $scope.selectedDate[id] = dateToUpdate;
        }

        function updateClassicSelected() {

          var monthZero = '';
          if (currentMonth < 10) {
            monthZero = '0';
          }
          var dayZero = '';
          if ($('.' + id + '-selected-day').attr('value') < 10) {
            dayZero = '0';
          }
          var dateToUpdate = currentYear + '-' + monthZero + currentMonth + '-'
            + dayZero + $('.' + id + '-selected-day').attr('value');
          $('#' + id + '-date-picker-input').attr('value', dateToUpdate);

          $scope.selectedDate[id] = dateToUpdate;
        }

        function updateClassicSelectedOnClick() {
          $('.' + id + '-selected-day').removeClass(id + '-selected-day selected-day');
          $(this).addClass(id + '-selected-day selected-day');
          updateClassicSelected();
          if (onDayChangeCallback !== undefined) {
            onDayChangeCallback();
          }
        }

        function insertDayHeaders() {
          $('#' + id + '-day-row-container-0').append(
            '<div class="day-container day-indicator"> <p class="day-text">' + 'do' + '</p> </div>'
          );

          $('#' + id + '-day-row-container-0').append(
            '<div class="day-container day-indicator"> <p class="day-text">' + 'lu' + '</p> </div>'
          );

          $('#' + id + '-day-row-container-0').append(
            '<div class="day-container day-indicator"> <p class="day-text">' + 'ma' + '</p> </div>'
          );

          $('#' + id + '-day-row-container-0').append(
            '<div class="day-container day-indicator"> <p class="day-text">' + 'mi' + '</p> </div>'
          );

          $('#' + id + '-day-row-container-0').append(
            '<div class="day-container day-indicator"> <p class="day-text">' + 'ju' + '</p> </div>'
          );

          $('#' + id + '-day-row-container-0').append(
            '<div class="day-container day-indicator"> <p class="day-text">' + 'vi' + '</p> </div>'
          );

          $('#' + id + '-day-row-container-0').append(
            '<div class="day-container day-indicator"> <p class="day-text">' + 'sa' + '</p> </div>'
          );

        }

        function getMonthName(month) {
          switch (month) {
            case 1:
              return 'Enero';
            case 2:
              return 'Febrero';
            case 3:
              return 'Marzo';
            case 4:
              return 'Abril';
            case 5:
              return 'Mayo';
            case 6:
              return 'Junio';
            case 7:
              return 'Julio';
            case 8:
              return 'Agosto';
            case 9:
              return 'Septiembre';
            case 10:
              return 'Octubre';
            case 11:
              return 'Noviembre';
            default:
              return 'Diciembre';
          }
        }

        function nextMonth() {
          $('#' + id + '-date-picker-input').remove($('#' + id + '-date-picker-container'));
          if (mm !== 12) {
            generateLegacyDatePicker(yyyy, mm, dm, currentYear, currentMonth);
          } else {
            generateLegacyDatePicker(yyyy, mm, dm, currentYear + 1, 1);
          }
        }

        function prevMonth() {
          $('#' + id + '-date-picker-input').remove($('#' + id + '-date-picker-container'));
          if (mm !== 1) {
            generateLegacyDatePicker(yyyy, mm, dm, currentYear, currentMonth - 1);
          } else {
            generateLegacyDatePicker(yyyy, mm, dm, currentYear + 1, 12);
          }

        }

        scope.selectedDate[id] = $('#' + id + '-date-picker-input').val();
      },

      updateDate: function (id, date) {
        if (date === undefined) {
          return;
        }
        var newDate = date.split('-');
        var newYear = newDate[0];
        var newMonth = parseInt(newDate[1], 10);
        var newDay = parseInt(newDate[2], 10);

        $('#' + id + '-date-picker-input').attr('value', date);
        $('.' + id + '-selected-day').removeClass(id + '-selected-day selected-day');
        $('#' + id + '-d-' + newDay + '-' + newMonth).addClass(id + '-selected-day selected-day');
      }
    };


  });

});
