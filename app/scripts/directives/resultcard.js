'use strict';
define(['hayequipo'], function (hayequipo) {

  hayequipo.directive('resultcard', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/resultcard.html'
    };
  });

});
