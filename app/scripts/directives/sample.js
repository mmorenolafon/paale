'use strict';
define(['hayequipo'], function (hayequipo) {

  hayequipo.directive('sample', function () {
    return {
      restrict: 'E',
      template: '<span>Sample</span>'
    };
  });
});
