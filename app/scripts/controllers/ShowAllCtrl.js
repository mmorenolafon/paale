'use strict';
define(['hayequipo'], function (hayequipo) {


  hayequipo.controller('ShowAllCtrl', ['$scope', '$http', '$location', '$window', function ($scope, $http, $location, $window) {

    var IP = $scope.ipconnection;
    $scope.siteTitle = "Hay Equipo - ver todas";

    if ($scope.history === undefined) {
      $scope.history = [];
    }

    if ($scope.history[0] !== $location.absUrl()) {
      $scope.history.push($location.absUrl());
    }

    $scope.params = $location.search();
    $scope.floor = $scope.params.floor !== undefined;
    $scope.selectedStartTime = $scope.params.beginHour;
    $scope.selectedEndTime = $scope.params.endHour;
    $scope.selectedDate = $scope.params.day;
    $scope.selectedSize = $scope.params.capacity;

    $http.get('http://' + IP + '/pitch?complexId=' + $scope.params.complexId
      + '&floor=' + $scope.params.floor
      + '&capacity=' + $scope.params.capacity
      + '&beginHour=' + $scope.params.beginHour
      + '&endHour=' + $scope.params.endHour
      + '&day=' + $scope.params.day)
      .success(function (data, status, headers, config) {
        $scope.pitches = data;
      }).error(function (data, status, headers, config) {
      $window.location.href = '#/error';
    });

    $scope.complex = {};
    $scope.complex.name = $scope.params.complexName;
    $scope.complex.address = $scope.params.address;
    $scope.complex.phone = $scope.params.phone;
    $scope.complex.email = $scope.params.email;
    var paramsDate = new Date($scope.params.day);
    $scope.date = paramsDate.getDate() + "/" + parseInt(paramsDate.getMonth() + 1) + "/" + paramsDate.getFullYear();

    $scope.goBack = function () {
      $window.location.href = '#/results?'
        + 'floor=' + $scope.params.floor
        + '&capacity=' + $scope.params.capacity
        + '&beginHour=' + $scope.params.beginHour
        + '&endHour=' + $scope.params.endHour
        + '&day=' + $scope.params.day;
    };

    $scope.book = function (result) {
      $window.location.href = '#/booking?'
        + 'address=' + $scope.params.address
        + '&phone=' + $scope.params.phone
        + '&startTime=' + result.startTime
        + '&endTime=' + (parseInt(result.startTime, 10) + 1).toString()
        + '&date=' + $scope.params.day
        + '&price=' + result.price
        + '&floor=' + result.floorName
        + '&email=' + $scope.params.email
        + '&name=' + $scope.params.complexName
        + '&size=' + result.size
        + '&complexId=' + result.complexId
        + '&pitchId=' + result.pitchId;
    };

  }]);
});
