'use strict';
define(['hayequipo'], function (hayequipo) {

  hayequipo.controller('UserCtrl', ['$scope', '$http', '$window', '$location', function ($scope, $http, $window, $location) {
    var token = sessionStorage.token;

    if ($scope.history === undefined) {
      $scope.history = [];
    }

    if ($scope.history[0] !== $location.absUrl()) {
      $scope.history.push($location.absUrl());
    }

    $scope.updateprofile = function () {
      $http.get('http://' + $scope.ipconnection + '/user/profile', {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {

        $scope.firstname = data.firstName;
        $scope.lastname = data.lastName;

        if (data.email.indexOf("@facebook") === -1) {
          $scope.email = data.email;
        }

        $scope.phone = data.phone;
        $scope.username = data.username;
        $scope.fullname = data.firstName + " " + data.lastName;


      }).error(function (data, status, headers, config) {
      });
    };


    $scope.updatebookins = function () {
      $http.get('http://' + $scope.ipconnection + '/user/booking', {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $scope.bookings = data.reserves;
        $scope.history = data.reserveHistory;

        if($scope.bookings.length == 0){
          $scope.noReserves = true;
        }else{
          for(var i = 0; i < $scope.bookings.length; i++){

            if($scope.bookings[i].complexName.length > 15){
              $scope.bookings[i].complexFullName = $scope.bookings[i].complexName;
              $scope.bookings[i].complexName = $scope.bookings[i].complexName.substring(0,15)+"...";
            }

          }
        }

        if($scope.history.length == 0){
          $scope.noHistory = true;
        }else{

          for(var i = 0; i < $scope.history.length; i++){

            if($scope.history[i].complexName.length > 15){
              $scope.history[i].complexFullName = $scope.history[i].complexName;
              $scope.history[i].complexName = $scope.history[i].complexName.substring(0,15)+"...";
            }

          }
        }

        $scope.limitCancelDate = new Date(data.reserveLimit);
      }).error(function (data, status, headers, config) {
      });
    };

    $scope.cancel = function (currentDate) {
      var current = new Date(currentDate);
      if (current > $scope.limitCancelDate) {
        return true;
      } else {
        return false;
      }
    };

    $scope.deleteuserbooking = function (date, begin, complexId, pitchId) {
      $scope.dateremove = date;
      $scope.beginremove = begin;
      $scope.complexremove = complexId;
      $scope.pitchremove = pitchId;
      $('#delete').modal('toggle');
    };

    $scope.confirmdeleteuserbooking = function () {
      var date = $scope.dateremove;
      var begin = $scope.beginremove;
      var complexId = $scope.complexremove;
      var pitchId = $scope.pitchremove;
      var params =
        '?complexId=' + complexId +
        '&pitchId=' + pitchId +
        '&date=' + date +
        '&startTime=' + begin;
      $http.delete('http://' + $scope.ipconnection + '/user/booking' + params, {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $('#delete').modal('hide');
        $scope.updatebookins();
      }).error(function (data, status, headers, config) {
      });
    };

    $scope.updatebookins();
    $scope.updateprofile();

    $scope.wasTrimmed = function(booking){
      return booking.complexFullName!=undefined;
    }

  }]);

});
