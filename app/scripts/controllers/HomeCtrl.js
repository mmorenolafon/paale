'use strict';
define(['hayequipo',
  '../directives/resultcard',
  'services/datePicker'], function (hayequipo) {

  hayequipo.controller('HomeCtrl', ['datePicker', '$scope', '$http', '$window', function (datePickerService, $scope, $http, $window) {

    datePickerService.generateDatePicker(this, 1);

    $scope.states = [];
    $scope.floors = [];
    $scope.selectedBeginTime = {};
    $scope.selectedEndTime = {};
    $scope.selectedSize = {};
    $scope.selectedState = {};

    // this.selectedDate[1] = angular.element(document.querySelector('#1-date-picker-input')).val();
    // $scope.selectedDate[1] = $('#1-date-picker-input').val();

    $scope.sizes = [5, 6, 7, 8, 9, 10, 11];
    $scope.selectedSize = 5;
    $scope.endTimes = [];
    $scope.beginTimes = [];

    for (var i = 7; i < 25; i++) {
      $scope.endTimes.push({
        id: i,
        displayName: i + ':00'
      });
      $scope.beginTimes.push({
        id: i,
        displayName: i + ':00'
      });
    }

    $scope.selectedEndTime = $scope.endTimes[$scope.endTimes.length - 1].id;
    $scope.selectedBeginTime = $scope.beginTimes[0].id;

    $scope.updateState = function (selected) {
    };

    $scope.updateSize = function (selected) {
    };

    $scope.updateDate = function () {
    };

    $scope.updateBeginTime = function () {
      $scope.endTimes = [];
      for (var i = $scope.selectedBeginTime; i <= 24; i++) {
        $scope.endTimes.push({
          id: i,
          displayName: i + ':00'
        });
      }
      if ($scope.selectedBeginTime > $scope.selectedEndTime) {
        $scope.selectedEndTime = $scope.selectedBeginTime;
      }
    };

    $scope.updateEndTime = function () {
      $scope.beginTimes = [];
      for (var i = 7; i <= $scope.selectedEndTime; i++) {
        $scope.beginTimes.push({
          id: i,
          displayName: i + ':00'
        });
      }
      if ($scope.selectedBeginTime > $scope.selectedEndTime) {
        $scope.selectedBeginTime = $scope.selectedEndTime;
      }
    };


    $scope.searchPitches = function () {
      var params = '?';
      if ($scope.selectedState) {
        params += 'stateId=' + $scope.selectedState;
      }
      params += '&floor=' + $scope.selectedFloor +
        '&capacity=' + $scope.selectedSize +
        '&beginHour=' + $scope.selectedBeginTime +
        '&endHour=' + $scope.selectedEndTime +
        '&day=' + $('#1-date-picker-input').val();
      $window.location.href = '#/result' + params;
    };

    $http.get('http://' + $scope.ipconnection + '/states')
      .success(function (data, status, headers, config) {
        $scope.states = data;
        $scope.states.unshift({
          id: 0,
          name: 'Todos'
        });
        $scope.selectedState = 0;
      }).error(function (data, status, headers, config) {
      $window.location.href = '#/error';
    });


    $http.get('http://' + $scope.ipconnection + '/floor')
      .success(function (data, status, headers, config) {
        $scope.floors = data;
        $scope.selectedFloor = 1;
      }).error(function (data, status, headers, config) {
      $window.location.href = '#/error';
    });


    if (typeof String.prototype.supplant !== 'function') {
      String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
          function (a, b) {
            var r = o[b];
            return typeof r === 'string' ?
              r : a;
          });
      };
    }

    var token = sessionStorage.token;

    $scope.sendValoration = function (score) {
      $scope.penddingValoration = false;
      var params =
        '?complexId=' + $scope.valoration.complexId +
        '&valoration=' + score +
        '&date=' + $scope.valoration.date;
      $http({
        method: 'POST',
        url: 'http://' + $scope.ipconnection + '/user/valoration' + params,
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
      }).error(function (data, status, headers, config) {
      });
    };

    $scope.penddingValoration = false;

    if ($scope.loggeduser && sessionStorage.token !== null && sessionStorage.token !== undefined) {
      $scope.getValoration();
    }

  }]);


});
