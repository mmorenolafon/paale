'use strict';
define(['hayequipo'], function (hayequipo) {

  hayequipo.controller('ErrorCtrl', 'location', function ($scope, $location) {
    if ($scope.history === undefined) {
      $scope.history = [];
    }

    if ($scope.history[0] !== $location.absUrl()) {
      $scope.history.push($location.absUrl());
    }

  });

});
