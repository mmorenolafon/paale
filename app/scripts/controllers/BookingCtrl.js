'use strict';
define(['hayequipo'], function (hayequipo) {

  hayequipo.controller('BookingCtrl', ['$scope', '$location', '$http', '$window', function ($scope, $location, $http, $window) {

    if ($scope.history === undefined) {
      $scope.history = [];
    }

    if ($scope.history[0] !== $location.absUrl()) {
      $scope.history.push($location.absUrl());
    }

    $scope.siteTitle = "Hay Equipo - reservar";

    var dt = new Date($location.search().date);

    $scope.isLogged = sessionStorage.getItem('token') !== null;

    $scope.displayPreconfirm = function () {
      $('#preconfirm').modal('toggle');
    };


    $scope.loginAndRegister = function () {

      $scope.isProcessing = true;

      if ( $scope.form != undefined) {
        var username = $scope.form.username;
        var password = $scope.form.password;
      }


      var token = btoa(username + ':' + password);
      $http.get('http://' + $scope.ipconnection + '/login', {
        headers: {'Authorization': 'Basic ' + token}
      }).success(function (data, status, headers, config) {

        $scope.role = data.role;
        var usertype = data;

        sessionStorage.token = headers('X-Token');
        sessionStorage.userRole = usertype.role;

        var requestRole = usertype.role;

        $http.get('http://' + $scope.ipconnection + '/' + requestRole + '/profile', {
          headers: {'X-Auth-Token': headers('X-Token')}
        }).success(function (data, status, headers, config) {
          sessionStorage.user = JSON.stringify(data);
          $scope.loggeduser = data.firstName;
          $('#preconfirm').modal('toggle');
          if (usertype.role === 'admin') {
            $('.modal').modal('hide');
            $window.location.href = '#/admin/';
          } else {
            $scope.confirmBooking();
          }
        }).error(function (data, status, headers, config) {
        });


      }).error(function (data, status, headers, config) {
        $scope.isProcessing = false;
        $scope.registerBookinMessage = 'Usuario y/o contraseña incorrectos';
      });
    };

    $scope.isProcessing = false;
    $scope.confirmBooking = function () {

      $scope.isProcessing = true;

      var token = sessionStorage.token;


      var params = 'complexId=' + $location.search().complexId +
        '&pitchId=' + $location.search().pitchId +
        '&date=' + $location.search().date +
        '&startTime=' + $location.search().startTime;

      var bookingPath = 'http://' + $scope.ipconnection + '/user/booking?' + params;

      $http({
        url: bookingPath,
        method: 'POST',
        data: null,
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $window.location.href = '#/bookconfirm?' + params;
      }).error(function (data, status, headers, config) {
        $scope.isProcessing = false;
        $scope.bookingError = 'Error inesperado en el proceso de reserva.';
      });

    };

    $scope.results =
    {
      address: $location.search().address,
      name: $location.search().name,
      phone: $location.search().phone,
      email: $location.search().email,
      startTime: $location.search().startTime,
      endTime: $location.search().endTime,
      date: dt.getDate() + 1 + '/' + dt.getMonth() + '/' + dt.getFullYear(),
      price: $location.search().price,
      floor: $location.search().floor,
      size: $location.search().size
    };


  }]);

});

