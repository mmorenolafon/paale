'use strict';
define(['hayequipo'], function (hayequipo) {

  hayequipo.controller('BookConfirmCtrl', ['$scope', '$location', '$http', '$window', function ($scope, $location, $http, $window) {

    if ($scope.history === undefined) {
      $scope.history = [];
    }

    if ($scope.history[0] !== $location.absUrl()) {
      $scope.history.push($location.absUrl());
    }

    $scope.acceptedEmailList = [];

    $scope.emailListChange = function () {
      if ($scope.emailList !== undefined && ($scope.emailList.match(/ /g) )) {
        $scope.updateMails();
      }
    };

    $scope.emailListChangeEnter = function () {
      console.log("ENTER");
      if ($scope.emailList !== undefined && ($scope.emailList.match(/ /g) )) {
        $scope.updateMails();
      }
    };


    $('#emailListInput').keypress(function (event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if (keycode === 13) {
        $scope.updateMails();
      }
    });

    $scope.updateMails = function () {

      var newMail = $scope.emailList.replace(/(\r\n|\n|\r)/gm,"").trim();

      console.log("!"+newMail+"!");

      var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
      if (newMail !== undefined) {
        if (!pattern.test(newMail.trim())) {
          $('#email-input-container').addClass('has-error');
        } else {
          $('#email-input-container').removeClass('has-error');
          newMail = newMail.trim();
          if (!($scope.acceptedEmailList.indexOf(newMail) > -1)) {
            $scope.acceptedEmailList.push(newMail);
            $scope.emailList = null;
            $('#invitePlayersBtn').removeClass('disabled');
          }
        }
      }

      $scope.$apply();

    };

    $scope.removeEmail = function (email) {
      var emailName = email.email;
      $scope.acceptedEmailList = jQuery.grep($scope.acceptedEmailList, function (value) {
        return value !== emailName;
      });

      if ($scope.acceptedEmailList.length === 0) {
        $('#invitePlayersBtn').addClass('disabled');
      }
    };


    $scope.invitePlayers = function () {
      var encodedMessg = encodeURIComponent($scope.emailComment);
      var emailList = '';
      for (var i = 0; i < $scope.acceptedEmailList.length; i++) {
        emailList += $scope.acceptedEmailList[i];
        if (i < $scope.acceptedEmailList.length - 1) {
          emailList += '';
        }
      }

      var params = '?complexId=' + $location.search().complexId +
        '&pitchId=' + $location.search().pitchId +
        '&date=' + $location.search().date +
        '&startTime=' + $location.search().startTime +
        '&emails=' + emailList +
        '&message=' + encodedMessg;

      $scope.emailsSent = true;

      $http({
        method: 'POST',
        url: 'http://' + $scope.ipconnection + '/user/invitation' + params,
        headers: {'X-Auth-Token': sessionStorage.token}
      }).success(function (data, status, headers, config) {
      }).error(function (data, status, headers, config) {
      });


    };
  }

  ]);

});
