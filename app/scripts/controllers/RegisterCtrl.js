'use strict';
define(['hayequipo', 'controllers/IndexCtrl'], function (hayequipo) {

  hayequipo.controller('RegisterCtrl', ['$scope', '$http', '$window', '$location', function ($scope, $http, $window, $location) {

    if ($scope.history === undefined) {
      $scope.history = [];
    }

    if ($scope.history[0] !== $location.absUrl()) {
      $scope.history.push($location.absUrl());
    }

    $scope.register = function () {

      $http({
        url: 'http://' + $scope.ipconnection + '/user',
        method: 'POST',
        data: $.param({
          firstName: $scope.firstName,
          lastName: $scope.lastName,
          password: $scope.registerPassword,
          email: $scope.email,
          phone: $scope.phone,
          username: $scope.registerUsername
        }),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data, status, headers, config) {


        var username = $scope.registerUsername;
        var password = $scope.registerPassword;
        var token = btoa(username + ':' + password);
        $http.get('http://' + $scope.ipconnection + '/login', {
          headers: {'Authorization': 'Basic ' + token}
        }).success(function (data, status, headers, config) {

          $scope.role=data.role;
          var usertype = data;

          sessionStorage.token = headers('X-Token');
          sessionStorage.userRole = usertype.role;

          var requestRole = usertype.role;

          $http.get('http://' + $scope.ipconnection + '/' + requestRole + '/profile', {
            headers: {'X-Auth-Token': headers('X-Token')}
          }).success(function (data, status, headers, config) {
            sessionStorage.user = JSON.stringify(data);
            $scope.loggeduser = data.firstName;
            $window.location.href = '#/';
          }).error(function (data, status, headers, config) {
          });


        }).error(function (data, status, headers, config) {
        });








      }).error(function (data, status, headers, config) {
        //$window.location.href = '#/error';
      });
    };
  }
  ]);
});
