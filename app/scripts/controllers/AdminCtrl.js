'use strict';
define(['hayequipo',
  'services/datePicker',
  'jquery-ui'], function (hayequipo) {

  hayequipo.controller('AdminCtrl', ['datePicker', '$scope', '$http', '$window', '$interval', '$location', '$filter',
    function (datePickerService, $scope, $http, $window, $interval, $location, $filter) {

      $('.modal-backdrop').remove();


      if ($scope.history === undefined) {
      $scope.history = [];
    }

    if ($scope.history[0] !== $location.absUrl()) {
      $scope.history.push($location.absUrl());
    }

    $scope.complexes = [];
    $scope.showstatus = false;
    var token = sessionStorage.token;

    $scope.endtimes = [
      {id: 7, value: '7:00'},
      {id: 8, value: '8:00'},
      {id: 9, value: '9:00'},
      {id: 10, value: '10:00'},
      {id: 11, value: '11:00'},
      {id: 12, value: '12:00'},
      {id: 13, value: '13:00'},
      {id: 14, value: '14:00'},
      {id: 15, value: '15:00'},
      {id: 16, value: '16:00'},
      {id: 17, value: '17:00'},
      {id: 18, value: '18:00'},
      {id: 19, value: '19:00'},
      {id: 20, value: '20:00'},
      {id: 21, value: '21:00'},
      {id: 22, value: '22:00'},
      {id: 23, value: '23:00'},
      {id: 24, value: '24:00'}
    ];

    $scope.starttimes = [
      {id: 7, value: '7:00'},
      {id: 8, value: '8:00'},
      {id: 9, value: '9:00'},
      {id: 10, value: '10:00'},
      {id: 11, value: '11:00'},
      {id: 12, value: '12:00'},
      {id: 13, value: '13:00'},
      {id: 14, value: '14:00'},
      {id: 15, value: '15:00'},
      {id: 16, value: '16:00'},
      {id: 17, value: '17:00'},
      {id: 18, value: '18:00'},
      {id: 19, value: '19:00'},
      {id: 20, value: '20:00'},
      {id: 21, value: '21:00'},
      {id: 22, value: '22:00'},
      {id: 23, value: '23:00'},
      {id: 24, value: '24:00'}
    ];

    $interval(function () {
      var params =
        '?date=' + $scope.selectedDate[3] +
        '&complexId=' + $scope.selectedcomplex +
        '&startTime=' + $scope.selectedbegin +
        '&endTime=' + $scope.selectedend;
      if ($scope.username !== undefined && $scope.username !== null) {
        params += '&username=' + $scope.username.toString();
      }
      $http.get('http://' + $scope.ipconnection + '/admin/booking' + params, {
        headers: {'X-Auth-Token': token, 'If-Modified-Since': sessionStorage.getItem('disponibilityLastModified')}
      }).success(function (data, status, headers, config) {
        if (!jQuery.isEmptyObject(data)) {
          $scope.bookings = data;
          $scope.updateavailables($scope.selectedcomplex);
        }
        sessionStorage.setItem('disponibilityLastModified', new Date());
      }).error(function (data, status, headers, config) {
      });
    }, 60000, 0, true);

    $scope.selectedend = 22;
    $scope.selectedbegin = 8;
    $scope.scheduleinput = [];
    $scope.allSelected = false;

    var generateSlider = function (input) {
      $scope.schedules = input;
      if (input === undefined) {
        return;
      }
      var separation = 39;

      $('#schedule_container').empty();
      var daysOfTheWeek = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

      for (var k = 0; k < $scope.schedules.length; k++) {
        var toAppend = $('<div>' + daysOfTheWeek[k].charAt(0) + '</div>')
          .addClass('schedule-day-header')
          .css({
            'top': (separation * k + 4) + 'px'
          })
          .attr({
            'title': daysOfTheWeek[k]
          })
          .tooltip();
        $('#schedule_container').append(toAppend);
        var schedulenodes = [];
        var scheduleprices = [];
        var first = true;
        var lastClosed = false;

        for (var i = 0; i < $scope.schedules[k].prices.length; i++) {

          if ($scope.schedules[k].prices[i] === -1) {
            if (!first) {
              lastClosed = true;
            }
          } else {
            if (first) {
              schedulenodes.push($scope.schedules[k].nodes[i]);
              first = false;
            } else if (lastClosed) {
              scheduleprices.push(-1);
              schedulenodes.push($scope.schedules[k].nodes[i]);
              lastClosed = false;
            }
            scheduleprices.push($scope.schedules[k].prices[i]);
            schedulenodes.push($scope.schedules[k].nodes[i + 1]);
          }
        }
        if (schedulenodes.length === 0) {
          scheduleprices.push(-1);
          schedulenodes.push($scope.schedules[k].nodes[0]);
          schedulenodes.push($scope.schedules[k].nodes[$scope.schedules[k].nodes.length - 1]);
        }

        $scope.schedules[k].nodes = schedulenodes;
        $scope.schedules[k].prices = scheduleprices;

        var current, currentWidth, step = (parseFloat($('#schedule_container').css('width')) - 75) / (24 - 7);
        current = ($scope.schedules[k].nodes[0] - 7) * step + 45;
        $scope.toAppend = $('<div></div>')
          .addClass('schedule-fixed-node')
          .css({
            'left': '30px',
            'z-index': '2',
            'top': (k * separation) + 'px',
            'position': 'absolute'
          })
          .attr({
            'title': 7
          });

        $('#schedule_container').append($scope.toAppend);

        $('.schedule-fixed-node')
          .tooltip();

        $scope.toAppend = $('<input type="number" />')
          .addClass('background_schedule_price')
          .css({
            'left': 30 + 'px',
            'width': ((24 - 7) * step + 30) + 'px',
            'z-index': '1',
            'top': (k * separation + 3) + 'px'
          });

        $('#schedule_container').append($scope.toAppend);


        for (var j = 0; j < $scope.schedules[k].nodes.length; j++) {

          $scope.toAppend = $('<div></div>')
            .addClass('schedule_node schedule_node_' + k)
            .css({
              'left': (current - 15) + 'px',
              'z-index': '3',
              'top': (k * separation) + 'px'
            })
            .attr({
              'title': Math.round($scope.schedules[k].nodes[j]),
              'day': k,
              'data-toggle':'tooltip'
            });
            //.tooltip();



          $('#schedule_container').append($scope.toAppend);
          if (j !== $scope.schedules[k].nodes.length - 1) {
            currentWidth = ($scope.schedules[k].nodes[j + 1] - $scope.schedules[k].nodes[j]) * step;

            $scope.toAppend = $('<input type="number" />')
              .addClass('schedule_price schedule_price_' + k)
              .css({
                'left': current + 'px',
                'width': currentWidth + 'px',
                'z-index': '1',
                'top': (k * separation + 2) + 'px'
              })
              .attr({
                'index': j,
                'value': '$' + $scope.schedules[k].prices[j],
                'min': 0,
                'day': k,
                'type': 'text'
              });
            if ($scope.schedules[k].prices[j] === -1) {
              $scope.toAppend.addClass('closed-entry').attr({
                'value': ''
              });
            }
            $('#schedule_container').append($scope.toAppend);
          }
          current += currentWidth;
        }

        $scope.toAppend = $('<div></div>')
          .addClass('schedule-fixed-node')
          .css({
            'left': ((24 - 7) * step + 30) + 'px',
            'z-index': '2',
            'top': (k * separation) + 'px'
          })
          .attr({
            'title': 24
          })
          .tooltip();
        $('#schedule_container').append($scope.toAppend);

        $('.schedule_price_' + k)
          .unbind('focusin focusout')
          .focusin(function (e) {
            $scope.lastValue = parseInt($(this).val().replace(/[$,]+/g, ''), 10);
            $(this).val(parseInt($scope.lastValue, 10));
            $(this).attr({
              'type': 'number'
            });

            var selectedDay = $(this).attr('day');
            var offset = (parseInt($(this).css('width'), 10) - 150) / 2 + parseFloat($(this).css('left'));
            $('#schedule_container').append('<div id="schedule-context-menu" style="top:'
              + (parseInt($('.schedule_price_' + selectedDay).css('top'), 10) - 25) + 'px; ' +
              'left:' + offset + 'px"></div>');
            $('#schedule-context-menu').append('<div id="left-context-button" class="context-button"></div> ' +
              '<div id="right-context-button" class="context-button"> </div>');
            $scope.toRemove = this;

            var index = parseInt($($scope.toRemove).attr('index'), 10);
            $('#left-context-button').unbind('mousedown');
            if ($scope.schedules[$(this).attr('day')].prices[parseInt($($scope.toRemove).attr('index'), 10)] === -1) {
              $('#left-context-button')
                .mousedown(function (e) {
                  $scope.schedules[selectedDay].prices[parseInt($($scope.toRemove).attr('index'), 10)] = parseInt($($scope.toRemove).attr('value'), 10);
                  $($('.schedule_price_' + selectedDay).get(index)).removeClass('closed-entry');
                  $($scope.toRemove).removeClass('closed-entry');
                  $scope.schedules[selectedDay].prices[parseInt($($scope.toRemove).attr('index'), 10)] = 50;
                  generateSlider($scope.schedules);
                })
                .html('<span>abrir</span>');
            } else {
              $('#left-context-button')
                .mousedown(function (e) {
                  $scope.schedules[selectedDay].prices[parseInt($($scope.toRemove).attr('index'), 10)] = -1;
                  $($('.schedule_price_' + selectedDay).get(index)).addClass('closed-entry');
                  $($scope.toRemove).addClass('closed-entry');
                  generateSlider($scope.schedules);
                })
                .html('<span>cerrar</span>');
            }
            $('#right-context-button')
              .unbind('mousedown')
              .mousedown(function (e) {

                var prom = Math.floor(($scope.schedules[selectedDay].nodes[index + 1] -
                  $scope.schedules[selectedDay].nodes[index]) / 2 +
                  $scope.schedules[selectedDay].nodes[index]);
                if (prom !== $scope.schedules[selectedDay].nodes[index]) {
                  $scope.schedules[selectedDay].nodes.splice(index + 1, 0, prom);
                  $scope.schedules[selectedDay].prices.splice(index + 1, 0, 50);
                }
                generateSlider($scope.schedules);
              })
              .html('<span>dividir</span>');
          })
          .focusout(function (e) {
            var selectedDay = $(this).attr('day');
            $(this).attr({
              'type': 'text'
            });
            if ($(this).val() === '' || parseInt($(this).val() === '', 10) < 0) {
              parseInt($(this).val($scope.lastValue), 10);
            } else {
              $(this).val('$' + Math.max(0, parseInt($(this).val().replace(/[$,]+/g, ''), 10)));
              $scope.schedules[selectedDay].prices[parseInt($(this).attr('index'), 10)] = parseInt($(this).val().replace(/[$,]+/g, ''), 10);
            }


            $('#schedule-context-menu').remove();
          });

        $('.schedule_node_' + k).each(function (index, element) {
          var x1 = $(element).offset().left,
            x2 = $(element).offset().left;
          var selectedDay = $(this).attr('day');
          if (index > 0) {
            x1 -= ($scope.schedules[selectedDay].nodes[index] - $scope.schedules[selectedDay].nodes[index - 1] - 1) * step;
          } else {
            x1 -= ($scope.schedules[selectedDay].nodes[index] - 7) * step;
          }
          if (index < $scope.schedules[selectedDay].nodes.length - 1) {
            x2 += ($scope.schedules[selectedDay].nodes[index + 1] - $scope.schedules[selectedDay].nodes[index] - 1) * step;
          } else {
            x2 += (24 - $scope.schedules[selectedDay].nodes[index]) * step;
          }

          $(element).draggable({
            grid: [step, 10000],
            containment: [
              x1,
              $(element).offset().top,
              x2,
              $(element).offset().top
            ],
            start: function (event, ui) {

              $scope.startsPosition = ui.position.left;

            },
            drag: function (event, ui) {
              $scope.$apply($scope.scheduleChangesSaved = false);

              var prevPrice = $($('.schedule_price_' + selectedDay).get(index - 1));
              var nextPrice = $($('.schedule_price_' + selectedDay).get(index));
              var prevNode = $($('.schedule_node_' + selectedDay).get(index - 1));
              var nextNode = $($('.schedule_node_' + selectedDay).get(index + 1));

              if (index > 0) {
                prevPrice.css('width', ($(element).offset().left - prevNode.offset().left) + 'px');
              }

              if (index < $scope.schedules[parseInt($(this).attr('day'), 10)].nodes.length - 1) {
                var nextWidth = (nextNode.offset().left - $(element).offset().left) + 'px';
                var nextLeft = (ui.position.left + 15) + 'px';
                nextPrice.css({
                  'width': nextWidth,
                  'left': nextLeft
                });
              }
            },
            stop: function (envent, ui) {
              if (ui.position.left !== $scope.startsPosition) {
                $scope.schedules[$(this).attr('day')].nodes[index] += Math.round((ui.position.left - $scope.startsPosition) / step);
              }

              generateSlider($scope.schedules);
            }
          });

        });
      }

    };

    $scope.scheduleUpdateValues = function (token) {

      $scope.scheduleChangesSaved = false;
      $scope.selectedPitch = undefined;
      $scope.all = true;

      for (var m = 0; m < $scope.filterPitches.length; m++) {

        if ($scope.filterPitches[m].selected === true) {
          for (var p = 0; p < $scope.pitchesRawSchedules.length && $scope.selectedPitch === undefined; p++) {
            if ($scope.pitchesRawSchedules[p].pitch.pitchId === $scope.filterPitches[m].pitchId
              && $scope.selectedPitch === undefined) {
              $scope.selectedPitch = $scope.pitchesRawSchedules[p];
            }
          }
        } else {
          $scope.all = false;
        }
      }
      $scope.allSelected = $scope.all;
      if ($scope.selectedPitch !== undefined) {
        console.log(true, [], $scope.selectedPitch.timeTables);

        var copyArr = $.extend(true, [], $scope.selectedPitch.timeTables);
        console.log('generating component');
        generateSlider(copyArr);
      } else {
        generateSlider([]);
      }
    };

    $scope.updatebooking = function (complexId) {
      var params =
        '?date=' + $scope.selectedDate[3] +
        '&complexId=' + complexId +
        '&startTime=' + $scope.selectedbegin +
        '&endTime=' + $scope.selectedend;
      if ($scope.usernameadmin !== undefined && $scope.usernameadmin !== null) {
        params += '&username=' + $scope.usernameadmin.toString();
      }
      $http.get('http://' + $scope.ipconnection + '/admin/booking' + params, {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        if (jQuery.isEmptyObject(data)) {
          $scope.emptyResultsBookings = true;
        } else {
          $scope.emptyResultsBookings = false;
        }
        $scope.bookings = data;
        $scope.isLoading = false;
      }).error(function (data, status, headers, config) {
      });
    };

    $scope.updateavailables = function (complexId) {
      var params =
        '?date=' + $scope.selectedDate[3] +
        '&complexId=' + complexId +
        '&startTime=' + $scope.selectedbegin +
        '&endTime=' + $scope.selectedend;
      $http.get('http://' + $scope.ipconnection + '/admin/complex/pitches' + params, {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        if (jQuery.isEmptyObject(data)) {
          $scope.emptyResults = true;
        } else {
          $scope.emptyResults = false;
        }
        $scope.availables = data;
        $scope.isLoading = false;
      }).error(function (data, status, headers, config) {
           $window.location.href = '#/error';
      });

    };

    $scope.addexception = function () {
      var start = $scope.selectedDate[4];
      var end = $scope.selectedDate[5];
      var complexId = $scope.selectedcomplex;
      var params =
        '?beginDate=' + start +
        '&complexId=' + complexId +
        '&endDate=' + end;
      $http({
        method: 'POST',
        url: 'http://' + $scope.ipconnection + '/admin/exception' + params,
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $scope.updateexceptions($scope.selectedcomplex);
      }).error(function (data, status, headers, config) {
      });
    };

    $scope.updateexceptions = function () {
      var complexId = $scope.selectedcomplex;
      $http.get('http://' + $scope.ipconnection + '/admin/exception?complexId=' + complexId, {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $scope.exceptions = data;
      }).error(function (data, status, headers, config) {
      });
    };

    $scope.removeexception = function (begin, end) {
      $scope.exceptionbegin = begin;
      $scope.exceptionend = end;
      $('#delete-condition-modal').modal('toggle');
    };

    $scope.confirmremoveexception = function () {
      var begin = $scope.exceptionbegin;
      var end = $scope.exceptionend;
      var complexId = $scope.selectedcomplex;
      var params =
        '?beginDate=' + begin +
        '&complexId=' + complexId +
        '&endDate=' + end;
      $http({
        method: 'DELETE',
        url: 'http://' + $scope.ipconnection + '/admin/exception' + params,
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $('#delete-condition-modal').modal('hide');
        $scope.updateexceptions($scope.selectedcomplex);
      }).error(function (data, status, headers, config) {
      });

    };

    $scope.makebooking = function (pitchId, startTime, complexId, date) {
      var params =
        '?date=' + $scope.selectedDate[3] +
        '&complexId=' + complexId +
        '&startTime=' + startTime +
        '&pitchId=' + pitchId;
      if ($scope.usernameadmin === '' || $scope.usernameadmin === undefined || $scope.usernameadmin === null) {
        $('#no-username-modal').modal('toggle');
        return;
      }
      params += '&bookerUsername=' + $scope.usernameadmin;
      $http({
        method: 'POST',
        url: 'http://' + $scope.ipconnection + '/admin/booking' + params,
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $scope.usernameadmin = null;
        $scope.updatebyfilters($scope.selectedcomplex);
      }).error(function (data, status, headers, config) {
      });
    };

    $scope.removebooking = function (pitchId, complexId, date, startTime) {
      $scope.removecplexid = complexId;
      $scope.removepitchid = pitchId;
      $scope.removestarttime = startTime;
      $scope.removedate = date;
      $('#delete').modal('toggle');
    };

    $scope.confirmremove = function () {
      var params =
        '?date=' + $scope.removedate +
        '&complexId=' + $scope.removecplexid +
        '&startTime=' + $scope.removestarttime +
        '&pitchId=' + $scope.removepitchid;
      $http.delete('http://' + $scope.ipconnection + '/admin/booking' + params, {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $('#delete').modal('hide');
        $scope.updatebyfilters($scope.selectedcomplex);
      }).error(function (data, status, headers, config) {
        //    $window.location.href = '/#/error';
      });
    };

    $scope.updatebyfilters = function (complexId) {
      $scope.isLoading = true;
      $scope.updatebooking(complexId);
      $scope.updateavailables(complexId);
      $scope.setdetails();
      sessionStorage.setItem('disponibilityLastModified', new Date());
    };

    $scope.updatedatepicker = function () {
      $scope.isLoading = true;
      $scope.updatebooking($scope.selectedcomplex);
      $scope.updateavailables($scope.selectedcomplex);
    };

    $scope.updatedatepicker2 = function () {
      var beginDate = $('#4-date-picker-input').val();
      var endDate = $('#5-date-picker-input').val();
      var endDateSplitted = beginDate.split('-');
      $('#5-date-picker-container').remove();
      datePickerService.generateDatePicker($scope, 5, true, undefined, parseInt(endDateSplitted[0], 10), parseInt(endDateSplitted[1], 10), parseInt(endDateSplitted[2], 10));
      if (Date.parse(endDate) > Date.parse(beginDate)) {
        datePickerService.updateDate(5, endDate);
      }
    };

    $scope.updatestarttime = function () {
      $scope.endtimes = [];
      for (var i = $scope.selectedbegin; i <= 24; i++) {
        $scope.endtimes.push({
          id: i,
          value: i + ':00'
        });
      }
      if ($scope.selectedbegin > $scope.selectedend) {
        $scope.selectedend = $scope.selectedbegin;
      }
      $scope.updatebyfilters($scope.selectedcomplex);
    };

    $scope.updateendtime = function () {
      $scope.starttimes = [];
      for (var i = 7; i <= $scope.selectedend; i++) {
        $scope.starttimes.push({
          id: i,
          value: i + ':00'
        });
      }
      if ($scope.selectedbegin > $scope.selectedend) {
        $scope.selectedbegin = $scope.selectedend;
      }
      $scope.updatebyfilters($scope.selectedcomplex);
    };

    $scope.updatecomplex = function (complexId) {
      $scope.updatebyfilters(complexId);
      $scope.updateexceptions();
    };
    $scope.selectedDate = [];

    $scope.selectedDate[3] = $('#3-date-picker-input').val();
    $scope.selectedDate[4] = $('#4-date-picker-input').val();
    $scope.selectedDate[5] = $('#5-date-picker-input').val();

    datePickerService.generateDatePicker($scope, 3, false, $scope.updatedatepicker);

    var dateForDp = new Date();
    dateForDp.setDate(dateForDp.getDate() + 15);
    var dd = dateForDp.getDate();
    var mm = dateForDp.getMonth() + 1;
    var yy = dateForDp.getFullYear();

    datePickerService.generateDatePicker($scope, 4, true, $scope.updatedatepicker2, yy, mm, dd);
    datePickerService.generateDatePicker($scope, 5, true, undefined, yy, mm, dd);

    $scope.modifyphone = function (value) {
      $scope.phoneModify = value;
    };

    $scope.modifyemail = function (value) {
      $scope.emailModify = value;
    };

    $scope.setdetails = function () {
      for (i = 0; i < $scope.complexes.length; i++) {
        if ($scope.selectedcomplex === $scope.complexes[i].complexId) {
          $scope.currentcomplex = $scope.complexes[i];
          $scope.newemail = $scope.currentcomplex.email;
          $scope.newphone = $scope.currentcomplex.phone;
          $scope.oldemail = $scope.currentcomplex.email;
          $scope.oldphone = $scope.currentcomplex.phone;
          $scope.complexaddress = $scope.currentcomplex.address;
        }
      }
    };

    $scope.submitphone = function () {
      $scope.updatecomplexdetails($scope.oldemail, $scope.newphone);
    };

    $scope.submitemail = function () {
      $scope.updatecomplexdetails($scope.newemail, $scope.oldphone);
    };

    $scope.updatecomplexdetails = function (email, phone) {
      var complexId = $scope.selectedcomplex;
      var params =
        '?complexId=' + complexId +
        '&email=' + email +
        '&phone=' + phone;
      $http({
        method: 'POST',
        url: 'http://' + $scope.ipconnection + '/admin/complex' + params,
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $scope.modifyemail(false);
        $scope.modifyphone(false);
        $http.get('http://' + $scope.ipconnection + '/admin/complex?complexId=' + $scope.selectedcomplex, {
          headers: {'X-Auth-Token': token}
        }).success(function (data, status, headers, config) {
          for (i = 0; i < $scope.complexes.length; i++) {
            if ($scope.selectedcomplex === $scope.complexes[i].complexId) {
              $scope.complexes[i] = data;
            }
          }
          $scope.setdetails();
        }).error(function (data, status, headers, config) {
          //  $window.location.href = '/#/error';
        });
      }).error(function (data, status, headers, config) {
      });

    };
    $scope.isLoading = true;

    $http.get('http://' + $scope.ipconnection + '/admin/profile', {
      headers: {'X-Auth-Token': token}
    }).success(function (data, status, headers, config) {
      $scope.details = data;
      for (i = 0; i < $scope.details.complexesIDs.length; i++) {
        $http.get('http://' + $scope.ipconnection + '/admin/complex?complexId=' + $scope.details.complexesIDs[i], {
          headers: {'X-Auth-Token': token}
        }).success(function (data, status, headers, config) {
          $scope.complexes.push(data);
          $scope.selectedcomplex = data.complexId;
        }).error(function (data, status, headers, config) {
          //  $window.location.href = '/#/error';
        });
      }
      $scope.updatebyfilters(data.complexesIDs[i - 1]);
    }).error(function (data, status, headers, config) {
      //   $window.location.href = '/#/error';
    });


    $scope.bshowReserve = true;
    $scope.bshowStatus = false;
    $scope.bshowSchedule = false;

    $scope.showReserve = function () {
      $('.selected-admin-tab').removeClass('selected-admin-tab');
      $('#reserves-admin-tab').addClass('selected-admin-tab');
      $scope.bshowStatus = false;
      $scope.bshowReserve = true;
      $scope.bshowSchedule = false;
      $scope.showstatus = false;
      $scope.usernameadmin = null;
    };
    $scope.showStatus = function () {
      $('.selected-admin-tab').removeClass('selected-admin-tab');
      $('#status-admin-tab').addClass('selected-admin-tab');
      $scope.emptyResults = false;
      $scope.emptyResultsBookings = false;
      $scope.bshowStatus = true;
      $scope.bshowReserve = false;
      $scope.bshowSchedule = false;
      $scope.showstatus = true;
      $scope.usernameadmin = null;
      $scope.setdetails();
      $scope.updateexceptions();
    };
    $scope.showSchedule = function () {
      $('.selected-admin-tab').removeClass('selected-admin-tab');
      $('#schedules-admin-tab').addClass('selected-admin-tab');
      $scope.emptyResults = false;
      $scope.emptyResultsBookings = false;
      $scope.bshowStatus = false;
      $scope.bshowReserve = false;
      $scope.bshowSchedule = true;
      $scope.usernameadmin = null;
      $scope.updatepitches();
    };

    $scope.modifyphone = function (value) {
      $scope.phoneModify = value;
    };

    $scope.modifyemail = function (value) {
      $scope.emailModify = value;
    };
    var i;

    $scope.updatepitches = function (complexId) {

      if (!$scope.bshowSchedule) {
        return;
      }

      var params =
        '?complexId=' + $scope.selectedcomplex;
      $http.get('http://' + $scope.ipconnection + '/complex/schedules' + params, {
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {


        $scope.pitchesRawSchedules = data;

        $scope.pitchFloorList = [];
        $scope.pitchSizeList = [];
        $scope.pitches = [];
        $scope.filterPitches = [];

        for (var i = 0; i < data.length; i++) {
          $scope.pitchFloorList.push(data[i].pitch.floor);
          $scope.pitchSizeList.push(data[i].pitch.size);
          $scope.pitches.push(data[i].pitch);
        }

        $scope.filterPitches = $scope.pitches;


        $.unique($scope.pitchFloorList);
        $.unique($scope.pitchSizeList);

        if ($scope.pitchFloorList.length > 1) {
          $scope.pitchFloorList.push('Todos');
          $scope.selectedPitchFloor = 'Todos';
        } else {
          $scope.selectedPitchFloor = data[0].pitch.floor;
        }

        if ($scope.pitchSizeList.length > 1) {
          $scope.pitchSizeList.push('Todos');
          $scope.selectedPitchSize = 'Todos';
        } else {
          $scope.selectedPitchSize = data[0].pitch.size;
        }


      }).error(function (data, status, headers, config) {
        $window.location.href = '#/error';
      });
    };

    $scope.updatePitchFloor = function (selectedPitchFloor) {

      $scope.filterPitches = [];
      for (var i = 0; i < $scope.pitches.length; i++) {
        if ((selectedPitchFloor === 'Todos' || $scope.pitches[i].floor === selectedPitchFloor) && ($scope.selectedPitchSize === 'Todos'
          || $scope.selectedPitchSize === $scope.pitches[i].size)) {
          $scope.filterPitches.push($scope.pitches[i]);
        }
      }

    };

    $scope.updatePitchSize = function (selectedPitchSize) {

      $scope.filterPitches = [];
      for (var i = 0; i < $scope.pitches.length; i++) {
        if ((selectedPitchSize === 'Todos' || $scope.pitches[i].size === selectedPitchSize) && ($scope.selectedPitchFloor === 'Todos'
          || $scope.selectedPitchFloor === $scope.pitches[i].floor)) {
          $scope.filterPitches.push($scope.pitches[i]);
        }
      }

    };

    $scope.selectAllPitches = function () {

      var changeAll = undefined;

      if (!$('#selectAllPitchesCheck').prop('checked')) {

        if ($('input:checkbox:not(:checked)').length - 1 === 0) {
          $('.pitch-checkbox').prop('checked', false);
          changeAll = false;
        }

      } else if ($('.pitch-checkbox').prop('checked').length !== $('.pitch-checkbox').length) {
        $('.pitch-checkbox').prop('checked', true);
        changeAll = true;
      } else {
        $('.pitch-checkbox').prop('checked', false);
        changeAll = false;
      }

      if (changeAll !== undefined) {
        for (var i = 0; i < $scope.filterPitches.length; i++) {
          $scope.filterPitches[i].selected = changeAll;
        }
      }

      $scope.scheduleUpdateValues();

    };

    $scope.scheduleResetModal = function () {
      $('#scheduleReset').modal('toggle');
    };

    $scope.scheduleSaveModal = function () {
      $('#scheduleSave').modal('toggle');
    };

    $scope.resetChanges = function () {
      $('#scheduleReset').modal('toggle');
      $scope.scheduleUpdateValues();
    };

    $scope.saveChanges = function () {
      var toApi = {};
      toApi.pitchIds = [];

      for (var i = 0; i < $scope.filterPitches.length; i++) {
        if ($scope.filterPitches[i].selected) {
          toApi.pitchIds.push($scope.filterPitches[i].pitchId);
        }
      }

      toApi.timeTables = $scope.schedules;
      toApi.complexId = $scope.selectedcomplex;

      $scope.savingSchedules = true;
      $scope.scheduleChangesSaved = false;

      $http({
        method: 'POST',
        url: 'http://' + $scope.ipconnection + '/complex/schedules?ttablesJson=' + JSON.stringify(toApi),
        headers: {'X-Auth-Token': token}
      }).success(function (data, status, headers, config) {
        $scope.savingSchedules = false;
        $scope.scheduleChangesSaved = true;
        $('#scheduleSave').modal('toggle');

      }).error(function (data, status, headers, config) {
      });


    };
    // component

  }]);


});
