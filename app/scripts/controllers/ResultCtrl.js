'use strict';
define(['hayequipo',
  '../directives/resultcard',
  'services/datePicker'], function (hayequipo) {

  hayequipo.controller('ResultCtrl', ['datePicker', '$scope', '$http', '$location', '$window', function (datePickerService, $scope, $http, $location, $window) {
    history.pushState({}, "Hay Equipo - resultados", $location.absUrl());

    if ($scope.history === undefined) {
      $scope.history = [];
    }

    if ($scope.history[0] !== $location.absUrl()) {
      $scope.history.push($location.absUrl());
    }

    document.title = "Hay Equipo - resultados";
    var params = $location.search();
    var lastDate = params.day.split('-');
    var calendarId = 2;

    $scope.updatedatepicker = function () {
      $scope.updateResults();
    };

    datePickerService.generateDatePicker($scope, calendarId, false, $scope.updatedatepicker);

    $('#' + calendarId + '-date-picker-input').attr('value', params.day);
    $('.' + calendarId + '-selected-day').removeClass(calendarId + '-selected-day selected-day');
    $('#' + calendarId + '-d-' + parseInt(lastDate[2], 10) + '-' + parseInt(lastDate[1], 10)).addClass(calendarId + '-selected-day selected-day');

    $scope.results = [];
    $scope.states = [];
    $scope.floor = [];
    $scope.selectedBeginTime = parseInt(params.beginHour, 10);
    $scope.selectedEndTime = parseInt(params.endHour, 10);
    $scope.selectedSize = parseInt(params.capacity, 10);
    $scope.selectedDate[2] = params.day;

    // this.selectedDate[2 = angular.element(document.querySelector('#1-date-picker-input')).val();

    $scope.sizes = [5, 6, 7, 8, 9, 10, 11];

    $scope.endTimes = [];
    $scope.beginTimes = [];

    for (var i = 7; i < 25; i++) {
      $scope.endTimes.push({
        id: i,
        displayName: i + ':00'
      });
      $scope.beginTimes.push({
        id: i,
        displayName: i + ':00'
      });
    }

    $scope.updateState = function () {
      $scope.updateResults();
    };

    $scope.updateSize = function () {
      $scope.updateResults();
    };

    $scope.updateDate = function () {
      $scope.updateResults();
    };

    $scope.updateFloor = function () {
      $scope.updateResults();
    };

    $scope.updateBeginTime = function () {
      $scope.endTimes = [];
      for (var i = $scope.selectedBeginTime; i <= 24; i++) {
        $scope.endTimes.push({
          id: i,
          displayName: i + ':00'
        });
      }
      if ($scope.selectedBeginTime > $scope.selectedEndTime) {
        $scope.selectedEndTime = $scope.selectedBeginTime;
      }
      $scope.updateResults();
    };

    $scope.updateEndTime = function () {
      $scope.beginTimes = [];
      for (var i = 7; i <= $scope.selectedEndTime; i++) {
        $scope.beginTimes.push({
          id: i,
          displayName: i + ':00'
        });
      }
      if ($scope.selectedBeginTime > $scope.selectedEndTime) {
        $scope.selectedBeginTime = $scope.selectedEndTime;
      }
      $scope.updateResults();
    };

    $scope.updateResults = function () {
      this.results = null;
      this.emptyResults = false;
      parameters = '?';
      if ($scope.selectedState) {
        parameters += 'stateId=' + $scope.selectedState;
      }
      parameters += '&floor=' + $scope.selectedFloor +
        '&capacity=' + $scope.selectedSize +
        '&beginHour=' + $scope.selectedBeginTime +
        '&endHour=' + $scope.selectedEndTime +
        '&day=' + $scope.selectedDate[2];

      $http.get('http://' + $scope.ipconnection + '/complex' + parameters)
        .success(function (data, status, headers, config) {
          $scope.results = data;
          if (jQuery.isEmptyObject(data)) {
            $scope.emptyResults = true;
          } else {
            $scope.emptyResults = false;
          }
        }).error(function (data, status, headers, config) {
        $window.location.href = '#/error';
      });
    };

    $scope.book = function (result) {

      $window.location.href = '#/booking?complexId=' + result.complexId
        + '&pitchId=' + result.availablePitch.pitchId
        + '&date=' + $scope.selectedDate[2]
        + '&floor=' + result.availablePitch.floorName
        + '&size=' + $scope.selectedSize
        + '&address=' + result.address
        + '&name=' + result.name
        + '&email=' + result.email
        + '&price=' + result.availablePitch.price
        + '&phone=' + result.phone
        + '&startTime=' + result.availablePitch.startTime
        + '&endTime=' + result.availablePitch.endTime;


    };

    $scope.showAll = function (result) {
      $window.location.href = '#/showall?complexId=' + result.complexId
        + '&floor=' + $scope.selectedFloor
        + '&floorName=' + result.availablePitch.floorName
        + '&capacity=' + result.availablePitch.size
        + '&beginHour=' + $scope.selectedBeginTime
        + '&endHour=' + $scope.selectedEndTime
        + '&day=' + $scope.selectedDate[2]
        + '&complexName=' + result.name
        + '&phone=' + result.phone
        + '&email=' + result.email
        + '&address=' + result.address;
    };

    $scope.results = null;


    $http.get('http://' + $scope.ipconnection + '/states')
      .success(function (data, status, headers, config) {
        $scope.states = data;
        $scope.states.unshift({
          id: 0,
          name: 'Todos'
        });
        if (params.stateId) {
          $scope.selectedState = parseInt(params.stateId, 10);
        } else {
          $scope.selectedState = 0;
        }
      }).error(function (data, status, headers, config) {
      $window.location.href = '#/error';
    });

    $http.get('http://' + $scope.ipconnection + '/floor')
      .success(function (data, status, headers, config) {
        $scope.floors = data;
        $scope.selectedFloor = parseInt(params.floor, 10);
      }).error(function (data, status, headers, config) {
      $window.location.href = '#/error';
    });

    var parameters = '?';
    if (params.state) {
      parameters += 'stateId=' + params.state;
    }
    parameters += '&floor=' + params.floor +
      '&capacity=' + params.capacity +
      '&beginHour=' + params.beginHour +
      '&endHour=' + params.endHour +
      '&day=' + params.day;

    $http.get('http://' + $scope.ipconnection + '/complex' + parameters)
      .success(function (data, status, headers, config) {
        console.log(data);
        $scope.results = data;
        if (jQuery.isEmptyObject(data)) {
          $scope.emptyResults = true;
        } else {
          $scope.emptyResults = false;
        }
      }).error(function (data, status, headers, config) {
      $window.location.href = '#/error';
    });

    // $http.get('http://' +  + ':8080/complex')
    //   .success(function (data, status, headers, config) {
    //   });
  }]);
});

