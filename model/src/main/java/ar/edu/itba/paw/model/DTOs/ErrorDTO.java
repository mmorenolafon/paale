package ar.edu.itba.paw.model.DTOs;

/**
 * Created by tritoon on 08/11/16.
 */
public class ErrorDTO {

    private String field;
    private String message;

    public ErrorDTO() {
    }

    public ErrorDTO(String field,String message) {
        this.message = message;
        this.field = field;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }

    public void setField(String field) {
        this.field = field;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
