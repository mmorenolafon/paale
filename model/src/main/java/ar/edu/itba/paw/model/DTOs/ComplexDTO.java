package ar.edu.itba.paw.model.DTOs;


public class ComplexDTO {
    private int complexId;
    private String name;
    private String email;
    private String address;
    private String phone;


    public ComplexDTO() {
    }

    public ComplexDTO(int complexId, String name, String email, String address, String phone) {
        this.complexId = complexId;
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public int getComplexId() {
        return complexId;
    }

    public void setComplexId(int complexId) {
        this.complexId = complexId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
