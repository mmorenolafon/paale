package ar.edu.itba.paw.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class PitchId implements Serializable {


    @ManyToOne
    private Complex complex;

    @Column(name = "id", table = "pitch")
    private int pitchIdByComplex;

    public PitchId(Complex complex, int pitchIdByComplex) {
        this.complex = complex;
        this.pitchIdByComplex = pitchIdByComplex;
    }

    public PitchId() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PitchId)) return false;

        PitchId pitchId = (PitchId) o;

        if (getComplexId() != pitchId.getComplexId()) return false;
        return getPitchIdByComplex() == pitchId.getPitchIdByComplex();

    }


    public int getComplexId() {

        return complex.getId();
    }

    public Complex getComplex() {
        return this.complex;
    }

    public void setComplex(Complex complex) {
        this.complex = complex;
    }

    public int getPitchIdByComplex() {
        return pitchIdByComplex;
    }

    public void setPitchIdByComplex(int pitchIdByComplex) {
        this.pitchIdByComplex = pitchIdByComplex;
    }

    public String toString() {
        return "{\"PitchId\":\"complexId\":\"" + complex.getId() + "\", \"pitchId\":\"" + this.pitchIdByComplex + "\"}";
    }
}
