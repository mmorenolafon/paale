package ar.edu.itba.paw.model.DTOs;

/**
 * Created by tritoon on 29/11/16.
 */
public class PitchDTO {

  private int pitchId;
  private int size;
  private String floor;

  public PitchDTO() {
  }

  public PitchDTO(int pitchId, int size, String floor) {
    this.pitchId = pitchId;
    this.size = size;
    this.floor = floor;
  }

  public int getPitchId() {
    return pitchId;
  }

  public void setPitchId(int id) {
    this.pitchId = id;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public String getFloor() {
    return floor;
  }

  public void setFloor(String floor) {
    this.floor = floor;
  }
}
