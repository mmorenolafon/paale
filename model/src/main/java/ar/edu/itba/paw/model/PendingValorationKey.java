package ar.edu.itba.paw.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
public class PendingValorationKey implements Serializable {

  @ManyToOne
  private User user;

  @ManyToOne
  private Complex complex;

  @Column(name = "date", nullable = false)
  @Convert(converter = LocalDateAttributeConverter.class)
  private LocalDate date;

  @Column(name = "time")
  private int time;

  public PendingValorationKey() {
  }

  public PendingValorationKey(User user, Complex complex, LocalDate date, int time) {
    this.user = user;
    this.complex = complex;
    this.date = date;
    this.time = time;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Complex getComplex() {
    return complex;
  }

  public void setComplex(Complex complex) {
    this.complex = complex;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public int getTime() {
    return time;
  }

  public void setTime(int time) {
    this.time = time;
  }


}
