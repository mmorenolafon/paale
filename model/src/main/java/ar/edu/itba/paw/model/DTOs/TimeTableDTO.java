package ar.edu.itba.paw.model.DTOs;

import java.util.List;

/**
 * Created by tritoon on 29/11/16.
 */
public class TimeTableDTO {

  private List<Integer> nodes;
  private List<Double> prices;

  public TimeTableDTO() {
  }

  public TimeTableDTO(List<Integer> times, List<Double> prices) {
    this.nodes = times;
    this.prices = prices;
  }

  public List<Integer> getNodes() {
    return nodes;
  }

  public void setNodes(List<Integer> nodes) {
    this.nodes = nodes;
  }

  public List<Double> getPrices() {
    return prices;
  }

  public void setPrices(List<Double> prices) {
    this.prices = prices;
  }
}
