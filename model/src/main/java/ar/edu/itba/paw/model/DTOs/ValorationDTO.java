package ar.edu.itba.paw.model.DTOs;

import java.time.LocalDate;

/**
 * Created by tritoon on 30/11/16.
 */
public class ValorationDTO {

  private int complexId;
  String complexName;
  private LocalDate date;

  public ValorationDTO() {
  }

  public ValorationDTO( int complexId, String complexName, LocalDate date) {
    this.complexId = complexId;
    this.complexName = complexName;
    this.date = date;
  }

  public int getComplexId() {
    return complexId;
  }

  public void setComplexId(int complexId) {
    this.complexId = complexId;
  }

  public String getComplexName() {
    return complexName;
  }

  public void setComplexName(String complexName) {
    this.complexName = complexName;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }
}
