package ar.edu.itba.paw.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
public class ScheduleExceptionId implements Serializable {

    @ManyToOne
    private Complex complex;

    @Column(name = "beginDate", nullable = false)
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate beginDate;

    @Column(name = "endDate", nullable = false)
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate endDate;


    public ScheduleExceptionId(Complex complex, LocalDate beginDate, LocalDate endDate) {
        this.complex = complex;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public ScheduleExceptionId() {
    }

    public Complex getComplex() {
        return this.complex;
    }

    public void setComplex(Complex complex) {
        this.complex = complex;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScheduleExceptionId that = (ScheduleExceptionId) o;

        if (!complex.equals(that.complex)) return false;
        if (!beginDate.equals(that.beginDate)) return false;
        return endDate.equals(that.endDate);

    }

    @Override
    public int hashCode() {
        int result = complex.hashCode();
        result = 31 * result + beginDate.hashCode();
        result = 31 * result + endDate.hashCode();
        return result;
    }
}
