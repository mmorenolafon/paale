package ar.edu.itba.paw.model.DTOs;

import java.util.List;

/**
 * Created by tritoon on 10/10/16.
 */
public class AdminDTO {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String username;

    private List<Integer> complexesIDs;

    public AdminDTO() {
    }

    public AdminDTO(String firstName, String lastName, String email, String phone, String username, List<Integer> complexesIDs) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.username = username;
        this.complexesIDs = complexesIDs;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Integer> getComplexesIDs() {
        return complexesIDs;
    }

    public void setComplexesIDs(List<Integer> complexesIDs) {
        this.complexesIDs = complexesIDs;
    }
}
