package ar.edu.itba.paw.model;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;


@Embeddable
public class ValorationKey implements Serializable {

    @ManyToOne(optional = false)
    private Complex complex;

    @ManyToOne(optional = false)
    private User user;

    public Complex getComplex() {
        return complex;
    }

    public void setComplex(Complex complex) {
        this.complex = complex;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValorationKey)) return false;

        ValorationKey that = (ValorationKey) o;

        return getComplex().equals(that.getComplex()) && getUser().equals(that.getUser());

    }

    @Override
    public int hashCode() {
        int result = getComplex().hashCode();
        result = 31 * result + getUser().hashCode();
        return result;
    }
}
