package ar.edu.itba.paw.model;


import ar.edu.itba.paw.model.DTOs.TimeTableDTO;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "schedule")
public class TimeTable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "schedule_id_seq")
  @SequenceGenerator(sequenceName = "schedule_id_seq", name = "schedule_id_seq", allocationSize = 1)
  private int id;

  @Column(name = "start", nullable = false)
  private int begin;

  @Column(name = "xend", nullable = false)
  private int end;

  @Column(name = "day", nullable = false)
  private int day;

  @Column(name = "price", nullable = false)
  private double price;

  @ManyToMany(mappedBy = "schedules")
  private List<Pitch> pitches;

  public TimeTable(int begin, int end, int day, double price) {
    super();
    this.begin = begin;
    this.end = end;
    this.day = day;
    this.price = price;
  }

  public TimeTable() {
  }

  public int getBegin() {
    return begin;
  }

  public void setBegin(int begin) {
    this.begin = begin;
  }

  public int getEnd() {
    return end;
  }

  public void setEnd(int end) {
    this.end = end;
  }

  public int getDay() {
    return day;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<Pitch> getPitches() {
    return pitches;
  }

  public void setPitches(List<Pitch> pitches) {
    this.pitches = pitches;
  }

  @Override
  public String toString() {
    return "{\"begin\":\"" + begin + "\", \"end\":\"" + end + "\", \"day\":\"" + day + "\", \"price\":\"" + price + "\"}";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    TimeTable timeTable = (TimeTable) o;

    if (begin != timeTable.begin) return false;
    if (end != timeTable.end) return false;
    if (day != timeTable.day) return false;
    return price == timeTable.price;

  }

    @Override
    public int hashCode() {
        int result = begin;
        result = 31 * result + end;
        result = 31 * result + day;
        result = 31 * result + (int) price;
        return result;
    }

  public TimeTableDTO toDTO() {
  // return new TimeTableDTO(id,begin,end,day,price);
    return null;
  }
}
