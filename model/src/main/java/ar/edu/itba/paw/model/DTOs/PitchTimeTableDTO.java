package ar.edu.itba.paw.model.DTOs;

/**
 * Created by tritoon on 30/11/16.
 */
public class PitchTimeTableDTO {

  private PitchDTO pitch;
  private TimeTableDTO[] timeTables;

  public PitchTimeTableDTO() {
    this.timeTables = new TimeTableDTO[7];
  }

  public PitchTimeTableDTO(PitchDTO pitch, TimeTableDTO[] timeTables) {
    this.pitch = pitch;
    this.timeTables = timeTables;
  }

  public void addSchedule(int day, TimeTableDTO ttdto){
    timeTables[day] = ttdto;
  }

  public PitchDTO getPitch() {
    return pitch;
  }

  public void setPitch(PitchDTO pitch) {
    this.pitch = pitch;
  }

  public TimeTableDTO[] getTimeTables() {
    return timeTables;
  }

  public void setTimeTables(TimeTableDTO[] timeTables) {
    this.timeTables = timeTables;
  }
}
