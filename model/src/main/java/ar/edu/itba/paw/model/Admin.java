package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.DTOs.AdminDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "admin")
public class Admin implements Serializable {

    @Id
    @OneToOne(optional = false)
    private User user;

    @ManyToMany(fetch = FetchType.EAGER)
    @ElementCollection(targetClass = Complex.class)
    private List<Complex> complexes;

    public Admin() {
    }

    public User getUser() {
        return user;
    }

    public List<Complex> getComplexes() {
        return complexes;
    }

    public Admin(User user, List<Complex> complexes) {
        this.user = user;
        this.complexes = complexes;
    }

    public AdminDTO toDTO() {
        List<Integer> complexesIds = new ArrayList<>();
        Complex complex;
        for(int i=0; i<complexes.size(); i++) {
            complex = complexes.get(i);
            complexesIds.add(complex.getId());
        }
        return new AdminDTO(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPhone(), user.getUsername(), complexesIds);
    }
}
