package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.DTOs.UserDTO;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "users")
public class User {

    @Column(name = "firstname", nullable = false, length = 100)
    private String firstName;

    @Column(name = "lastname", nullable = false, length = 100)
    private String lastName;

    @Column(name = "password", length = 100)
    private String password;

    @Id
    @Column(name = "email", nullable = false, length = 150)
    private String email;

    @Column(name = "phone", length = 30)
    private String phone;

    @Column(name = "username", unique = true, length = 50)
    private String username;

    @OneToMany
    private List<Reserve> bookings;

    @OneToMany
    private List<Valoration> valorations;

    @OneToMany
    private List<PendingValoration> pendingValorations;

    public User() {
    }

    public User(final String firstName, final String lastName, final String password, final String email,
                final String phone, final String username) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserDTO toDTO(){
        return new UserDTO(firstName,lastName,email,phone,username);
    }

}
