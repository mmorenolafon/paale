package ar.edu.itba.paw.model;


import ar.edu.itba.paw.model.DTOs.ScheduleExceptionDTO;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class ScheduleException implements Serializable {

    @EmbeddedId
    private ScheduleExceptionId scheduleExceptionId;


    public ScheduleException(Complex complex, LocalDate beginDate, LocalDate endDate) {
        this.scheduleExceptionId = new ScheduleExceptionId(complex, beginDate, endDate);
    }

    public ScheduleException() {
    }

    public Complex getComplex() {
        return this.scheduleExceptionId.getComplex();
    }

    public void setComplex(Complex complex) {
        this.scheduleExceptionId.setComplex(complex);
    }

    public LocalDate getBeginDate() {
        return this.scheduleExceptionId.getBeginDate();
    }

    public void setBeginDate(LocalDate beginDate) {
        this.scheduleExceptionId.setBeginDate(beginDate);
    }

    public LocalDate getEndDate() {
        return this.scheduleExceptionId.getEndDate();
    }

    public void setEndDate(LocalDate endDate) {
        this.scheduleExceptionId.setEndDate(endDate);
    }

    public ScheduleExceptionDTO toDTO() {
        return new ScheduleExceptionDTO(this.getBeginDate(), this.getEndDate());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScheduleException that = (ScheduleException) o;

        return scheduleExceptionId.equals(that.scheduleExceptionId);
    }

    @Override
    public int hashCode() {
        return scheduleExceptionId.hashCode();
    }

    @Override
    public String toString() {
        return "ScheduleException{" + scheduleExceptionId.toString() + '}';
    }
}
