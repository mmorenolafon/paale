package ar.edu.itba.paw.model.DTOs;

import java.util.List;

/**
 * Created by tritoon on 11/10/16.
 */
public class ReservesDTO {
    private List<ReserveDTO> reserves;
    private List<ReserveDTO> reserveHistory;
    private String reserveLimit;

    public ReservesDTO(List<ReserveDTO> reserves,
                       List<ReserveDTO> reserveHistory, String reserveLimit) {
        this.reserves = reserves;
        this.reserveHistory = reserveHistory;
        this.reserveLimit = reserveLimit;
    }

    public ReservesDTO() {
    }

    public List<ReserveDTO> getReserves() {
        return reserves;
    }

    public void setReserves(List<ReserveDTO> reserves) {
        this.reserves = reserves;
    }

    public List<ReserveDTO> getReserveHistory() {
        return reserveHistory;
    }

    public void setReserveHistory(List<ReserveDTO> reserveHistory) {
        this.reserveHistory = reserveHistory;
    }

    public String getReserveLimit() {
        return reserveLimit;
    }

    public void setReserveLimit(String reserveLimit) {
        this.reserveLimit = reserveLimit;
    }
}
