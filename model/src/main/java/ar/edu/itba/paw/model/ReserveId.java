package ar.edu.itba.paw.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
public class ReserveId implements Serializable {

    @ManyToOne
    private Pitch pitch;

    @Column(name = "start")
    private int start;

    @Column(name = "date")
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate date;

    public ReserveId(Pitch pitch, int start, LocalDate date) {
        this.pitch = pitch;
        this.start = start;
        this.date = date;
    }

    public ReserveId() {
    }

    public Pitch getPitch() {
        return pitch;
    }

    public void setPitch(Pitch pitch) {
        this.pitch = pitch;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
