package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.DTOs.ComplexDTO;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "complex")
public class Complex {
  private final static int TIME_TO_CLOSE = 14;

  @Column(name = "address", nullable = false, length = 100)
  private String address;

  @Column(name = "phone", nullable = false, length = 100)
  private String phone;

  @Column(name = "name", nullable = false, length = 100)
  private String name;

  @Column(name = "email", nullable = false, length = 100)
  private String email;

  @Column(name = "lastModified")
  @Convert(converter = LocalDateTimeAttributeConverter.class)
  private LocalDateTime lastModified;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "complex_id_seq")
  @SequenceGenerator(sequenceName = "complex_id_seq", name = "complex_id_seq", allocationSize = 1)
  @Column(name = "id", nullable = false)
  private int id;

  @ManyToOne
  private States state;

  @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, orphanRemoval = true)
  private List<ScheduleException> scheduleExceptions;

  @OneToMany(fetch = FetchType.EAGER)
  @ElementCollection(targetClass = Pitch.class)
  List<Pitch> pitches;

  public Complex() {
    pitches = new LinkedList<>();
    scheduleExceptions = new LinkedList<>();
  }

  public Complex(String address, String phone, String name, String email, States state) {
    super();
    this.state = state;
    this.address = address;
    this.phone = phone;
    this.name = name;
    this.email = email;
    this.lastModified = LocalDateTime.now();
    pitches = new LinkedList<>();
    scheduleExceptions = new LinkedList<>();
  }

  public List<Pitch> getPitches() {
    return pitches;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public int getStateid() {
    return state.getId();
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getState() {
    return this.state.getName();
  }

  public LocalDateTime getLastModified() {
    return lastModified;
  }

  public void setLastModified(LocalDateTime lastModified) {
    this.lastModified = lastModified;
  }

  public List<ScheduleException> getScheduleExceptions() {
    return scheduleExceptions;
  }

  public void setScheduleExceptions(List<ScheduleException> scheduleExceptions) {
    this.scheduleExceptions = scheduleExceptions;
  }

  public boolean addScheduleException(LocalDate beginDate, LocalDate endDate) {
    LocalDate minDate = LocalDate.now().plus(14, ChronoUnit.DAYS);
    if (beginDate.isBefore(minDate) || beginDate.isAfter(endDate)) {
      return false;
    }
    ScheduleException newScheduleException = new ScheduleException(this, beginDate, endDate);
    if (!scheduleExceptions.contains(newScheduleException)) {
      return scheduleExceptions.add(newScheduleException);
    }
    return false;
  }


  public void deleteScheduleException(LocalDate beginDate, LocalDate endDate) {
    if (scheduleExceptions.isEmpty()) {
      return;
    }
    ScheduleException current;
    Iterator<ScheduleException> it = scheduleExceptions.iterator();
    while (it.hasNext()) {
      current = it.next();
      if (current.getComplex().getId() == this.id && current.getBeginDate().equals(beginDate) && current.getEndDate().equals(endDate)) {
        scheduleExceptions.remove(current);
        return;
      }
    }
  }

  @Override
  public String toString() {
    return "{\"phone\":\"" + phone + "\", \"name\":\"" + name + "\", \"email\":\""
      + email + "\", \"id\":\"" + id + "\", \"state\":" + state + ", \"address\":\"" + address + "\"}";
  }

  public void addPitch(Pitch pitch) {
    this.pitches.add(pitch);
  }

  public ComplexDTO toDTO() {
    return new ComplexDTO(this.id, this.name, this.email, this.address, this.phone);
  }

}
