package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.DTOs.ReserveDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "reserve")
public class Reserve implements Serializable {

    @Column(name = "paid", nullable = false)
    private double paid;

    @Column(name = "xend", nullable = false)
    private int end;

    @Column(name = "price", nullable = false)
    private double price;

    @ManyToOne
    private User user;

    @Column(name = "bookerUsername", nullable=false)
    private String bookerUsername;

    @EmbeddedId
    private ReserveId id;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDate(LocalDate date) {
        this.id.setDate(date);
    }


    @Override
    public String toString() {
        return "Reserve [complexId=" + id.getPitch().getId().getComplexId() + ", pitchId=" + id.getPitch().getId().getPitchIdByComplex()
                + ", begin=" + id.getStart() + ", end=" + end + ", day=" + id.getDate().getDayOfWeek().getValue() + "]";
    }

    public int getComplexId() {
        return this.id.getPitch().getId().getComplexId();
    }

    public int getPitchId() {
        return id.getPitch().getId().getPitchIdByComplex();
    }

    public void setPitchId(int pitchId) {
        this.id.getPitch().getId().setPitchIdByComplex(pitchId);
    }

    public int getBegin() {
        return id.getStart();
    }

    public void setBegin(int begin) {
        id.setStart(begin);
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public LocalDate getDate() {
        return this.id.getDate();
    }

    public int getDayOfWeek() {
        return (this.id.getDate().getDayOfWeek().getValue() - 1) % 7;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ReserveId getId() {
        return id;
    }

    public void setId(ReserveId id) {
        this.id = id;
    }

    public Reserve() {
        this.id = new ReserveId();
    }

    public Pitch getPitch() {
        return this.id.getPitch();
    }

    public void setPitch(Pitch pitch) {
        this.id.setPitch(pitch);
    }

    public Reserve(Pitch pitch, int begin, int end, LocalDate date, User user, String bookerUsername,
                   double price, double paid) {
        super();
        this.user = user;
        this.bookerUsername = bookerUsername;
        this.end = end;
        this.price = price;
        this.paid = paid;
        this.id = new ReserveId(pitch, begin, date);
    }

    public Complex getComplex() {
        return this.id.getPitch().getComplex();
    }

    public void setComplex(Complex complex) {
        this.id.getPitch().getId().setComplex(complex);
    }

    public User getUserEmail() {
        return user;
    }

    public String getStringDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return id.getDate().format(dtf);
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public int getStart() {
        return id.getStart();
    }

    public void setStart(int start) {
        this.id.setStart(start);
    }

    public String getBookerUsername() {
        return bookerUsername;
    }

    public void setBookerUsername(String bookerUsername) {
        this.bookerUsername = bookerUsername;
    }

    public ReserveDTO toDTO() {
        return new ReserveDTO(getComplex().getName(), getPitch().getSize(), getBegin(), getStringDate(),
                getUserEmail().getEmail(), price, paid, getPitchId(), getComplexId(), getDate(), getBookerUsername());
    }
}
