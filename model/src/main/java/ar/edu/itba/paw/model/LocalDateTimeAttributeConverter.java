package ar.edu.itba.paw.model;


import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Date> {


    @Temporal(TemporalType.TIMESTAMP)
    @Override
    public Date convertToDatabaseColumn(LocalDateTime locDate) {
        ZonedDateTime zdt = locDate.atZone(ZoneId.systemDefault());

        return (locDate == null ? null :  Date.from(zdt.toInstant()));
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    public LocalDateTime convertToEntityAttribute(Date sqlDate) {
        return (sqlDate == null ? null : LocalDateTime.ofInstant(sqlDate.toInstant(),ZoneId.systemDefault()));
    }
}

