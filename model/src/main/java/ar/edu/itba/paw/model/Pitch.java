package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.DTOs.PitchDTO;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "pitch")
public class Pitch {

    @Column(name = "capacity", nullable = false)
    private int size;

    @Column(name = "floor", nullable = false)
    private int floor;

    public List<Reserve> getBookings() {
        return bookings;
    }

    public void setBookings(List<Reserve> bookings) {
        this.bookings = bookings;
    }

    @EmbeddedId
    private PitchId id;

    @ManyToMany
    private List<TimeTable> schedules;

    @OneToMany
    private List<Reserve> bookings;

    public List<TimeTable> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<TimeTable> schedules) {
        this.schedules = schedules;
    }

    public Complex getComplex() {
        return id.getComplex();
    }


    public Pitch(int size, int floor, int id, Complex complex) {
        super();
        this.size = size;
        this.floor = floor;
        this.schedules = new LinkedList<>();
        this.id = new PitchId(complex, id);
    }

    public Pitch() {
        this.schedules = new LinkedList<>();
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "{\"id\":\"" + id + "\", \"floorId\":\"" + floor + "\", \"size\":\"" + size + "\", \"floor\":\"" + Floor.getById(floor).toString() + "\"}";
    }

    public PitchId getId() {
        return id;
    }

    public void setId(PitchId id) {
        this.id = id;
    }

    public Floor getFloorName() {
        return Floor.getById(floor);
    }

    public double getPrice(int start, int day) {
        for (TimeTable x : this.schedules) {
            if (x.getBegin() <= start && x.getEnd() > start && x.getDay() == day) {
                return x.getPrice();
            }
        }
        return -1;
    }

  public PitchDTO toDTO() {
    return new PitchDTO(id.getPitchIdByComplex(),size,Floor.getById(floor).getDisplayName());
  }
}
