package ar.edu.itba.paw.model.DTOs;

import java.time.LocalDate;

public class ScheduleExceptionDTO {
    private LocalDate beginDate;
    private LocalDate endDate;

    public ScheduleExceptionDTO() {
    }

    public ScheduleExceptionDTO(LocalDate beginDate, LocalDate endDate) {
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
