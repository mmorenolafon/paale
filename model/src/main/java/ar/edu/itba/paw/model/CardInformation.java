package ar.edu.itba.paw.model;

public class CardInformation {
  private AvailablePitch availablePitch;
  private int complexId;
  private String address;
  private String phone;
  private String email;
  private String name;
  private int stateId;
  private String stateName;
  private int floorId;
  private String floor;
  private double price;

  public CardInformation(AvailablePitch availablePitch, int complexId, String address, String phone, String email, String name, int stateId, String stateName, int floorId, String floor, double price) {
    this.availablePitch = availablePitch;
    this.complexId = complexId;
    this.address = address;
    this.phone = phone;
    this.email = email;
    this.name = name;
    this.stateId = stateId;
    this.stateName = stateName;
    this.floorId = floorId;
    this.floor = floor;
    this.price = price;
  }

  public CardInformation() {
  }

  public AvailablePitch getAvailablePitch() {
    return availablePitch;
  }

  public void setAvailablePitch(AvailablePitch availablePitch) {
    this.availablePitch = availablePitch;
  }

  public int getComplexId() {
    return complexId;
  }

  public void setComplexId(int complexId) {
    this.complexId = complexId;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStateId() {
    return stateId;
  }

  public void setStateId(int stateId) {
    this.stateId = stateId;
  }

  public String getStateName() {
    return stateName;
  }

  public void setStateName(String stateName) {
    this.stateName = stateName;
  }

  public int getFloorId() {
    return floorId;
  }

  public void setFloorId(int floorId) {
    this.floorId = floorId;
  }

  public String getFloor() {
    return floor;
  }

  public void setFloor(String floor) {
    this.floor = floor;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String toString() {
    return "{\"phone\":\"" + phone + "\", \"name\":\"" + name + "\", \"email\":\""
      + email + "\", \"complexId\":\"" + complexId + "\", \"state\":\"" + stateName + "\", \"address\":\"" + address + "\", \"availablePitch\":" + availablePitch.toString() + "}";
  }
}
