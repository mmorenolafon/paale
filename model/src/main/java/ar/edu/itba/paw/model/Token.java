package ar.edu.itba.paw.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "tokens")
public class Token {

    @Id
    @Column(name = "user_email", nullable = false, length = 150)
    private String userEmail;

    @Column(name = "last_modified")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime lastModified;

    @Column(name = "token", unique = true, length = 100)
    private String token;

    public Token() {
    }

    public Token(String userEmail, LocalDateTime lastModified, String token) {
        this.userEmail = userEmail;
        this.lastModified = lastModified;
        this.token = token;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
