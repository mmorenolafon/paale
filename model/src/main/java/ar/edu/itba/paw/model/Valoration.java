package ar.edu.itba.paw.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


@Entity
@Table(name = "valorations")
public class Valoration implements Serializable {

    @Column(name = "valoration", nullable = false)
    private int valoration;

    @Column(name = "date", nullable = false)
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate date;

    @EmbeddedId
    private ValorationKey key;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getValoration() {
        return valoration;
    }

    public void setValoration(int valoration) {
        this.valoration = valoration;
    }

    public User getUser() {
        return key.getUser();
    }

    public void setUser(User user) {
        key.setUser(user);
    }

    public Complex getComplex() {
        return key.getComplex();
    }

    public void setComplex(Complex complex) {
        key.setComplex(complex);
    }

    public Valoration() {
        this.key = new ValorationKey();
    }

    public Valoration(User user, Complex complex, LocalDate date, int valoration) {
        this.key = new ValorationKey();
        this.key.setUser(user);
        this.key.setComplex(complex);
        this.date = date;
        this.valoration = valoration;
    }

    public String getStringDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return this.date.format(dtf);
    }

}
