package ar.edu.itba.paw.model.DTOs;

import java.util.List;

/**
 * Created by tritoon on 30/11/16.
 */
public class PitchSetTimeTableDTO {

  private List<Integer> pitchIds;
  private List<TimeTableDTO> timeTables;
  private Integer complexId;

  public PitchSetTimeTableDTO() {
  }

  public PitchSetTimeTableDTO(List<Integer> pitchIds, List<TimeTableDTO> timeTables, Integer complexId) {
    this.pitchIds = pitchIds;
    this.timeTables = timeTables;
    this.complexId = complexId;
  }

  public List<Integer> getPitchIds() {
    return pitchIds;
  }

  public void setPitchIds(List<Integer> pitchIds) {
    this.pitchIds = pitchIds;
  }

  public List<TimeTableDTO> getTimeTables() {
    return timeTables;
  }

  public void setTimeTables(List<TimeTableDTO> timeTables) {
    this.timeTables = timeTables;
  }

  public Integer getComplexId() {
    return complexId;
  }

  public void setComplexId(Integer complexId) {
    this.complexId = complexId;
  }
}
