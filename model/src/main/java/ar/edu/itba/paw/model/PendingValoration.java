package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.DTOs.ValorationDTO;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "pending_valorations")
public class PendingValoration {

    @EmbeddedId
    private PendingValorationKey key;

    public PendingValoration(User user, Complex complex, int time, LocalDate date) {
        this.key = new PendingValorationKey();
        this.key.setComplex(complex);
        this.key.setUser(user);
        this.key.setDate(date);
        this.key.setTime(time);
    }

    public PendingValoration() {
        this.key = new PendingValorationKey();
    }

    public User getUser() {
        return key.getUser();
    }

    public void setUser(User user) {
        this.key.setUser(user);
    }

    public Complex getComplex() {
        return this.key.getComplex();
    }

    public void setComplex(Complex complex) {
        this.setComplex(complex);
    }

    public LocalDate getDate() {
        return key.getDate();
    }

    public String getStringDate() {
        return getDate().toString();
    }

    public void setDate(LocalDate date) {
        this.setDate(date);
    }

    public int getTime() {
        return this.key.getTime();
    }

    public void setTime(int time) {
        this.key.setTime(time);
    }

  public PendingValorationKey getKey() {
    return key;
  }

  public void setKey(PendingValorationKey key) {
    this.key = key;
  }

  public ValorationDTO toDTO() {
    return new ValorationDTO(getComplex().getId(),getComplex().getName(),getDate());
  }
}
