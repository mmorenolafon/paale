package ar.edu.itba.paw.model;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "AvailablePitchResult", classes = {
        @ConstructorResult(
                targetClass = AvailablePitch.class,
                columns = {
                        @ColumnResult(name = "floor", type = Integer.class),
                        @ColumnResult(name = "size", type = Integer.class),
                        @ColumnResult(name = "pitchId", type = Integer.class),
                        @ColumnResult(name = "startTime", type = Integer.class),
                        @ColumnResult(name = "price", type = Double.class),
                }
        )
})
public class AvailablePitch {

    private int floor;
    private int size;
    private int pitchId;
    private int startTime;
    private int endTime;
    private double price;
    private String floorName;

    public int getComplexId() {
        return complexId;
    }

    public void setComplexId(int complexId) {
        this.complexId = complexId;
    }

    private int complexId;

    public AvailablePitch(int pitchId, int floor, int size, int startTime, double price, int complexId) {
        this.pitchId = pitchId;
        this.size = size;
        this.floor = floor;
        this.startTime = startTime;
        this.endTime = startTime + 1;
        this.price = price;
        this.complexId = complexId;
    }

    public AvailablePitch() {
    }

    public String getFloorName() {
        return floorName;
    }

    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
        this.floorName = Floor.getById(floor).getDisplayName();
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPitchId() {
        return pitchId;
    }

    public void setPitchId(int pitchId) {
        this.pitchId = pitchId;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


}
