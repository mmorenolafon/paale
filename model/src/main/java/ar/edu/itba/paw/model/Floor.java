package ar.edu.itba.paw.model;

import ar.edu.itba.paw.model.DTOs.FloorDTO;

import java.util.*;

public enum Floor {
  Grass("Pasto", "Grass"), SyntheticGrass("Pasto sintético", "SyntheticGrass"), Cement("Cemento", "Cement"), Parquet("Parquet", "Parquet");
  private static Map<Integer, Floor> map;
  private static Set<FloorDTO> floorDTOs;

  private static boolean init = false;
  private String displayName;
  private String dbname;


  public static Collection<FloorDTO> getFloorsDTO() {
    if (!init) {
      initialize();
      init = true;
    }

    return floorDTOs;
  }

  public static Collection<Floor> getFloors() {
    if (!init) {
      initialize();
      init = true;
    }

    return map.values();
  }

  private Floor(String displayName, String dbname) {
    this.displayName = displayName;
    this.dbname = dbname;
  }

  public static Floor getById(int id) {
    if (!init) {
      initialize();
      init = true;
    }

    if (map.containsKey(id)) {
      return map.get(id);
    } else {
      return null;
    }
  }

  public static Integer getId(Floor floor) {
    if (!init) {
      initialize();
      init = true;
    }
    for (Integer x : map.keySet()) {
      if (map.get(x).equals(floor)) {
        return x;
      }
    }
    return null;
  }

  public String getDisplayName() {
    return this.displayName;
  }

  private static void initialize() {
    map = new HashMap<Integer, Floor>();
    map.put(1, Floor.Grass);
    map.put(2, Floor.SyntheticGrass);
    map.put(3, Floor.Cement);
    map.put(4, Floor.Parquet);

    floorDTOs = new HashSet<>();

    for (Map.Entry<Integer,Floor> e : map.entrySet()) {
          floorDTOs.add(new FloorDTO(e.getKey(),e.getValue().toString()));
    }
  }

  @Override
  public String toString() {
    return this.displayName;
  }

  public String toDBString() {
    return this.dbname;
  }
}
