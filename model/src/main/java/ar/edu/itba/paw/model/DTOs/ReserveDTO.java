package ar.edu.itba.paw.model.DTOs;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ReserveDTO {
  public int getPitchId() {
    return pitchId;
  }

  public void setPitchId(int pitchId) {
    this.pitchId = pitchId;
  }

  private int pitchId;
  private int complexId;
  private String complexName;
  private int pitchSize;
  private int begin;
  private String stringDate;
  private String date;
  private double price;
  private double paid;
  private String email;
  private String bookerUsername;

  @Override
  public String toString() {
    return "{\"complexId\":\"" + complexId + "\", \"stringDate\":\"" + stringDate + "\", \"start\":\"" + begin + "\", \"email\":\"" + email + "\" , \"paid\":\"" + paid + "\",\"pitchId\":\"" + pitchId + "\"}";
  }

  public ReserveDTO() {
  }

  public ReserveDTO(String name, int size, int begin, String stringDate, String email, double price, double paid, int pitchId, int complexId, LocalDate date, String bookerUsername) {
    this.complexName = name;
    this.pitchSize = size;
    this.begin = begin;
    this.stringDate = stringDate;
    this.email = email;
    this.price = price;
    this.paid = paid;
    this.pitchId = pitchId;
    this.complexId = complexId;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    this.date = date.format(dtf);
    this.bookerUsername = bookerUsername;
  }

  public int getComplexId() {
    return complexId;
  }

  public void setComplexId(int complexId) {
    this.complexId = complexId;
  }

  public String getComplexName() {
    return complexName;
  }

  public void setComplexName(String complexName) {
    this.complexName = complexName;
  }

  public int getPitchSize() {
    return pitchSize;
  }

  public void setPitchSize(int pitchSize) {
    this.pitchSize = pitchSize;
  }

  public int getBegin() {
    return begin;
  }

  public void setBegin(int begin) {
    this.begin = begin;
  }

  public String getStringDate() {
    return stringDate;
  }

  public void setStringDate(String stringDate) {
    this.stringDate = stringDate;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getPaid() {
    return paid;
  }

  public void setPaid(double paid) {
    this.paid = paid;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getBookerUsername() {
    return bookerUsername;
  }

  public void setBookerUsername(String bookerUsername) {
    this.bookerUsername = bookerUsername;
  }
}
