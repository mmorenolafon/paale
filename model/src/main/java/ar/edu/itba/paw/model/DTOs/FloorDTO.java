package ar.edu.itba.paw.model.DTOs;

/**
 * Created by tritoon on 26/11/16.
 */
public class FloorDTO {

  private int id;
  private String displayName;

  public FloorDTO() {
  }

  public FloorDTO(int id, String displayName) {
    this.id = id;
    this.displayName = displayName;
  }

  public int getId() {
    return id;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
}
