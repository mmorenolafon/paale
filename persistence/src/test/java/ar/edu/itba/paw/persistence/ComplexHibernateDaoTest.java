package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.ComplexDao;
import ar.edu.itba.paw.interfaces.PitchDao;
import ar.edu.itba.paw.interfaces.StatesDao;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.ScheduleException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ComplexHibernateDaoTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(ComplexHibernateDaoTest.class);

  @Autowired private ComplexDao complexDao;
  @Autowired private StatesDao statesDao;
  @Autowired private PitchDao pitchDao;

  private TestDataBasePopulator populator;


  @BeforeClass
  public static void beforeClass() {
    LOGGER.debug("Initializing ComplexHibernateDao class test...");
  }

  @Before
  public void setUp() {
    populator = new TestDataBasePopulator(this.complexDao, this.pitchDao, this.statesDao);
    populator.populate();
  }

  @Test
  @Transactional
  public void testGetComplex() {
    List<Complex> result = complexDao.getComplex();

    assertNotNull(result);
    assertEquals(6, result.size());
    assertEquals("Dummy", result.get(0).getName());
    assertEquals("Vamo a calmarno", result.get(1).getName());
    assertEquals("Bien jugado", result.get(4).getName());
    assertEquals("RFC", result.get(5).getName());
  }

  @Test
  @Transactional
  public void testGetComplexById() {
    List<Complex> complexes = complexDao.getComplex();
    Integer requiredId = complexes.get(0).getId();
    Complex result = complexDao.getById(requiredId);
    assertNotNull(result);
    assertEquals("Dummy", result.getName());
    assertEquals(complexes.get(0).getName(), result.getName());

    requiredId = complexes.get(2).getId();
    result = complexDao.getById(requiredId);
    assertNotNull(result);
    assertEquals("La vida no es justa", result.getName());
    assertEquals(complexes.get(2).getName(), result.getName());

  }


  @Test
  public void testAddScheduleException() {

    boolean result = complexDao.addScheduleException(1, LocalDate.now(), LocalDate.now());
    assertFalse(result);

    LocalDate beginDate, endDate;

    beginDate = LocalDate.now();
    beginDate.plus(17, ChronoUnit.DAYS);
    endDate = LocalDate.now();
    endDate.plus(20, ChronoUnit.DAYS);
    result = complexDao.addScheduleException(6, beginDate, endDate);
    assertFalse("Must be false because the interval is between an exist one", result);

    int complexId = complexDao.getComplex().get(0).getId();
    beginDate = LocalDate.now();
    beginDate.plus(17, ChronoUnit.DAYS);
    endDate = LocalDate.now();
    endDate.plus(20, ChronoUnit.DAYS);
    result = complexDao.addScheduleException(complexId, beginDate, endDate);
    assertFalse("Must be false because the interval already exists", result);

    beginDate = LocalDate.now().plus(16, ChronoUnit.DAYS);
    endDate = LocalDate.now().plus(18, ChronoUnit.DAYS);
    assertTrue(complexDao.addScheduleException(complexId, beginDate, endDate));

    beginDate = LocalDate.now().plus(17, ChronoUnit.DAYS);
    endDate = LocalDate.now().plus(16, ChronoUnit.DAYS);
    assertFalse(complexDao.addScheduleException(complexId, beginDate, endDate));

    beginDate = LocalDate.now().plus(8, ChronoUnit.DAYS);
    assertFalse(complexDao.addScheduleException(complexId, beginDate, endDate));
  }


  @Test
  @Transactional
  public void testGetScheduleExceptions() {
    List<Complex> complexes = complexDao.getComplex();
    int complexId = complexes.get(0).getId();
    List<ScheduleException> result = complexDao.getScheduleExceptions(complexId);
    assertNotNull(result);
    assertEquals(0, result.size());

    LocalDate beginDate = LocalDate.now().plus(16, ChronoUnit.DAYS);
    LocalDate endDate = LocalDate.now().plus(18, ChronoUnit.DAYS);
    complexDao.addScheduleException(complexId, beginDate, endDate);
    complexDao.addScheduleException(complexId, beginDate, endDate);
    result = complexDao.getScheduleExceptions(complexId);
    assertNotNull(result);
    assertEquals(1, result.size());

    complexDao.addScheduleException(complexId, beginDate, endDate);
    complexDao.addScheduleException(complexId, beginDate.plus(4, ChronoUnit.DAYS), endDate.plus(4, ChronoUnit.DAYS));
    assertEquals(2, result.size());
  }

  @Test
  @Transactional
  public void testDeleteScheduleException() {
    LocalDate beginDate = LocalDate.now();
    beginDate.plus(15, ChronoUnit.DAYS);
    LocalDate endDate = LocalDate.now();
    endDate.plus(15, ChronoUnit.DAYS);
    complexDao.deleteScheduleException(4, beginDate, endDate);

    List<ScheduleException> result = complexDao.getScheduleExceptions(4);
/*
    assertEquals(beginDate.getDayOfMonth(), result.get(0).getBeginDate().getDayOfMonth());
    assertEquals(endDate.getDayOfMonth(), result.get(0).getEndDate().getDayOfMonth());

    assertEquals(beginDate, result.get(0).getBeginDate());
    assertEquals(endDate, result.get(0).getEndDate());
*/

    assertEquals(0, result.size());
  }

  @Test
  @Transactional
  public void testModifyComplexFields() {
    boolean result = complexDao.modifyComplexFields(2, "87654321", "mail@gmail.com");
    Complex complex = complexDao.getComplex().get(1);

    assertTrue(result);
    assertEquals("87654321", complex.getPhone());
    assertEquals("mail@gmail.com", complex.getEmail());
  }

  @Test
  @Transactional
  public void testAddComplex() {
    Complex complex = new Complex("San Pedro 999", "11111111", "Super complejo", "mmorenolafon@itba.edu.ar",
      statesDao.getStatesById(3));
    Complex result = complexDao.addComplex(complex);
    assertNotNull(result);

    Complex complexAdded = complexDao.getById(complex.getId());
    assertSame(complex, complexAdded);
  }

  @AfterClass
  public static void afterClass() {
    LOGGER.debug("Finishing ComplexHibernateDao class test...");
  }
}
