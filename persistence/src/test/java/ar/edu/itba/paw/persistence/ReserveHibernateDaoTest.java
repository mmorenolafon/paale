package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.*;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.Pitch;
import ar.edu.itba.paw.model.Reserve;
import ar.edu.itba.paw.model.User;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
public class ReserveHibernateDaoTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(ComplexHibernateDaoTest.class);

  @Autowired private UserDao userDao;
  @Autowired private ComplexDao complexDao;
  @Autowired private StatesDao statesDao;
  @Autowired private PitchDao pitchDao;
  @Autowired private ReserveDao reserveDao;

  private TestDataBasePopulator populator;

  private User user = userDao.create("Marco", "Boschetti", "44332211", "asd1234", "Tritoon", "mboschetti@itba.edu.ar");
  private Complex complex = complexDao.getById(1);
  private Pitch pitch = complex.getPitches().get(1);
  private int startTime = 8, endTime = 9;
  private LocalDate date = LocalDate.now().plus(8, ChronoUnit.DAYS);
  private String bookerUsername = "Milagros";


  @BeforeClass
  public static void beforeClass() {
    LOGGER.debug("Initializing ReserveHibernateDao class test...");
  }

  @Before
  public void setUp() {
    populator = new TestDataBasePopulator(this.complexDao, this.pitchDao, this.statesDao);
    populator.populate();
  }

  @Test
  @Transactional
  public void testGetUserReserves() {
    reserveDao.addReserve(pitch, startTime, endTime, date, user, bookerUsername);
    List<Reserve> reserves = reserveDao.getUserReserves(user);

    boolean result = false;
    for (Reserve reserve : reserves) {
      if ( (reserve.getBegin() == startTime) && (reserve.getEnd() == endTime)
        && (reserve.getPitch().equals(pitch)) && (reserve.getDate().equals(date))
        && (reserve.getBookerUsername().equals(bookerUsername)) ) {
        result = true;
      }
    }
    assertTrue(result);
  }

  @Test
  @Transactional
  public void testAddReserve() {
    assertTrue(reserveDao.addReserve(pitch, startTime, endTime, date, user, bookerUsername));
    assertFalse(reserveDao.addReserve(pitch, startTime, endTime, date, user, bookerUsername));
  }

  @Test
  @Transactional
  public void testRemoveBooking() {
    reserveDao.addReserve(pitch, startTime, endTime, date, user, bookerUsername);

    assertTrue(reserveDao.removeBooking(pitch, startTime, date, user));
    assertFalse(reserveDao.removeBooking(pitch, startTime, date, user));
    assertFalse(reserveDao.removeBooking(pitch, startTime, date, user));
    assertFalse(reserveDao.removeBooking(pitch, startTime+1, date, user));
  }

  @Test
  @Transactional
  public void testGetBookingsByComplex() {
    List<Pitch> pitches = complex.getPitches();

    for (Pitch currentPitch : pitches) {
      reserveDao.addReserve(currentPitch, startTime, endTime, date, user, bookerUsername);
    }
    assertEquals(pitches.size(), reserveDao.getBookingsByComplex(complex, user.getUsername(), date, startTime, endTime).size());
  }


  @AfterClass
  public static void afterClass() {
    LOGGER.debug("Finishing ReserveHibernateDao class test...");
  }
}
