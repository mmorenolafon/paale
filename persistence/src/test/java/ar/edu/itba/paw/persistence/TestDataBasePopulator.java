package ar.edu.itba.paw.persistence;


import ar.edu.itba.paw.interfaces.*;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.Pitch;
import ar.edu.itba.paw.model.TimeTable;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

/**
 * * The {@code TestDataBasePopulator} populates the in
 * memory database used for tests.
 */
public class TestDataBasePopulator implements Populator {

  private ComplexDao complexDao;
  private PitchDao pitchDao;
  private StatesDao statesDao;


  public TestDataBasePopulator(ComplexDao complexDao, PitchDao pitchDao, StatesDao statesDao) {
    this.complexDao = complexDao;
    this.pitchDao = pitchDao;
    this.statesDao = statesDao;
  }

  @Override
  public void populate() {
    Complex complex;
    Pitch pitch;

    LocalDate beginDate = LocalDate.now(), endDate;
    beginDate.plus(15, ChronoUnit.DAYS);
    complex = complexDao.addComplex(new Complex("Av. Rivadavia 4500", "114567236", "Dummy", "jlepore@itba.edu.ar",
      statesDao.getStatesById(3)));
    pitch = new Pitch(5, 1, 1, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    pitch = new Pitch(5, 1, 2, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    addSchedule(complex, 8, 22, 0, 6, 800.0);
    endDate = LocalDate.now();
    endDate.plus(16, ChronoUnit.DAYS);
    complexDao.addScheduleException(complex.getId(), beginDate, endDate);

    complex = complexDao.addComplex(new Complex("Av. Corrientes 2400", "114567236", "Vamo a calmarno",
      "mmorenolafon@itba.edu.ar", statesDao.getStatesById(3)));
    pitch = new Pitch(5, 1, 1, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    pitch = new Pitch(5, 1, 2, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    addSchedule(complex, 8, 22, 0, 6, 900.0);
    endDate = LocalDate.now();
    endDate.plus(20, ChronoUnit.DAYS);
    complexDao.addScheduleException(complex.getId(), beginDate, endDate);

    complex = complexDao.addComplex(new Complex("Av. Santa Fe 400", "114567236", "La vida no es justa",
      "mmorenolafon@itba.edu.ar", statesDao.getStatesById(3)));
    pitch = new Pitch(5, 1, 1, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    pitch = new Pitch(5, 1, 2, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    addSchedule(complex, 8, 22, 0, 6, 850.0);

    complex = complexDao.addComplex(new Complex("Av. Córdoba 450", "114567236", "GROSO", "gmogni@itba.edu.ar",
      statesDao.getStatesById(2)));
    pitch = new Pitch(5, 1, 1, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    pitch = new Pitch(5, 1, 2, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    addSchedule(complex, 8, 22, 0, 6, 700.0);
    endDate = LocalDate.now();
    endDate.plus(15, ChronoUnit.DAYS);
    complexDao.addScheduleException(complex.getId(), beginDate, endDate);

    complex = complexDao.addComplex(new Complex("Av. Libertador 500", "114567236", "Bien jugado",
      "mboschetti@itba.edu.ar", statesDao.getStatesById(1)));
    pitch = new Pitch(5, 1, 1, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    pitch = new Pitch(5, 1, 2, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    addSchedule(complex, 8, 22, 0, 6, 1100.0);

    complex = complexDao.addComplex(new Complex("Av. 9 de Julio 10", "114567236", "RFC", "jlepore@itba.edu.ar",
      statesDao.getStatesById(5)));
    pitch = new Pitch(5, 1, 1, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    pitch = new Pitch(5, 1, 2, complex);
    pitchDao.addPitch(pitch);
    complex.addPitch(pitch);
    addSchedule(complex, 8, 22, 0, 6, 950.0);
    endDate = LocalDate.now();
    endDate.plus(25, ChronoUnit.DAYS);
    complexDao.addScheduleException(complex.getId(), beginDate, endDate);
  }


  public void addSchedule(Complex complex, int startTime, int endTime, int startDay, int endDay, double price) {
    TimeTable schedule;
    for (Pitch pitch: complex.getPitches()){
      for(int i = startDay; i <= endDay; i++){
        schedule = new TimeTable(startTime, endTime, i, price);
        pitchDao.addSchedule(schedule);
        pitch.getSchedules().add(schedule);
      }
    }
  }

  public void addLepoPitches() {
    Complex test = complexDao.getById(5);
    for (Pitch pitch: test.getPitches()) {
      for (TimeTable tt : pitch.getSchedules()) {
        tt.getPitches().add(pitch);
      }
    }
  }

}
