package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.ComplexDao;
import ar.edu.itba.paw.interfaces.PitchDao;
import ar.edu.itba.paw.interfaces.StatesDao;
import ar.edu.itba.paw.interfaces.UserDao;
import ar.edu.itba.paw.model.User;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
public class UserHibernateDaoTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComplexHibernateDaoTest.class);

    @Autowired private ComplexDao complexDao;
    @Autowired private StatesDao statesDao;
    @Autowired private PitchDao pitchDao;
    @Autowired private UserDao userDao;

    private TestDataBasePopulator populator;

    /* Entry parameters */
    private static final String CREATE_FIRSTNAME = "Marco";
    private static final String CREATE_LASTNAME = "Boschetti";
    private static final String CREATE_PHONE = "44332211";
    private static final String CREATE_PASSWORD = "asd1234";
    private static final String CREATE_USERNAME = "Tritoon";
    private static final String CREATE_EMAIL = "mboschetti@itba.edu.ar";

    private static final String TEST_FIRSTNAME = "Guido";
    private static final String TEST_LASTNAME = "Mogni";
    private static final String TEST_PHONE = "43434433";
    private static final String TEST_PASSWORD = "asd1234";
    private static final String TEST_USERNAME = "gibarsin";
    private static final String TEST_EMAIL = "gmogni@itba.edu.ar";

    @BeforeClass
    public static void beforeClass() {
        LOGGER.debug("Initializing UserHibernateDao class test...");
    }

    @Before
    public void setUp() {
        populator = new TestDataBasePopulator(this.complexDao, this.pitchDao, this.statesDao);
        populator.populate();
    }

    @Test
    public void testGetByUsername() {
        User ans = userDao.getByUsername(TEST_USERNAME);
        assertNotNull(ans);
        assertEquals(ans.getUsername(), TEST_USERNAME);
        assertEquals(ans.getFirstName(), TEST_FIRSTNAME);
        assertEquals(ans.getLastName(), TEST_LASTNAME);
        assertEquals(ans.getPassword(), TEST_PASSWORD);
        assertEquals(ans.getEmail(), TEST_EMAIL);
        assertEquals(ans.getPhone(), TEST_PHONE);
    }

    @Test
    public void testGetByEmail() {
        User ans = userDao.getByEmail(TEST_EMAIL);
        assertNotNull(ans);
        assertEquals(ans.getUsername(), TEST_USERNAME);
        assertEquals(ans.getFirstName(), TEST_FIRSTNAME);
        assertEquals(ans.getLastName(), TEST_LASTNAME);
        assertEquals(ans.getPassword(), TEST_PASSWORD);
        assertEquals(ans.getEmail(), TEST_EMAIL);
        assertEquals(ans.getPhone(), TEST_PHONE);
    }


    @Test
    public void testCreate() {
        userDao.create(CREATE_FIRSTNAME, CREATE_LASTNAME, CREATE_PASSWORD, CREATE_EMAIL, CREATE_PHONE, CREATE_USERNAME);
        User ans = userDao.getByUsername(CREATE_USERNAME);
        assertNotNull(ans);
        assertEquals(ans.getUsername(), CREATE_USERNAME);
        assertEquals(ans.getFirstName(), CREATE_FIRSTNAME);
        assertEquals(ans.getLastName(), CREATE_LASTNAME);
        assertEquals(ans.getPassword(), CREATE_PASSWORD);
        assertEquals(ans.getEmail(), CREATE_EMAIL);
        assertEquals(ans.getPhone(), CREATE_PHONE);
    }


    @AfterClass
    public static void afterClass() {
        LOGGER.debug("Finishing UserHibernateDao class test...");
    }
}
