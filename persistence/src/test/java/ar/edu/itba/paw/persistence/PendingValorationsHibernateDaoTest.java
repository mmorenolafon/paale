package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.*;
import ar.edu.itba.paw.model.PendingValoration;
import ar.edu.itba.paw.model.User;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
public class PendingValorationsHibernateDaoTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(ComplexHibernateDaoTest.class);

  @Autowired private ComplexDao complexDao;
  @Autowired private StatesDao statesDao;
  @Autowired private PitchDao pitchDao;
  @Autowired private PendingValorationsDao pendingValorationsDao;
  @Autowired private UserDao userDao;

  private TestDataBasePopulator populator;


  @BeforeClass
  public static void beforeClass() {
    LOGGER.debug("Initializing PendingValorationsHibernateDao class test...");
  }

  @Before
  public void setUp() {
    populator = new TestDataBasePopulator(this.complexDao, this.pitchDao, this.statesDao);
    populator.populate();
  }

  @Test
  @Transactional
  public void testAddPendingValoration() {
    User user = userDao.create("Marco", "Boschetti", "44332211", "asd1234", "Tritoon", "mboschetti@itba.edu.ar");
    pendingValorationsDao.addPendingValoration(user, complexDao.getById(1), LocalDate.now(), 8);
    PendingValoration pv = pendingValorationsDao.getPendingValoration(user);

    assertEquals(complexDao.getById(1), pv.getComplex());
    assertEquals(8, pv.getTime());
    assertEquals(user, pv.getUser());
  }

  @AfterClass
  public static void afterClass() {
    LOGGER.debug("Finishing PendingValorationsHibernateDao class test...");
  }
}
