package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.ComplexDao;
import ar.edu.itba.paw.interfaces.PitchDao;
import ar.edu.itba.paw.interfaces.StatesDao;
import ar.edu.itba.paw.model.States;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StatesHibernateDaoTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(ComplexHibernateDaoTest.class);

  @Autowired private ComplexDao complexDao;
  @Autowired private StatesDao statesDao;
  @Autowired private PitchDao pitchDao;

  private TestDataBasePopulator populator;


  @BeforeClass
  public static void beforeClass() {
    LOGGER.debug("Initializing StatesHibernateDao class test...");
  }

  @Before
  public void setUp() {
    populator = new TestDataBasePopulator(this.complexDao, this.pitchDao, this.statesDao);
    populator.populate();
  }

  @Test
  @Transactional
  public void testGetAllStates() {
    List<States> result = statesDao.getAllStates();
    assertNotNull(result);
//    assertTrue(result.size()>0);
  }

  @AfterClass
  public static void afterClass() {
    LOGGER.debug("Finishing StatesHibernateDao class test...");
  }
}
