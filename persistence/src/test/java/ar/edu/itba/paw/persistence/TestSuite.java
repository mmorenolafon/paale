package ar.edu.itba.paw.persistence;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({PendingValorationsHibernateDaoTest.class, ComplexHibernateDaoTest.class, PitchHibernateDaoTest.class, StatesHibernateDaoTest.class})
public class TestSuite {
}
