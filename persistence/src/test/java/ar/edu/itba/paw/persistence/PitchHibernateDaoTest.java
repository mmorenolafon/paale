package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.ComplexDao;
import ar.edu.itba.paw.interfaces.PitchDao;
import ar.edu.itba.paw.interfaces.StatesDao;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.Floor;
import ar.edu.itba.paw.model.Pitch;
import ar.edu.itba.paw.model.TimeTable;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PitchHibernateDaoTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(ComplexHibernateDaoTest.class);

  @Autowired private ComplexDao complexDao;
  @Autowired private StatesDao statesDao;
  @Autowired private PitchDao pitchDao;

  private TestDataBasePopulator populator;


  @BeforeClass
  public static void beforeClass() {
    LOGGER.debug("Initializing PitchHibernateDao class test...");
  }

  @Before
  public void setUp() {
    populator = new TestDataBasePopulator(this.complexDao, this.pitchDao, this.statesDao);
    populator.populate();
  }

  @Test
  @Transactional
  public void testGetPitchById() {
    Pitch result = pitchDao.getPitchById(1, 1);
    Complex complex = complexDao.getById(1);

    assertNotNull(result);
    assertEquals(5, result.getSize());
    assertNotNull(result.getFloor());
    assertEquals(1, result.getFloor());
    assertNotNull(result.getFloorName());
    assertEquals(Floor.getById(1), result.getFloorName());
    assertSame(complex, result.getComplex());
  }

  @Test
  @Transactional
  public void testGetPitchPrice() {
    Double result = pitchDao.getPitchPrice(6, 2, 4, 20);
    assertNotNull(result);
    assertEquals(Double.valueOf(950.0), result);

    result = pitchDao.getPitchPrice(5, 1, 5, 9);
    assertNotNull(result);
    assertEquals(Double.valueOf(1100.0), result);
  }

  @Test
  @Transactional
  public void testAddPitch() {
    Pitch pitch = new Pitch(9, 2, 11, complexDao.getById(2));
    pitchDao.addPitch(pitch);

    Pitch result = pitchDao.getPitchById(2, 11);
    assertNotNull(result);
    assertSame(pitch, result);
  }

  @Test
  @Transactional
  public void testAddSchedule() {
    TimeTable tt1 = new TimeTable(8, 22, 0, 800.0);
    TimeTable tt2 = new TimeTable(8, 22, 0, 900.0);
    TimeTable tt3 = new TimeTable(8, 22, 0, 850.0);
    assertTrue(complexDao.getComplex().get(0).getPitches().get(0).getSchedules().contains(tt1));
    assertTrue(complexDao.getComplex().get(1).getPitches().get(0).getSchedules().contains(tt2));
    assertTrue(complexDao.getComplex().get(2).getPitches().get(0).getSchedules().contains(tt3));
  }

  @AfterClass
  public static void afterClass() {
    LOGGER.debug("Finishing PitchHibernateDao class test...");
  }
}
