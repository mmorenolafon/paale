package ar.edu.itba.paw.persistence;


import ar.edu.itba.paw.interfaces.AdminDao;
import ar.edu.itba.paw.interfaces.TokenDao;
import ar.edu.itba.paw.model.Admin;
import ar.edu.itba.paw.model.Token;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class TokenHibernateDao implements TokenDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Token getToken(String token) {
        final TypedQuery<Token> query = em.createQuery("from Token as t where t.token = :token", Token.class);
        query.setParameter("token", token);
        final List<Token> list = query.getResultList();
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public void setDao(Token token) {
        em.persist(token);
    }

    @Override
    public Token getTokenByUserEmail(String userEmail) {
        final TypedQuery<Token> query = em.createQuery("from Token as t where t.userEmail = :userEmail", Token.class);
        query.setParameter("userEmail", userEmail);
        final List<Token> list = query.getResultList();
        return list.isEmpty() ? null : list.get(0);
    }
}
