package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.ComplexDao;
import ar.edu.itba.paw.interfaces.PitchDao;
import ar.edu.itba.paw.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Repository
public class PitchHibernateDao implements PitchDao {

  private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(PitchHibernateDao.class);

  @PersistenceContext
  private EntityManager em;

  @Autowired
  private ComplexDao complexDao;

  private Map<String, String> namesMapping;

  public PitchHibernateDao() {
    namesMapping = new HashMap<>();
    namesMapping.put("size", "capacity");
    namesMapping.put("floor", "floor");
    namesMapping.put("stateId", "states.id");
    namesMapping.put("day", "schedule.day");
  }

  @Override
  public Pitch getPitchById(int complexId, int pitchId) {
    PitchId id = new PitchId(complexDao.getById(complexId), pitchId);
    final TypedQuery<Pitch> query = em.createQuery("from Pitch as p where p.id = :id", Pitch.class);
    query.setParameter("id", id);
    final List<Pitch> list = query.getResultList();
    return list.isEmpty() ? null : list.get(0);
  }

  @Override
  public List<CardInformation> getAvailablePitchList(Integer stateId, String name, Integer floor, Integer size, Integer beginHour,
                                                     Integer endHour, String date) {
    String SQL_QUERY = "WITH complexwrapper AS " +
      "( " +
      "  WITH RECURSIVE hours AS " +
      "  ( " +
      "    WITH range AS ( " +
      "        SELECT " +
      "          %d AS start, " +
      "          %d AS xend " +
      "    ) " +
      "    SELECT " +
      "      start, " +
      "      xend " +
      "    FROM range " +
      "    UNION ALL " +
      "    SELECT " +
      "      start + 1, " +
      "      xend " +
      "    FROM hours " +
      "    WHERE start < xend " +
      "  ) " +
      "  SELECT DISTINCT ON (complex.id) " +
      "    hours.start  AS start, " +
      "    pitch.id     AS pitchid, " +
      "    schedule.price AS price, " +
      "    pitch_schedule.pitches_complex_id AS my_complex_id, " +
      "    complex.address AS address, " +
      "    complex.phone AS phone, " +
      "    pitch.floor AS floor, " +
      "    pitch.capacity     AS size, " +
      "    complex.name AS complexname, " +
      "    complex.email AS complex_email, " +
      "    states.name  AS statesname, " +
      "    states.id    AS state_id, " +
      "    schedule.day AS day " +
      "  FROM schedule, pitch, pitch_schedule, hours, states, complex " +
      "  WHERE schedule.id = pitch_schedule.schedules_id AND pitch_schedule.pitches_id = pitch.id " +
      "        AND hours.start >= schedule.start AND hours.start < schedule.xend " +
      "        AND pitch_schedule.pitches_complex_id = complex.id " +
      "        AND complex.id = pitch.complex_id " +
      "        AND states.id = complex.state_id AND NOT EXISTS " +
      "                                                (SELECT *" +
      "                                                 FROM reserve " +
      "                                                 WHERE reserve.date ='%s' AND reserve.start = hours.start AND " +
      "                                                       pitch.id = reserve.pitch_id AND" +
      "                                                       complex.id = reserve.pitch_complex_id) " +
      "        AND NOT EXISTS " +
      "           (SELECT * " +
      "           FROM complex_scheduleexception " +
      "           WHERE complex_scheduleexception.scheduleexceptions_begindate <= '%s' " +
      "           AND complex_scheduleexception.scheduleexceptions_enddate >= '%s' " +
      "           AND pitch_schedule.pitches_complex_id = complex_scheduleexception.complex_id ) " +
      "           %s " +
      " ORDER BY complex.id, schedule.price, hours.start ASC " +
      " ) " +
      " SELECT my_complex_id, start, pitchid, address, phone, floor, size, complexname, complex_email, " +
      "statesname, state_id, day, price , avg(coalesce(valoration, 0)) AS val_prom " +
      " FROM complexwrapper LEFT OUTER JOIN valorations ON (complexwrapper.my_complex_id = valorations.complex_id) " +
      " GROUP BY my_complex_id, start, pitchid, address, phone, floor, size, complexname, complex_email, " +
      "statesname, state_id, day, price " +
      " ORDER BY price ASC, val_prom DESC;";

    String additionalParams;
    if (floor != null) {
      additionalParams = "AND pitch.floor = " + floor + " ";
    } else {
      additionalParams = "";
    }
    if (size != null && size != 0) {
      additionalParams += "AND pitch.capacity = " + size;
    } else {
      additionalParams += "";
    }
    if (stateId != null) {
      additionalParams += "AND complex.state_id = " + stateId;
    } else {
      additionalParams += "";
    }
    String finalQuery = String.format(SQL_QUERY, beginHour, endHour, date, date, date, additionalParams);

    Query query = em.createNativeQuery(finalQuery);
    List<Object[]> list = query.getResultList();
    List<CardInformation> finalList = new LinkedList<>();
    for (Object[] pitch : list) {
      CardInformation wch = new CardInformation();
      AvailablePitch ap = new AvailablePitch();
      ap.setComplexId((int) pitch[0]);
      ap.setStartTime((int) pitch[1]);
      ap.setPitchId((int) pitch[2]);
      wch.setAddress((String) pitch[3]);
      wch.setPhone((String) pitch[4]);
      ap.setFloor((int) pitch[5]);
      ap.setSize((int) pitch[6]);
      wch.setName((String) pitch[7]);
      wch.setEmail((String) pitch[8]);
      wch.setStateName((String) pitch[9]);
      wch.setStateId((int) pitch[10]);
      ap.setPrice((double) pitch[12]);
      ap.setEndTime((ap.getStartTime() + 1));
      wch.setComplexId((int) pitch[0]);
      wch.setAvailablePitch(ap);
      finalList.add(wch);
    }
    return finalList;

  }

  @Transactional
  @Override
  public Double getPitchPrice(int complexId, int pitchId, int day, int startTime) {
    Complex complex = complexDao.getById(complexId);
    final TypedQuery<Pitch> query = em.createQuery("from Pitch as p where p.id.complex = :complex and p.id.pitchIdByComplex = :pitchId", Pitch.class);
    query.setParameter("complex", complex);
    query.setParameter("pitchId", pitchId);
    query.setParameter("pitchId", pitchId);
    return query.getSingleResult().getPrice(startTime, day);
  }

  @Override
  public List<AvailablePitch> getAvailableByComplex(Integer startTime, Integer endHour, Integer complexId,
                                                    String date, Integer day, Integer floor, Integer capacity) {

    String SQL_QUERY = "WITH RECURSIVE hours AS  " +
      "            (  " +
      "              WITH range AS (  " +
      "                  SELECT  " +
      "                    %d AS start,  " +
      "                    %d AS xend  " +
      "              )  " +
      "              SELECT  " +
      "                start,  " +
      "                xend  " +
      "              FROM range  " +
      "              UNION ALL  " +
      "              SELECT  " +
      "                start + 1,  " +
      "                xend  " +
      "              FROM hours  " +
      "              WHERE start < xend  " +
      "            )  " +
      "            SELECT DISTINCT ON (hours.start, pitch.capacity, schedule.price, pitch.floor)  " +
      "              hours.start  AS startTime,  " +
      "              pitch.id     AS pitchId,  " +
      "              schedule.price AS price,  " +
      "              pitch.floor AS floor,  " +
      "              pitch.capacity     AS size,  " +
      "              schedule.day AS day,  " +
      "              pitch_schedule.pitches_complex_id AS complexId  " +
      "            FROM schedule, pitch, pitch_schedule, hours " +
      "            WHERE schedule.id = pitch_schedule.schedules_id AND pitch_schedule.pitches_id = pitch.id  " +
      "                  AND hours.start >= schedule.start AND hours.start < schedule.xend AND pitch_schedule.pitches_complex_id = %d  " +
      "                  AND pitch_schedule.pitches_complex_id = pitch.complex_id " +
      "                  AND NOT EXISTS  " +
      "                          (SELECT * " +
      "                           FROM reserve  " +
      "                           WHERE reserve.date = '%s' AND reserve.start = hours.start AND  " +
      "                                 pitch.id = reserve.pitch_id AND reserve.pitch_complex_id = pitch_schedule.pitches_complex_id) AND schedule.day = %d " +
      "        AND NOT EXISTS " +
      "           (SELECT * " +
      "           FROM complex_scheduleexception " +
      "           WHERE complex_scheduleexception.scheduleexceptions_begindate <= '%s' " +
      "           AND complex_scheduleexception.scheduleexceptions_enddate >= '%s' " +
      "           AND pitch_schedule.pitches_complex_id = complex_scheduleexception.complex_id ) " +
      "                                  %s " +
      "            ORDER BY hours.start, capacity, price, floor ASC ;";

    String additionalParams;
    if (floor != null && floor != 0) {
      additionalParams = "AND pitch.floor = " + floor + " ";

    } else {
      additionalParams = "";
    }
    if (capacity != null && capacity != 0) {
      additionalParams += "AND pitch.capacity = " + capacity;
    } else {
      additionalParams += "";
    }
    String finalQuery = String.format(SQL_QUERY, startTime, endHour, complexId, date, day, date, date, additionalParams);
    Query query = em.createNativeQuery(finalQuery);
    List<Object[]> list = query.getResultList();
    List<AvailablePitch> finalList = new LinkedList<>();
    for (Object[] pitch : list) {
      AvailablePitch ap = new AvailablePitch();
      ap.setStartTime((int) pitch[0]);
      ap.setPitchId((int) pitch[1]);
      ap.setPrice((double) pitch[2]);
      ap.setFloor((int) pitch[3]);
      ap.setSize((int) pitch[4]);
      ap.setEndTime((ap.getStartTime() + 1));
      ap.setComplexId((int) pitch[6]);
      finalList.add(ap);
    }
    return finalList;
  }

  @Override
  public void addPitch(Pitch pitch) {
    em.persist(pitch);
  }

  @Override
  public void addSchedule(TimeTable schedule) {
    em.persist(schedule);
  }
}
