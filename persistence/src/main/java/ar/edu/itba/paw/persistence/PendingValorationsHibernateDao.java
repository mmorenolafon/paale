package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.PendingValorationsDao;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.PendingValoration;
import ar.edu.itba.paw.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;


@Repository
public class PendingValorationsHibernateDao implements PendingValorationsDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void addPendingValoration(User user, Complex complex, LocalDate date, int time) {
        final TypedQuery<PendingValoration> query = em.createQuery("FROM PendingValoration AS r WHERE r.key.user=:user AND r.key.date=:date " +
                        "                AND time=:time ORDER BY user_email, date, time DESC);",
                PendingValoration.class);
        query.setParameter("user", user);
        query.setParameter("time", time);
        query.setParameter("date", date);

        final List<PendingValoration> list = query.getResultList();
        if (list.isEmpty()) {
            PendingValoration penVal = new PendingValoration(user, complex, time, date);
            em.persist(penVal);
        }
    }


    @Override
    @Transactional
    public PendingValoration getPendingValoration(User user) {

        LocalDate today = LocalDate.now();
        LocalTime now = LocalTime.now();

        final TypedQuery<PendingValoration> query = em.createQuery("FROM PendingValoration AS r WHERE r.key.user=:user AND r.key.date<:date " +
                        "                OR (r.key.date=:date AND time<:time) ORDER BY user_email, date, time DESC);",
                PendingValoration.class);
        query.setParameter("user", user);
        query.setParameter("time", now.getHour());
        query.setParameter("date", today);

        final List<PendingValoration> list = query.getResultList();
        if (!list.isEmpty())
            em.remove(list.get(0));

        return list.isEmpty() ? null : list.get(0);
    }
}
