package ar.edu.itba.paw.persistence;


import ar.edu.itba.paw.interfaces.AdminDao;
import ar.edu.itba.paw.model.Admin;
import ar.edu.itba.paw.model.User;
import ar.edu.itba.paw.model.Complex;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

import static sun.awt.X11.XConstants.Complex;

@Repository
public class AdminHibernateDao implements AdminDao {

  @PersistenceContext
  private EntityManager em;

  @Override
  public Admin getAdmin(User user) {
    final TypedQuery<Admin> query = em.createQuery("from Admin as a where a.user = :email", Admin.class);
    query.setParameter("email", user);
    final List<Admin> list = query.getResultList();
    return list.isEmpty() ? null : list.get(0);
  }

  @VisibleForTesting
    /* default */ void createAdmin(User user, List<Complex> complexes) {
    new Admin(user, complexes);
  }
}
