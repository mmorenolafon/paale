package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.ComplexDao;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.ScheduleException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

@Repository
public class ComplexHibernateDao implements ComplexDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Complex> getComplex() {
        final TypedQuery<Complex> query = em.createQuery("from Complex as c", Complex.class);
        final List<Complex> list = query.getResultList();
        return list;
    }

    @Override
    public Complex getById(int id) {
        final TypedQuery<Complex> query = em.createQuery("from Complex as c where c.id = :id", Complex.class);
        query.setParameter("id", id);
        final List<Complex> list = query.getResultList();
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public List<ScheduleException> getScheduleExceptions(int complexId) {
        Complex complex = this.getById(complexId);
        return complex.getScheduleExceptions();
    }

    @Override
    public boolean addScheduleException(int complexId, LocalDate beginDate, LocalDate endDate) {
        Complex complex = this.getById(complexId);
        boolean result = complex.addScheduleException(beginDate, endDate);
        em.persist(complex);
        return result;
    }

    @Override
    public void deleteScheduleException(int complexId, LocalDate beginDate, LocalDate endDate) {
        Complex complex = this.getById(complexId);
        complex.deleteScheduleException(beginDate, endDate);
        em.persist(complex);
    }

    @Override
    public boolean modifyComplexFields(int complexId, String phone, String email) {
        Complex complex = getById(complexId);
        complex.setPhone(phone);
        complex.setEmail(email);
        em.persist(complex);
        return true;
    }

    @Override
    public Complex addComplex(Complex complex) {
        em.persist(complex);
        return complex;
    }

}
