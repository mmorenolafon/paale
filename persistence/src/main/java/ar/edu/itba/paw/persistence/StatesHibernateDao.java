package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.StatesDao;
import ar.edu.itba.paw.model.States;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class StatesHibernateDao implements StatesDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<States> getAllStates() {
        final TypedQuery<States> query = em.createQuery("from States as s", States.class);
        final List<States> list = query.getResultList();
        return list;
    }

    @Override
    public States getStatesById(int id) {
        final TypedQuery<States> query = em.createQuery("from States as s where s.id = :id", States.class);
        query.setParameter("id", id);
        final List<States> list = query.getResultList();
        return list.isEmpty() ? null : list.get(0);
    }

}
