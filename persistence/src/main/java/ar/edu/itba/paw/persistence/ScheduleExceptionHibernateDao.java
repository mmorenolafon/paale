package ar.edu.itba.paw.persistence;


import ar.edu.itba.paw.interfaces.ScheduleExceptionDao;
import ar.edu.itba.paw.model.ScheduleException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

@Repository
public class ScheduleExceptionHibernateDao implements ScheduleExceptionDao {

    @PersistenceContext
    private EntityManager em;


    public ScheduleExceptionHibernateDao() {
    }


    @Override
    public void removeOldScheduleExceptions() {
        List<ScheduleException> list = this.getOldScheduleExceptions();
        if (list == null) {
            return;
        }
        for (ScheduleException sc : list) {
            em.remove(sc);
        }
    }

    private List<ScheduleException> getOldScheduleExceptions() {
        final TypedQuery<ScheduleException> query =
                em.createQuery("from ScheduleException as e where e.scheduleExceptionId.endDate < :currentdate", ScheduleException.class);
        query.setParameter("currentdate", LocalDate.now());
        final List<ScheduleException> list = query.getResultList();
        return list;
    }
}
