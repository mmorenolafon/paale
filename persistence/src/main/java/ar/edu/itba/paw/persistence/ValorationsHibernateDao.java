package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.ValorationsDao;
import ar.edu.itba.paw.model.Valoration;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class ValorationsHibernateDao implements ValorationsDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void addValoration(Valoration val) {
        em.merge(val);
    }

    @Override
    public void deleteByQuantity(int quantity) {
    }
}
