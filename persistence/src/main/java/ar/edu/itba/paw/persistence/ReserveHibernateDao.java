package ar.edu.itba.paw.persistence;

import ar.edu.itba.paw.interfaces.ReserveDao;
import ar.edu.itba.paw.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;


@Repository
public class ReserveHibernateDao implements ReserveDao {

    private Logger LOGGER = LoggerFactory.getLogger(ReserveHibernateDao.class);

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Reserve> getUserReserveHistory(User user) {
        final TypedQuery<Reserve> query = em.createQuery("from Reserve as r where r.user = :user and r.id.date < :date order by r.id.date desc", Reserve.class);
        query.setParameter("user", user);
        query.setParameter("date", LocalDate.now());
        final List<Reserve> list = query.getResultList();
        return list;
    }


    @Override
    public List<Reserve> getUserReserves(User user) {
        final TypedQuery<Reserve> query = em.createQuery("from Reserve as r where r.user = :user and r.id.date > :date order by r.id.date asc", Reserve.class);
        query.setParameter("user", user);
        query.setParameter("date", LocalDate.now());
        final List<Reserve> list = query.getResultList();
        return list;
    }

    @Override
    public boolean addReserve(Pitch pitch, int startTime, int endTime, LocalDate date, User user, String bookerUsername) {
        if (getReserve(pitch, startTime, date, user) != null) {
            return false;
        }
        int dayOfWeek = (date.getDayOfWeek().getValue() - 1) % 7;
        double price = pitch.getPrice(startTime, dayOfWeek);
        final Reserve reserve = new Reserve(pitch, startTime, endTime, date, user, bookerUsername, price, 0.0);
        em.persist(reserve);
        return true;
    }

    @Override
    public boolean removeBooking(Pitch pitch, int startTime, LocalDate date, User user) {
        if (user != null) {
            PendingValoration pv = getPV(pitch, startTime, date, user);
            if(pv != null){
              em.remove(pv);
            }
        }

        if ( pitch == null || date == null ) {
            return false;
        }

        Reserve reserve = getReserve(pitch, startTime, date, user);
        if (reserve != null)
            em.remove(reserve);
        else {
            LOGGER.info("Could not remove reserve, reserve is null");
            return false;
        }
        return true;
    }

    @Override
    public List<Reserve> getBookingsByComplex(Complex complex, String user, LocalDate date, int startTime, int endTime) {
        final TypedQuery<Reserve> query;
        if (user == null) {
            query = (TypedQuery<Reserve>) em.createQuery("from Reserve as r where r.id.pitch.id.complex = :complex and r.id.date = :date and r.id.start >= :startTime and r.end <= :endTime");
        } else {
            query = (TypedQuery<Reserve>) em.createQuery("from Reserve as r where r.id.pitch.id.complex = :complex and r.id.date = :date and r.id.start >= :startTime and r.end <= :endTime and (r.user.email like :user" +
              " or r.bookerUsername like :user)");
            query.setParameter("user", user);
        }
        query.setParameter("complex", complex);
        query.setParameter("startTime", startTime);
        query.setParameter("date", date);
        query.setParameter("endTime", endTime);
        final List<Reserve> list = query.getResultList();
        return list;
    }

    private Reserve getReserve(Pitch pitch, int startTime, LocalDate date, User user) {
        final TypedQuery<Reserve> query;
        if (user == null) {
            query = (TypedQuery<Reserve>) em.createQuery("from Reserve as r where r.id.pitch = :pitch and r.id.date = :date and r.id.start = :startTime");
        } else {
            query = (TypedQuery<Reserve>) em.createQuery("from Reserve as r where r.id.pitch = :pitch and r.id.date = :date and r.id.start = :startTime and r.user = :user");
            query.setParameter("user", user);
        }
        query.setParameter("pitch", pitch);
        query.setParameter("startTime", startTime);
        query.setParameter("date", date);
        final List<Reserve> list = query.getResultList();
        return list.isEmpty() ? null : list.get(0);
    }

    private PendingValoration getPV(Pitch pitch, int starTime, LocalDate date, User user) {

        final TypedQuery<PendingValoration> query;
        query = (TypedQuery<PendingValoration>) em.createQuery(
                "from PendingValoration as pv where pv.key.complex = :complex and pv.key.date = :date and pv.key.user = :user and pv.key.time = :startTime");
        query.setParameter("complex", pitch.getComplex());
        query.setParameter("startTime", starTime);
        query.setParameter("date", date);
        query.setParameter("user", user);
        final List<PendingValoration> list = query.getResultList();
        return list.isEmpty() ? null : list.get(0);
    }

}
