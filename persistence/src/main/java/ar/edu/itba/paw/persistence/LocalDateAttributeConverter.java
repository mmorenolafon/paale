package ar.edu.itba.paw.persistence;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;

@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {


    @Override
    public Date convertToDatabaseColumn(LocalDate locDate) {
        System.out.println("LOCALDATE : "+ locDate);
        return (locDate == null ? null : Date.valueOf(locDate));
    }

    @Override
    public LocalDate convertToEntityAttribute(Date sqlDate) {
        System.out.println("SQLDATE : "+ sqlDate);
        return (sqlDate == null ? null : sqlDate.toLocalDate());
    }
}
