# README #
Integrantes:

55156     Mogni, Guido Matías
55266     Boschetti, Marco
55332     Moreno Lafon, Maria Milagros
55124     Lepore, Juan Cruz


El proyecto propuesto consiste en una aplicación Web orientada a la reserva de canchas de fútbol en el ámbito de la Ciudad de Buenos Aires. 
El público en general debe ser capaz de:
Ver la canchas disponibles con diferentes filtros, seleccionándolas por ubicación, rango de precios, tamaño y disponibilidad horaria.
Realizar reserva mediante herramientas brindadas por la aplicación, notificando el dueño de la cancha.
Cancelar una reserva hecha sobre la cancha.
Proveer métodos efectivos para posibilitar la comunicación con el dueño de las canchas, previendo casos de reclamos o preguntas.

Para administrar la reserva de canchas, el sistema contará con panel de control accesible por los propietarios de las canchas de fútbol, donde podrán ver y gestionar la utilización de cada cancha individualmente. Los dueños de las canchas deben ser capaces de agregar y remover canchas.
La aplicación no estará integrada con servicios de pago de terceros pues este no es un rasgo fundamental de la misma.