package ar.edu.itba.paw.service;

import ar.edu.itba.paw.model.*;
import ar.edu.itba.paw.model.DTOs.ReserveDTO;
import ar.edu.itba.paw.persistence.ComplexHibernateDao;
import ar.edu.itba.paw.persistence.PitchHibernateDao;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@Transactional
public class BookingServiceImplTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(PitchServiceImpl.class);

  @InjectMocks private BookingServiceImpl bookingService;
  private User user;
  @Spy private UserServiceImpl userService;
  @Mock private ComplexServiceImpl complexService;
  @Mock private ComplexHibernateDao complexDao;
  @Mock private PitchServiceImpl pitchService;
  @Mock private PitchHibernateDao pitchDao;


  @BeforeClass
  public static void beforeClass() {
    LOGGER.debug("Initializing BookingService class test...");
  }

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    bookingService = new BookingServiceImpl();
    bookingService.setPitchService(this.pitchService);
    bookingService.setComplexService(this.complexService);
    user = new User("Milagros", "Moreno", "asd1234", "mmml@yahoo.com.ar", "1234-5678", "mmml");
    when(userService.getLoggedUser()).thenReturn(user);
    bookingService.setUserService(this.userService);

  }

  @Test
  public void makeVisibleTest() {
    Complex complex = new Complex("address", "1111-5555", "ComplexName", "email@email.com", new States(1, "StateName"));
    Pitch pitch = new Pitch(5, 1, 1, complex);
    LocalDate date = LocalDate.now().plus(20, ChronoUnit.DAYS);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    String dateString = date.format(dtf);

    Reserve reserve1 = new Reserve(pitch, 8, 9, date, user, "BookerUsername", 5000, 3000);
    Reserve reserve2 = new Reserve(pitch, 10, 11, date, user, "BookerUsername", 5000, 3000);
    Reserve reserve3 = new Reserve(pitch, 12, 13, date, user, "BookerUsername", 5000, 3000);
    Reserve reserve4 = new Reserve(pitch, 14, 15, date, user, "BookerUsername", 5000, 3000);
    List<Reserve> reserves = new ArrayList<>();
    reserves.add(reserve1);
    reserves.add(reserve2);
    reserves.add(reserve3);
    reserves.add(reserve4);

    when(pitchService.getPitchById(eq(complex.getId()), eq(pitch.getId().getPitchIdByComplex()))).thenReturn(pitch);
    when(complexService.getComplexById(anyInt())).thenReturn(complex);
    List<ReserveDTO> reserveDTOs = bookingService.makeVisible(reserves);

    assertTrue(reserveDTOs.contains(new ReserveDTO("ComplexName", pitch.getSize(), 8, dateString, "mmml@yahoo.com.ar",
                                    5000, 3000, pitch.getId().getPitchIdByComplex(), complex.getId(), date,
                                    "BookerUsername")));
    assertTrue(reserveDTOs.contains(new ReserveDTO("ComplexName", pitch.getSize(), 10, dateString, "mmml@yahoo.com.ar",
                                    5000, 3000, pitch.getId().getPitchIdByComplex(), complex.getId(), date,
                                    "BookerUsername")));
    assertTrue(reserveDTOs.contains(new ReserveDTO("ComplexName", pitch.getSize(), 12, dateString, "mmml@yahoo.com.ar",
                                    5000, 3000, pitch.getId().getPitchIdByComplex(), complex.getId(), date,
                                    "BookerUsername")));
    assertTrue(reserveDTOs.contains(new ReserveDTO("ComplexName", pitch.getSize(), 14, dateString, "mmml@yahoo.com.ar",
                                    5000, 3000, pitch.getId().getPitchIdByComplex(), complex.getId(), date,
                                    "BookerUsername")));

  }


  @AfterClass
  public static void afterClass() {
    LOGGER.debug("Finishing ComplexJdbcDao class test...");
  }
}
