package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.EmailSender;
import ar.edu.itba.paw.interfaces.UserDao;
import ar.edu.itba.paw.model.User;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)

public class UserServiceImplTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImplTest.class);

  private static final String CREATE_FIRSTNAME = "Marco";
  private static final String CREATE_LASTNAME = "Boschetti";
  private static final String CREATE_PHONE = "44332211";
  private static final String CREATE_PASSWORD = "asd1234";
  private static final String CREATE_USERNAME = "Tritoon";
  private static final String CREATE_EMAIL = "mboschetti@itba.edu.ar";

  @Mock
  private UserDao userDaoMock;

  @Mock
  private EmailSender emailSenderMock;

  private UserServiceImpl userService;


  @BeforeClass
  public static void beforeClass() {
    LOGGER.debug("Initializing ComplexServiceImpl Test...");
  }

  @Before
  public void setUp() {

    User ansUser = new User(CREATE_FIRSTNAME, CREATE_LASTNAME, CREATE_PASSWORD, CREATE_EMAIL, CREATE_PHONE, CREATE_USERNAME);

    MockitoAnnotations.initMocks(this);
    when(userDaoMock.create(any(), any(), any(), any(), any(), any())).thenReturn(ansUser);
    when(emailSenderMock.sendMailTo(any(), any(), any())).thenReturn(true);
    userService = new UserServiceImpl();
  }

  @Test
  public void reserve() {
    userService.setUserDao(userDaoMock);
    userService.setEmailSender(emailSenderMock);
    User ans = userService.register(CREATE_FIRSTNAME, CREATE_LASTNAME, CREATE_PASSWORD, CREATE_EMAIL, CREATE_PHONE, CREATE_USERNAME);
    assertNotNull(ans);
    assertEquals(ans.getFirstName(), CREATE_FIRSTNAME);
    assertEquals(ans.getLastName(), CREATE_LASTNAME);
    assertEquals(ans.getPassword(), CREATE_PASSWORD);
    assertEquals(ans.getEmail(), CREATE_EMAIL);
    assertEquals(ans.getPhone(), CREATE_PHONE);
    verify(userDaoMock).create(CREATE_FIRSTNAME, CREATE_LASTNAME, CREATE_PASSWORD, CREATE_EMAIL, CREATE_PHONE, CREATE_USERNAME);
  }


  @AfterClass
  public static void afterClass() {
    LOGGER.debug("Finishing ComplexServiceImpl Test...");
  }

}
