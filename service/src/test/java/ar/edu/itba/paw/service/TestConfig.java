package ar.edu.itba.paw.service;

import org.hsqldb.jdbc.JDBCDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan({ "ar.edu.itba.paw.service", "ar.edu.itba.paw.persistence" })
@EnableTransactionManagement
public class TestConfig {

  @Bean
  public JavaMailSender javaMailService() {
    JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

    javaMailSender.setHost("smtp.gmail.com");
    javaMailSender.setPort(587);

    javaMailSender.setJavaMailProperties(getMailProperties());

    return javaMailSender;
  }


  private Properties getMailProperties() {
    Properties properties = new Properties();
    properties.setProperty("mail.transport.protocol", "smtp");
    properties.setProperty("mail.smtp.auth", "false");
    properties.setProperty("mail.smtp.starttls.enable", "false");
    properties.setProperty("mail.debug", "false");
    return properties;
  }

  @Bean
  public DataSource dataSource() {

    final SimpleDriverDataSource ds = new SimpleDriverDataSource();
    ds.setDriverClass(JDBCDriver.class);
    ds.setUrl("jdbc:hsqldb:mem:paw");
    ds.setUsername("hq");
    ds.setPassword("");

    return ds;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
    factoryBean.setPackagesToScan("ar.edu.itba.paw.model");

    factoryBean.setDataSource(dataSource());

    final JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    factoryBean.setJpaVendorAdapter(vendorAdapter);

    final Properties properties = new Properties();
    properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
    properties.setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");

    properties.setProperty("hibernate.show_sql", "false");
    properties.setProperty("use_sql_comments", "false");
    properties.setProperty("format_sql", "false");

    factoryBean.setJpaProperties(properties);

    return factoryBean;
  }

  @Bean
  public PlatformTransactionManager transactionManager() {
    return new DataSourceTransactionManager(dataSource());
  }
}
