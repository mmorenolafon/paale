INSERT INTO states (name) VALUES
			('Chacarita'),
			('Belgrano'),
			('Caballito');

INSERT INTO complex (name,email,address,stateId,phone) VALUES
			('Complex1','paco@itba.edu.ar','Calle 1',1,'43051689'),
			('Complex2','tito@itba.edu.ar','Calle 2',2,'46897531'),
			('Complex3','jorge@itba.edu.ar','Calle 3',3,'49782513');

INSERT INTO pitch(capacity,floor,complexId) VALUES
						(2,1,1),
						(5,1,2),
						(5,2,2),
						(5,2,2),
						(5,1,3);

INSERT INTO schedule (day, start ,xend,price) VALUES
(1,12,19,500),
(2,19,23,800),
(3,19,23,800),
(4,12,19,500);

INSERT INTO users VALUES
('Guido','Mogni','asd1234','gmogni@itba.edu.ar','43434433','gibarsin');

