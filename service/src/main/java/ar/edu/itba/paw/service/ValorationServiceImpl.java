package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.*;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.PendingValoration;
import ar.edu.itba.paw.model.User;
import ar.edu.itba.paw.model.Valoration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

@Service
public class ValorationServiceImpl implements ValorationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValorationServiceImpl.class);

    @Autowired
    private UserService us;

    @Autowired
    private ValorationsDao vd;

    @Autowired
    private PendingValorationsDao pd;

    @Autowired
    private ComplexService cs;

    @Override
    @Transactional
    public void addValoration(int complexid, int valoration, String dateString) {
        if (valoration < 1 || valoration > 1000) {
            LOGGER.info("valoration out of range");
            return;
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        dtf.withResolverStyle(ResolverStyle.STRICT);

        LocalDate date = null;
        try {
            date = LocalDate.parse(dateString, dtf);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            return;
        }

        User user = us.getLoggedUser();
        if (user == null)
            return;

        Complex complex = cs.getComplexById(complexid);

        vd.addValoration(new Valoration(user, complex, date, valoration));
    }

    @Override
    public void addPendingValoration(int complexId, LocalDate date, int time) {
        User user = us.getLoggedUser();
        if (user == null)
            return;

        Complex complex = cs.getComplexById(complexId);
        pd.addPendingValoration(user, complex, date, time);
    }

    @Override
    public PendingValoration getPendingValoration() {
        User user = us.getLoggedUser();
        if (user == null)
            return null;
        return pd.getPendingValoration(user);
    }


}
