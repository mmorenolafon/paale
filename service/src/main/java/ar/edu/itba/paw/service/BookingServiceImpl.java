package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.*;
import ar.edu.itba.paw.model.*;
import ar.edu.itba.paw.model.DTOs.ReserveDTO;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.AccessControlException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.DataFormatException;


@Service
public class BookingServiceImpl implements BookingService {

  private static final Logger LOGGER = LoggerFactory.getLogger(BookingServiceImpl.class);


  @Autowired
  private EmailSender es;

  @Autowired
  private ReserveDao rd;

  @Autowired
  private ComplexService cd;

  @Autowired
  private PitchService pd;

  @Autowired
  private UserService us;

  @Autowired
  private ValorationService vs;

  @Autowired
  private AdminDao ad;



  @Transactional
  public boolean book(int complexId, int pitchId, String date, int startTime) throws DataFormatException {

    LocalDate localDate;
    LocalDateTime localDateTime;

    try {
      localDate = LocalDate.parse(date);
      localDateTime = localDate.atTime(startTime, 0);
    } catch (DateTimeParseException e) {
      LOGGER.info("Invalid date or time");
      return false;
    }

    User user = us.getLoggedUser();
    if (user == null)
      throw new AccessControlException("Logged user required.");

    Pitch pitch = pd.getPitchById(complexId, pitchId);
    if (pitch == null)
      throw new DataFormatException("Pitch does not exist.");

    boolean result = rd.addReserve(pitch, startTime, startTime + 1, localDate, user, user.getUsername());

    if (result) {

      cd.getComplexById(complexId).setLastModified(LocalDateTime.now());

      if (ad.getAdmin(user) == null) {
        es.sendBookingConfirmation(complexId, pitchId, localDate.getDayOfMonth(), localDate.getMonthValue(),
          localDate.getYear(), startTime, startTime + 1);
        vs.addPendingValoration(complexId, localDate, startTime);
      }
    }

    return result;
  }

  @Override
  @Transactional
  public boolean book(Admin admin, String bookerUsername, int complexId, int pitchId, String date, int startTime) throws DataFormatException {
    LocalDate localDate;

    try {
      localDate = LocalDate.parse(date);
    } catch (DateTimeParseException e) {
      LOGGER.info("Invalid date or time");
      throw new DataFormatException("Invalid date or time");
    }

    Pitch pitch = pd.getPitchById(complexId, pitchId);
    if (pitch == null)
      throw new DataFormatException("Pitch does not exist.");

    cd.getComplexById(complexId).setLastModified(LocalDateTime.now());
    boolean result = rd.addReserve(pitch, startTime, startTime + 1, localDate, admin.getUser(), bookerUsername);

    return result;
  }

  @Override
  public List<ReserveDTO> getUserReserveHistory(String email) {
    User user = us.getByUsername(email);
    if (user == null)
      throw new IllegalArgumentException("No Log user.");
    return makeVisible(rd.getUserReserveHistory(user));
  }

  @Override
  public List<ReserveDTO> getUserReserves(String email) {
    User user = us.getByUsername(email);

    if (user == null)
      throw new IllegalArgumentException("No logged user.");
    return makeVisible(rd.getUserReserves(user));
  }

  @Override
  public List<ReserveDTO> makeVisible(List<Reserve> reserves) {
    List<ReserveDTO> ans = new LinkedList<>();
    for (Reserve reserve : reserves) {
      LocalDate cal = reserve.getDate();

      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

      String date = cal.format(dtf);


      ans.add(new ReserveDTO(
        cd.getComplexById(reserve.getComplexId()).getName(),
        pd.getPitchById(
          reserve.getComplexId(),
          reserve.getPitchId()).getSize(),
        reserve.getBegin(),
        date,
        reserve.getUserEmail().getEmail(),
        reserve.getPrice(),
        reserve.getPaid(),
        reserve.getPitchId(),
        reserve.getComplexId(),
        reserve.getDate(),
        reserve.getBookerUsername()
      ));
    }
    return ans;
  }

  @Override
  public Complex getComplexById(int complexId) {
    return cd.getComplexById(complexId);
  }

  @Override
  public Pitch getPitchById(int complexId, int pitchId) {
    return pd.getPitchById(complexId, pitchId);
  }

  @Override
  public Double getPitchPrice(int complexId, int pitchId, String date, int startTime) {
    return pd.getPitchPrice(complexId, pitchId, date, startTime);
  }

  @Override
  @Transactional
  public boolean removeBooking(int complexId, int pitchId, String dateString, int startTime) throws DataFormatException {

    LocalDate date;

    try {
      date = LocalDate.parse(dateString);
    } catch (DateTimeParseException e) {
      LOGGER.info("Invalid date or time");
      throw new DataFormatException("Invalid date or time");
    }

    Pitch pitch = pd.getPitchById(complexId, pitchId);
    if (pitch == null)
      throw new DataFormatException("Pitch does not exist.");

    User user = us.getLoggedUser();
    if (ad.getAdmin(user) != null) {
      return rd.removeBooking(pitch, startTime, date, null);
    }
    return rd.removeBooking(pitch, startTime, date, user);
  }

  @Override
  public List<Reserve> getBookingsByComplex(int complexId, String username, String dateString, int startTime, int endTime) throws DataFormatException {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    dtf.withResolverStyle(ResolverStyle.STRICT);
    LocalDate date = null;
    try {
      date = LocalDate.parse(dateString, dtf);
    } catch (DateTimeParseException e) {
      return null;
    }
    //date.plus(startTime, ChronoUnit.HOURS);

    if (date.isBefore(LocalDate.now())) {
      LOGGER.info("Invalid date or time: ");
      return null;
    }

    Complex complex = cd.getComplexById(complexId);
    if (complex == null)
      throw new IllegalArgumentException("complex does not exist.");

    if (username != null) {
      username = String.format("%%%s%%", username);
    }

    List<Reserve> bookings = rd.getBookingsByComplex(complex, username, date, startTime, endTime);

    return bookings;

  }

  @Override
  public void invitePlayers(int complexId, int pitchId, String dateString, int startTime, String[] emailList, String message) {

    LocalDate date = null;

    try {
      date = LocalDate.parse(dateString);
    } catch (DateTimeParseException e) {
      LOGGER.info("Invalid date or time");
      return;
    }

    User user = us.getLoggedUser();

    es.sendBookingInvitation(complexId, pitchId, date.getDayOfMonth(), date.getMonthValue(),
      date.getYear(), startTime, startTime + 1, emailList, message);
  }


  @VisibleForTesting
    /* default */ void setUserService(UserService us) {
    this.us = us;
  }

  @VisibleForTesting
    /* default */ void setPitchService(PitchService pd) { this.pd = pd; }

  @VisibleForTesting
    /* default */ void setComplexService(ComplexService cd) { this.cd = cd; }

  @VisibleForTesting
    /* default */ void setReserveDao(ReserveDao rd) {
    this.rd = rd;
  }

  @VisibleForTesting
    /* default */ void setEmailSender(EmailSender es) {
    this.es = es;
  }

}
