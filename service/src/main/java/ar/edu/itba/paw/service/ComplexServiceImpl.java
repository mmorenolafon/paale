package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.ComplexDao;
import ar.edu.itba.paw.interfaces.ComplexService;
import ar.edu.itba.paw.interfaces.PitchService;
import ar.edu.itba.paw.model.*;
import ar.edu.itba.paw.model.DTOs.PitchDTO;
import ar.edu.itba.paw.model.DTOs.PitchSetTimeTableDTO;
import ar.edu.itba.paw.model.DTOs.PitchTimeTableDTO;
import ar.edu.itba.paw.model.DTOs.TimeTableDTO;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.*;

@Service
public class ComplexServiceImpl implements ComplexService {


  @Autowired
  private ComplexDao complexDao;

  @Autowired
  private PitchService pitchService;

  @Override
  public List<CardInformation> getComplex(Integer stateId, String name, Integer floor, Integer size, Integer beginHour,
                                          Integer endHour, String date) throws IllegalArgumentException {
    date = date.substring(0, 10);

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    df.setLenient(false);
    try {
      df.parse(date);
    } catch (ParseException e) {
      throw new IllegalArgumentException("Incorrect date format");
    }

    return pitchService.getAvailable(stateId, name, floor, size, beginHour, endHour, date);

  }


  @Override
  public Complex getComplexById(int id) {
    return complexDao.getById(id);
  }

  @Override
  @Transactional
  public boolean modifyComplexFields(int complexId, String phone, String email) {
    return complexDao.modifyComplexFields(complexId, phone, email);
  }

  @Override
  public List<ScheduleException> getScheduleExceptions(int complexId) {
    return complexDao.getScheduleExceptions(complexId);
  }

  @Override
  public boolean addScheduleException(int complexId, LocalDate beginDate, LocalDate endDate) {
    return complexDao.addScheduleException(complexId, beginDate, endDate);
  }

  @Override
  public void deleteScheduleException(int complexId, LocalDate beginDate, LocalDate endDate) {
    complexDao.deleteScheduleException(complexId, beginDate, endDate);
  }

  @Transactional
  @Override
  public List<PitchTimeTableDTO> getSchedule(int complexId) {

    Complex complex = getComplexById(complexId);
    List<PitchDTO> pitchesDTOs = new LinkedList<>();

    Map<Integer, List<TimeTable>>[] rawBlocks = new Map[7];
    for (int i = 0; i < rawBlocks.length; i++) {
      rawBlocks[i] = new HashMap<>();
    }

    Map<Integer, Pitch> timeTables = new HashMap<>();
    List<PitchTimeTableDTO> pitchDTOs = new LinkedList<>();

    for (Pitch p : complex.getPitches()) {
      Pitch pfull = pitchService.getPitchById(complexId, p.getId().getPitchIdByComplex());

      PitchTimeTableDTO pitchDTO = new PitchTimeTableDTO();
      pitchDTO.setPitch(p.toDTO());
      pitchDTOs.add(pitchDTO);

      timeTables.put(p.getId().getPitchIdByComplex(), pfull);

      for (int i = 0; i < rawBlocks.length; i++) {
        Map<Integer, List<TimeTable>> l = rawBlocks[i];
        l.put(p.getId().getPitchIdByComplex(), new LinkedList<>());
      }

      for (TimeTable tt : pfull.getSchedules()) {
        rawBlocks[tt.getDay()].get(p.getId().getPitchIdByComplex()).add(tt);
      }

      pitchesDTOs.add(pfull.toDTO());
    }

    for (int i = 0; i < rawBlocks.length; i++) {
      for (PitchTimeTableDTO ptt : pitchDTOs) {
        if (rawBlocks[i].containsKey(ptt.getPitch().getPitchId())) {
          List<Integer> times = new LinkedList<>();
          List<Double> prices = new LinkedList<>();
          List<TimeTable> timetables = rawBlocks[i].get(ptt.getPitch().getPitchId());

          List<Integer> ends = new LinkedList<>();
          Map<Integer, Double> startAndPrice = new HashMap<>();

          for (TimeTable tt : timetables) {
            times.add(tt.getBegin());
            ends.add(tt.getEnd());
            startAndPrice.put(tt.getBegin(), tt.getPrice());
          }
          if (timetables.size() == 0) {
            times.add(7);
            ends.add(24);
            startAndPrice.put(7, -1d);
          }
          Iterator<Integer> endsIt = ends.iterator();

          while (endsIt.hasNext()) {
            int aux = endsIt.next();
            if (!times.contains(aux)) {
              times.add(aux);
            }
          }
          Collections.sort(times);
          for (Integer time : times) {
            if (startAndPrice.containsKey(time)) {
              prices.add(startAndPrice.get(time));
            } else {
              prices.add(-1d);
            }
          }
          ptt.addSchedule(i, new TimeTableDTO(times, prices));
        }
      }
    }

    return pitchDTOs;
  }

  @Transactional
  @Override
  public void setNewPrices(Integer complexId, PitchSetTimeTableDTO timeTables) {

    List<TimeTable> newTimeTables = new LinkedList<>();

    for (int day = 0; day < 7; day++) {
      List<Integer> times = timeTables.getTimeTables().get(day).getNodes();
      List<Double> prices = timeTables.getTimeTables().get(day).getPrices();

      for (int time = 0; time < times.size(); time++) {
        if (time < times.size() - 1) {
          if (prices.get(time) != -1) {
            TimeTable newTimeTable = new TimeTable(times.get(time), times.get(time + 1), day, prices.get(time));
            newTimeTables.add(newTimeTable);
            pitchService.persistSchedule(newTimeTable);
          }
        }
      }
    }

    for (Integer pitchId : timeTables.getPitchIds()) {
      Pitch pitch = pitchService.getPitchById(complexId, pitchId);
      pitch.setSchedules(newTimeTables);
      pitchService.persistPitch(pitch);
    }

  }

  @VisibleForTesting
    /* default */ void setComplexDao(ComplexDao complexDao) {
    this.complexDao = complexDao;
  }

  private String convertToDay(String date) {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    dtf.withResolverStyle(ResolverStyle.STRICT);

    LocalDate cal = null;
    try {
      cal.parse(date, dtf);
    } catch (DateTimeParseException e) {
      e.printStackTrace();
    }
    int day = cal.getDayOfWeek().getValue() - 1;
    return String.valueOf(day);
  }


}
