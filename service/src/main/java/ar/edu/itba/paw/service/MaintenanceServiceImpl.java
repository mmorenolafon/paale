package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.MaintenanceService;
import ar.edu.itba.paw.interfaces.ScheduleExceptionService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MaintenanceServiceImpl implements MaintenanceService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(BookingServiceImpl.class);

    @Autowired
    ScheduleExceptionService ses;


    @Scheduled(cron = "0 0 3 * * ?")
    @Override
    @Transactional
    public void cleanUpScheduleExceptions() {
        LOGGER.info("[SCHEDULER] Removing old schedule exceptions");
        ses.removeOldScheduleExceptions();
    }
}
