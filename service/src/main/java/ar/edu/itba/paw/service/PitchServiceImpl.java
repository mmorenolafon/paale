package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.PitchDao;
import ar.edu.itba.paw.interfaces.PitchService;
import ar.edu.itba.paw.model.AvailablePitch;
import ar.edu.itba.paw.model.CardInformation;
import ar.edu.itba.paw.model.Pitch;
import ar.edu.itba.paw.model.TimeTable;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.List;

@Service
public class PitchServiceImpl implements PitchService {

  private static final Logger LOGGER = LoggerFactory.getLogger(PitchServiceImpl.class);

  @Autowired
  private PitchDao pitchDao;

  @Override
  public List<Pitch> getPitch(String complexId) {
    return null;
  }


  @Override
  public List<AvailablePitch> getPitchAvailable(Integer startTime, Integer endTime, Integer complexId,
                                                String date, Integer day, Integer floor, Integer capacity) {

    return pitchDao.getAvailableByComplex(startTime, endTime, complexId, date, day, floor, capacity);
  }

  @Override
  public List<CardInformation> getAvailable(Integer stateId, String name, Integer floor, Integer size, Integer beginHour,
                                            Integer endHour, String date) {
    return pitchDao.getAvailablePitchList(stateId, name, floor, size, beginHour, endHour, date);
  }

  private int convertToDay(String dateString) {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    dtf.withResolverStyle(ResolverStyle.STRICT);
    LocalDate cal = null;

    try {
      cal = LocalDate.parse(dateString, dtf);
      int date = cal.getDayOfWeek().getValue() - 1;
      return date;
    } catch (DateTimeParseException e) {
    }
    return 0;
  }

  @Override
  public List<AvailablePitch> getGroupsAvailable(Integer floor, Integer capacity, Integer startTime, Integer endHour,
                                                 String date, Integer complexId) throws ParseException {

    LOGGER.info("complexId: {}", complexId);
    LOGGER.info("StartTime: {}", startTime);
    LOGGER.info("date: {}", date);
    LOGGER.info("Floor: {}", floor);
    LOGGER.info("Capacity: {}", capacity);

    try {
      date = date.substring(0, 10);

      DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
      df.setLenient(false);
      df.parse(date);
    } catch (StringIndexOutOfBoundsException e) {
    }

    int day = convertToDay(date);

    LOGGER.debug("Parameters validated");

    List<AvailablePitch> list = this.getPitchAvailable(startTime, endHour, complexId, date, day, floor, capacity);

    return list;
  }

  @Override
  public Pitch getPitchById(int complexId, int pitchId) {
    return pitchDao.getPitchById(complexId, pitchId);
  }

  @Override
  public Double getPitchPrice(int complexId, int pitchId, String date, int startTime) {
    return pitchDao.getPitchPrice(complexId, pitchId, Integer.valueOf(convertToDay(date)), startTime);
  }

  @Override
  public void persistPitch(Pitch pitch) {
    pitchDao.addPitch(pitch);
  }

  @Override
  public void persistSchedule(TimeTable timeTable) {
    pitchDao.addSchedule(timeTable);
  }

  @VisibleForTesting
    /* default */ void setComplexDao(PitchDao pd) {
    pitchDao = pd;
  }

}
