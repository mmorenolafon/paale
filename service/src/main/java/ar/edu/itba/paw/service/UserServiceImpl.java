package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.BookingService;
import ar.edu.itba.paw.interfaces.EmailSender;
import ar.edu.itba.paw.interfaces.UserDao;
import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.model.DTOs.ReserveDTO;
import ar.edu.itba.paw.model.User;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.zip.DataFormatException;


@Service
public class UserServiceImpl implements UserService {

  final static String ANONYMOUS_USER = "anonymousUser";
  final static String NO_USER = "";
  final String REGISTRATION_MAIL_HEADER = "Hay Equipo - Nueva cuenta";
  final String REGISTRATION_MAIL_BODY = "¡Hola %s!\nTu cuenta en Hay Equipo fue creada exitosamente.\n" +
    "Podes ingresar con la dirección de correo electrónico: \"%s\".\n" +
    "¡Que empiece el partido!\n Hay Equipo\n";


  @Autowired
  private BookingService bs;

  @Autowired
  private UserDao userDao;

  @Autowired
  private EmailSender emailSender;

  @Transactional
  @Override
  public User register(String firstName, String lastName, String password, String email, String phone, String username) {

    User registered = userDao.create(firstName, lastName, password, email, phone, username);
    if (registered != null) {
        emailSender.sendMailTo(email, REGISTRATION_MAIL_HEADER, String.format(REGISTRATION_MAIL_BODY, firstName, email));
    }

    return registered;
  }

   @Override
  public User getByUsername(String username) {
    if (ANONYMOUS_USER.equals(username) || NO_USER.equals(username)) {
      return null;
    }
    User user = userDao.getByUsername(username);
    if (user == null) {
      return userDao.getByEmail(username);
    }
    return user;
  }

  @Override
  public User getLoggedUser() {
    final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth == null) {
      return null;
    }

    User user = getByUsername((String) auth.getPrincipal());
    if (user == null) {
      user = getByEmail((String) auth.getPrincipal());
    }
    return user;
  }

  @Override
  public List<ReserveDTO> getUserReserveHistory(User user) {
    return bs.getUserReserveHistory(user.getEmail());
  }

  @Override
  public List<ReserveDTO> getUserReserves(User user) {
    return bs.getUserReserves(user.getEmail());
  }

  @Override
  public User getByEmail(String email) {
    return userDao.getByEmail(email);
  }

  @Override
  public String getLoggedUserName() {
    final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth == null) {
      return null;
    }

    final String username = (String) auth.getPrincipal();

    User user = getByUsername(username);
    if (user == null) {
      user = getByEmail(username);
    }
    if (user == null) {
      return null;
    }
    return user.getFirstName();
  }

  @Override
  public List<User> getByPartOfEmail(String email) {
    return userDao.getByPartOfEmail(String.format("%%%s%%",email));
  }

  @Override
  public boolean removeBooking(int complexId, int pitchId, String date, int startTime) throws DataFormatException {
    return bs.removeBooking(complexId, pitchId, date, startTime);
  }

  @VisibleForTesting
    /*default*/void setUserDao(final UserDao userDao) {
    this.userDao = userDao;
  }

  @VisibleForTesting
    /*default*/void setEmailSender(final EmailSender emailSender) {
    this.emailSender = emailSender;
  }

}
