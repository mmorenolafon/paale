package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.AdminDao;
import ar.edu.itba.paw.interfaces.AdminService;
import ar.edu.itba.paw.model.Admin;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminDao ad;

    @Transactional
    @Override
    public Admin getAdmin(User user) {
        if (user == null || user.getEmail() == null) {
            return null;
        }
        return ad.getAdmin(user);
    }

    @Transactional
    @Override
    public List<Complex> getAdminComplexes(final Admin admin) {
        final List<Complex> complexes = admin.getComplexes();
        complexes.size(); // Force to init collection
        return complexes;
    }

}
