package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.StatesDao;
import ar.edu.itba.paw.interfaces.StatesService;
import ar.edu.itba.paw.model.States;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StateServiceImpl implements StatesService {

    @Autowired
    private StatesDao stateDao;

    @Override
    public List<States> getAllStates() {
        return stateDao.getAllStates();
    }

}
