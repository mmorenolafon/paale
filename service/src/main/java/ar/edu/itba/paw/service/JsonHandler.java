package ar.edu.itba.paw.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


public class JsonHandler {
    public static String jsonStringify(Object object) {
        try {
            return String.format("{%s}", jsonStringifyR(object));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return "No json representation";
    }

    private static String jsonStringifyR(Object object) throws IllegalAccessException, InvocationTargetException {
        StringBuilder response = new StringBuilder();
        List<String> objList = new LinkedList<>();
        Class objectType = object.getClass();
        if (objectType.equals(String.class)) {
            response.append("'").append(object.toString()).append("'");
        } else if (objectType.equals(Integer.class)) {
            response.append(object.toString());
        } else if (objectType.equals(List.class)) {
            List<String> list = new LinkedList<>();
            ((List) object).forEach(x -> {
                try {
                    list.add(jsonStringifyR(x));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            });
            response.append(list.stream().collect(Collectors.joining(",", "[", "]")));
        } else {
            for (Method method : object.getClass().getDeclaredMethods()) {
                String methodFullName = method.getName();
                String fieldName;
                if (methodFullName.startsWith("get")) {
                    char[] array = methodFullName.substring(3).toCharArray();
                    array[0] = Character.toLowerCase(array[0]);
                    fieldName = String.valueOf(array);

                    Class type = method.getReturnType();
                    if (type.equals(String.class)) {
                        objList.add(String.format("%s:'%s'", fieldName, method.invoke(object).toString()));
                    } else if (type.equals(Integer.class)) {
                        objList.add(String.format("%s:%s", fieldName, method.invoke(object).toString()));
                    } else if (type.equals(List.class)) {
                        List<String> list = new LinkedList<>();
                        ((List) method.invoke(object)).forEach(x -> {
                            try {
                                list.add(jsonStringifyR(x));
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        });
                        String listString = list.stream().collect(Collectors.joining(",", "[", "]"));
                        objList.add(String.format("%s:%s", fieldName, listString));
                    } else {
                        objList.add(String.format("%s:{%s}", fieldName, jsonStringifyR(method.invoke(object))));
                    }
                }
            }
            response.append(objList.stream().collect(Collectors.joining(",")));
        }
        return response.toString();
    }
}
