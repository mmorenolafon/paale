package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.ComplexService;
import ar.edu.itba.paw.interfaces.EmailSender;
import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.model.Complex;
import ar.edu.itba.paw.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.AccessControlException;
import java.util.Properties;

@Service
public class EmailSenderImpl implements EmailSender {

  private static final Logger LOGGER = LoggerFactory.getLogger(EmailSenderImpl.class);
  private static final CharSequence FACEBOOK_POSTFIX = "@facebook";

  private final String SUBJECT = "Hay Equipo ⚽ - Reserva de cancha ";

  private final String CANCELLATION_SUBJECT = "Hay Equipo ⚽ - Cancelación de reserva";

  private final String INVITATION_SUBJECT = "Hay Equipo ⚽ - Estás invitado a un partido";

  //userName, complexName, complexAddress, pitchId, day, month, year, startTime, endTime
  private final String USER_CONTENT_TEMPLATE = "<div dir=3D\"ltr\">¡Hola %s!\n" +
    "<div><div>Tu reserva se realizó con éxito. Los datos contenidos en este mail van a ser solicitados el " +
    "día del partido, ¡guardá este mail o anotalos!</div></div><div><br></div><div><br></div>\n" +
    "<div><b><u>DATOS DE LA RESERVA</u></b></div>\n" +
    "<div><b><u><br></u></b></div><div><b>Nombre del complejo:</b> %s</div>\n" +
    "<div><b>Dirección: </b>%s</div><div><b>Número de cancha: </b>%d</div>\n" +
    "<div><b>Fecha: </b>%d/%d/%d</div><div><b>Horario:</b> %d:00-%d:00</div>\n" +
    "<div><br></div><div>Hay Equipo</div></div>";

  //userName,userLastName, complexName, complexAddress, pitchId, day, month, year, startTime, endTime
  private final String INVITATION_CONTENT_TEMPLATE = "<div dir=3D\"ltr\">¡Buenas!\n" +
    "<div><div>%s %s te invitó a un partido. Ponete en contacto con él para confirmarle que vas a ir.</div></div><div><br></div><div><br></div>\n" +
    "<div><br><br><b><u>DATOS DE LA RESERVA</u></b></div>\n %s" +
    "<div><b><u><br></u></b></div><div><b>Nombre del complejo:</b> %s</div>\n" +
    "<div><b>Dirección: </b>%s</div><div><b>Número de cancha: </b>%d</div>\n" +
    "<div><b>Fecha: </b>%d/%d/%d</div><div><b>Horario:</b> %d:00-%d:00</div>\n" +
    "<div><br></div><div>Hay Equipo</div></div>";

  // message
  private final String INVITATION_MESSAGE_SEGMENT =  "<div style=\"height:1px;background-color:#eee\"></div>" +
    "<div><br><b>Mensaje de %s :</b> \"<i>%s</i>\"<br><br></div>" +
    "<div style=\"height:1px;background-color:#eee\"><br><br><br></div>";

  //userFirstName, userLastName, userEmail, pitchId, day, month, year, startTime, endTime
  private final String COMPLEX_CONTENT_TEMPLATE = "<div dir=3D\"ltr\"><div style=3D\"font-size:12.8px\">Se ha " +
    "realizado una reserva en una de sus canchas. Los datos de la reserva se presentan a continuación:</div>\n" +
    "<div><br></div><div style=3D\"font-size:12.8px\"><b><u>DATOS DE LA RESERVA</u></b></div>\n" +
    "<div style=3D\"font-size:12.8px\"><b><u><br></u></b></div>\n" +
    "<div style=3D\"font-size:12.8px\"><b>Nombre y apellido del usuario: </b>%s %s</div>\n %s" +
    "<b>Número de cancha: </b>%d</div>\n<div style=3D\"font-size:12.8px\"><b>Fecha: </b>%d/%d/%d</div>" +
    "<div style=3D\"font-size:12.8px\"><b>Horario: <b>%d:00-%d:00</div>\n" +
    "<div><br></div><div style=3D\"font-size:12.8px\">Hay Equipo</div>\n" +
    "</div>\n";

  //userFirstName, userLastName, userEmail, pitchId, day, month, year, startTime, endTime
  private final String COMPLEX_CANCELLATION_TEMPLATE = "<div dir=3D\"ltr\"><div style=3D\"font-size:12.8px\">Se ha " +
    "cancelado una reserva en una de sus canchas.</div>\n" +
    "<div><br></div><div style=3D\"font-size:12.8px\"><b><u>DATOS DE LA RESERVA CANCELADA</u></b></div>\n" +
    "<div style=3D\"font-size:12.8px\"><b><u><br></u></b></div>\n" +
    "<div style=3D\"font-size:12.8px\"><b>Nombre y apellido del usuario: </b>%s %s</div>\n %s" +
    "<b>Número de cancha: </b>%d</div>\n<div style=3D\"font-size:12.8px\"><b>Fecha: </b>%d/%d/%d</div>" +
    "<div style=3D\"font-size:12.8px\"><b>Horario: <b>%d:00-%d:00</div>\n" +
    "<div><br></div><div style=3D\"font-size:12.8px\">Hay Equipo</div>\n" +
    "</div>\n";

  private final String EMAIL_SEGMENT =
    "<div style=3D\"font-size:12.8px\"><b>Email del usuario: </b>%s</div><div style=3D\"font-size:12.8px\">";



  @Autowired
  private UserService us;

  @Autowired
  private ComplexService cs;

  Transport transport;
  MimeMessage message;

  public EmailSenderImpl() {
    connect();
  }


  @Override
  public boolean sendBookingConfirmation(int complexId, int pitchId, int day, int month, int year, int startTime,
                                         int endTime) throws AccessControlException {
    Complex complex = cs.getComplexById(complexId);
    String complexEmailAddress = complex.getEmail();

    User user = us.getLoggedUser();

    String userEmail;
    try {
      userEmail = user.getEmail();
    } catch (NullPointerException e) {
      throw new AccessControlException("User should be logged");
    }

    String emailSegment = "";
    if(!userEmail.toLowerCase().contains(FACEBOOK_POSTFIX)){
      emailSegment=String.format(EMAIL_SEGMENT,userEmail);
    }

    String userFirstName = user.getFirstName();
    String userLastName = user.getLastName();

    String userContent = String.format(USER_CONTENT_TEMPLATE, userFirstName, complex.getName(), complex.getAddress(),
      pitchId, day, month, year, startTime, endTime);
    sendMailTo(userEmail, SUBJECT, userContent);

    String complexContent = String.format(COMPLEX_CONTENT_TEMPLATE, userFirstName, userLastName, emailSegment, pitchId,
      day, month, year, startTime, endTime);
    blockingSendMailTo(complexEmailAddress, SUBJECT, complexContent);

    return true;
  }

  @Override
  public boolean sendBookingCancellation(int complexId, int pitchId, int day, int month, int year, int startTime, int endTime) {
    Complex complex = cs.getComplexById(complexId);
    String complexEmailAddress = complex.getEmail();

    User user = us.getLoggedUser();

    String userEmail;
    try {
      userEmail = user.getEmail();
    } catch (NullPointerException e) {
      throw new AccessControlException("User should be logged");
    }

    String emailSegment = "";
    if(!userEmail.toLowerCase().contains(FACEBOOK_POSTFIX)){
      emailSegment=String.format(EMAIL_SEGMENT,userEmail);
    }

    String userFirstName = user.getFirstName();
    String userLastName = user.getLastName();



    String complexContent = String.format(COMPLEX_CANCELLATION_TEMPLATE, userFirstName, userLastName, emailSegment, pitchId,
      day, month, year, startTime, endTime);
    sendMailTo(complexEmailAddress, CANCELLATION_SUBJECT, complexContent);

    return true;
  }

  @Override
  public void sendBookingInvitation(int complexId, int pitchId, int day, int month, int year, int startTime,
                                    int endTime, String[] emailList, String message) {

    User user = us.getLoggedUser();
    Complex complex = cs.getComplexById(complexId);

    String userEmail;
    try {
      userEmail = user.getEmail();
    } catch (NullPointerException e) {
      LOGGER.error("User should be logged");
      return;
    }
    String userFirstName = user.getFirstName();
    String userLastName = user.getLastName();

    String messageSegment = "";
    if(!"undefined".equals(message) && message.trim().length()>0){
      messageSegment = String.format(INVITATION_MESSAGE_SEGMENT,userFirstName,message);
    }


    String invitationContent = String.format(INVITATION_CONTENT_TEMPLATE, userFirstName, userLastName, messageSegment, complex.getName(), complex.getAddress(),
      pitchId, day, month, year, startTime, endTime);

    for (String invitedEmail : emailList) {
      blockingSendMailTo(invitedEmail, INVITATION_SUBJECT, invitationContent);
    }

    return;

  }


  private static final int MAX_EMAILS_TRIES = 1;

  //This method does not waits for the email to be sent
  public boolean sendMailTo(String to, String subject, String content) {
    Thread sendMail = new Thread() {
      public void run() {
        sendMailTo(to, subject, content);
      }
    };
    sendMail.start();
    return true;
  }


  private boolean blockingSendMailTo(String to, String subject, String content) {
    boolean sent = false;
    for (int tries = 0; tries < MAX_EMAILS_TRIES; tries++) {
      try {
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
        message.setSubject(subject);//formBean.getString(
        message.setText(content, "utf-8", "html");
        transport.send(message, InternetAddress.parse(to));//(message);
        sent = true;
      } catch (MessagingException e) {
        LOGGER.error("Cannot send mail email to {}, try {}", to, tries);
      }
    }
    if (sent) {
      LOGGER.error("Email sent to {}", to);
    }
    return sent;
  }


  private void connect() {

    Properties props = new Properties();
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.port", "465");
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.starttls.enable", "true");

    final String username = "hayequipoteam@gmail.com";
    final String password = "hayequipopaw";

    Session session = Session.getInstance(props,
      new javax.mail.Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
          return new PasswordAuthentication(username, password);
        }
      });

    boolean connected = true;
    while (!connected) {
      try {
        transport = session.getTransport();
        transport.connect();
        connected = true;
      } catch (NoSuchProviderException e) {
        e.printStackTrace();
      } catch (MessagingException e) {
        LOGGER.info("Email Provider connection fail, retrying SMPT");
      }
    }

    message = new MimeMessage(session);
    try {
      message.setFrom(new InternetAddress("hayequipoteam@gmail.com"));
    } catch (MessagingException e) {
      e.printStackTrace();
    }


  }
}
