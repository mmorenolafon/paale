package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.FacebookService;
import ar.edu.itba.paw.interfaces.UserService;
import ar.edu.itba.paw.model.User;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Reading;
import facebook4j.auth.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FacebookServiceImpl implements FacebookService {
  private static final Logger LOGGER = LoggerFactory.getLogger(FacebookServiceImpl.class);

  @Autowired
  UserService us;

  @Override
  public User loginUser(String fToken) {

    Facebook facebook = new FacebookFactory().getInstance();
    facebook.setOAuthAppId("218740625229988", "1a2bea936cfb3bedba339eb143399033");
    facebook.setOAuthPermissions("email,public_profile");

    facebook.setOAuthAccessToken(new AccessToken(fToken, null));

    try {
      String id = facebook.getId();
      Reading reading = new Reading();
      facebook4j.User fUser = facebook.getUser(id, reading.
        fields("id,email,first_name,last_name,middle_name"));

      String emailGot = fUser.getEmail();
      if(emailGot==null){
        emailGot = String.format("%s@facebook.com",fUser.getId());
      }

      User user = us.getByEmail(emailGot);

      if(user==null){
          user = us.register(fUser.getFirstName(),fUser.getLastName(),null,emailGot,null,null);
      }

      return user;
    } catch (FacebookException e) {}

    return null;
  }
}
