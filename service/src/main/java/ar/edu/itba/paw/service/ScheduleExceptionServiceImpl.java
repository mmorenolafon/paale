package ar.edu.itba.paw.service;

import ar.edu.itba.paw.interfaces.ComplexService;
import ar.edu.itba.paw.interfaces.ScheduleExceptionDao;
import ar.edu.itba.paw.interfaces.ScheduleExceptionService;
import ar.edu.itba.paw.model.ScheduleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.List;


@Service
public class ScheduleExceptionServiceImpl implements ScheduleExceptionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingServiceImpl.class);

    @Autowired
    private ComplexService complexService;

    @Autowired
    private ScheduleExceptionDao scheduleExceptionDao;

    @Override
    @Transactional
    public boolean addScheduleException(int complexId, String beginDate, String endDate) {
        LocalDate beginCalendar = getDateFromString(beginDate, "yyyy-MM-dd");
        LocalDate endCalendar = getDateFromString(endDate, "yyyy-MM-dd");
        return complexService.addScheduleException(complexId, beginCalendar, endCalendar);
    }


    @Override
    @Transactional
    public void deleteScheduleException(int complexId, String beginDate, String endDate) {
        LocalDate beginCalendar = getDateFromString(beginDate, "yyyy-MM-dd");
        LocalDate endCalendar = getDateFromString(endDate, "yyyy-MM-dd");
        complexService.deleteScheduleException(complexId, beginCalendar, endCalendar);
    }


    @Override
    public List<ScheduleException> getAllScheduleExceptionsByComplex(int complexId) {
        return complexService.getScheduleExceptions(complexId);
    }

    @Override
    public void removeOldScheduleExceptions() {
        scheduleExceptionDao.removeOldScheduleExceptions();
    }


    private LocalDate getDateFromString(String date, String dateFormat) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
        dtf.withResolverStyle(ResolverStyle.STRICT);
        LocalDate cal = null;
        try {
            cal = LocalDate.parse(date, dtf);
        } catch (DateTimeParseException e) {}
        return cal;
    }

}
